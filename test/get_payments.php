<?
//require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
//$APPLICATION->SetTitle("Тест");
//$APPLICATION->RestartBuffer();

$filter = array();

$filter['ACTIVE'] = 'N';
$filter['!XML_ID'] = false;

if(!isset($by))
	$by = 'ID';
if(!isset($order))
	$order = 'ASC';

$dbRes = \Bitrix\Sale\Internals\PaySystemActionTable::getList(
	array(
		'select' => array('ID', 'XML_ID'),
		// 'select' => array('*'),
		'filter' => $filter,
		'order' => array(ToUpper($by) => ToUpper($order))
	)
);

$arrPayments = array();
while ($arResult = $dbRes->fetch())
	$arrPayments[$arResult['XML_ID']] = $arResult['ID'];

//pr(count($arrPayments));
//pr($arrPayments);
