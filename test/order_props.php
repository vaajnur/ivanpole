<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Тест");
$APPLICATION->RestartBuffer();

use Bitrix\Sale;

\Bitrix\Main\Loader::includeModule('sale');

include 'cache.php';

//pr($CACHE['arDeliveries']);
//pr($CACHE['arrPayments']);

// pr(count($CACHE['arDeliveries']));
// pr(count($CACHE['arrPayments']));
//exit;

$ws_order = array (
  'id' => '95921',
  'contact_id' => '68383',
  'create_datetime' => '2020-12-23 16:21:05',
  'update_datetime' => '2020-12-23 19:29:48',
  'state_id' => 'sobran',
  'total' => '130.0000',
  'currency' => 'RUB',
  'rate' => '1.00000000',
  'tax' => '21.6700',
  'shipping' => '0.0000',
  'discount' => '20.0000',
  'assigned_contact_id' => NULL,
  'paid_year' => '2020',
  'paid_quarter' => '4',
  'paid_month' => '12',
  'paid_date' => '2020-12-23',
  'is_first' => '0',
  'unsettled' => '0',
  'comment' => 'ЗАБОР СО СКЛАДА!!!! НЕ УПАКОВЫВАТЬ!!!',
  'shipping_datetime' => '2020-12-23 23:30:00',
  'manager_id' => '5554',
  'roistat_visit' => '650140',
  'crmr_id' => NULL,
  'crmr_status' => NULL,
  'winner' => '0',
  'params' => 
  array (
    'ip' => '62.141.68.133',
    'kmgmt.ymuid' => '1593179583802681024',
    'kmgtm.cid' => '634636592.1603958732',
    'landing' => '/search/',
    'payment_id' => '9',
    'payment_name' => 'Банковской картой онлайн',
    'payment_plugin' => 'sb',
    'plugin_nocall' => 'no',
    'reduced' => '1',
    'reduce_times' => '1',
    'referer' => 'https://acs3.sbrf.ru/',
    'referer_host' => 'acs3.sbrf.ru',
    'sales_channel' => 'storefront:ivan-pole.ru',
    'shipping_address.city' => 'Москва',
    'shipping_address.country' => 'rus',
    'shipping_address.region' => '77',
    'shipping_address.street' => 'Ильинский тупик, 6',
    'shipping_address.zip' => '143405',
    'shipping_currency' => 'RUB',
    'shipping_currency_rate' => '1',
    'shipping_end_datetime' => '2020-12-23 23:30:00',
    'shipping_est_delivery' => '',
    'shipping_id' => '14',
    'shipping_name' => 'Самовывоз',
    'shipping_plugin' => 'courier',
    'shipping_rate_id' => 'delivery',
    'shipping_start_datetime' => '2020-12-23 00:00:00',
    'shipping_tax_id' => '',
    'signup_url' => '',
    'storefront' => 'ivan-pole.ru',
    'user_agent' => 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36',
  ),
  'contact' => 
  array (
    'id' => '68383',
    'name' => 'kesova.ma Кесова Марианна',
    'email' => 'kesova.ma@ssnab.ru',
    'phone' => '79261715561',
    'registered' => true,
    'photo_50x50' => 'https://ivan-pole.ru/wa-content/img/userpic50.jpg',
  ),
  'items' => 
  array (
    627834 => 
    array (
      'id' => '627834',
      'order_id' => '95921',
      'name' => 'Соус «Сметанный с белыми грибами» ГОСТ (Соус «Сметанный с белыми грибами» ГОСТ)',
      'product_id' => '87',
      'sku_id' => '88',
      'sku_code' => '30085056',
      'type' => 'product',
      'service_id' => NULL,
      'service_variant_id' => NULL,
      'price' => '150.0000',
      'quantity' => '1',
      'parent_id' => NULL,
      'stock_id' => NULL,
      'virtualstock_id' => NULL,
      'purchase_price' => '35.6100',
      'total_discount' => '20.0000',
      'tax_percent' => '20.0000',
      'tax_included' => '1',
      'goodsItemIndex' => NULL,
      'image_id' => '3422',
      'image_filename' => '',
      'sku_image_id' => '0',
      'ext' => 'png',
      'file_name' => '',
      'file_size' => '0',
    ),
  ),
  'items_total_discount' => 0,
);

$ws_order_params = $ws_order['params'];
$ws_contact = $ws_order['contact'];

  array (
    'id' => '68383',
    'name' => 'kesova.ma Кесова Марианна',
    'email' => 'kesova.ma@ssnab.ru',
    'phone' => '79261715561',
    'registered' => true,
    'photo_50x50' => 'https://ivan-pole.ru/wa-content/img/userpic50.jpg',
  );


$user_ID = false;    
$filter = Array
(
    "UF_EXTERNAL_ID"                  => "68383",
);
$by = array();
$order = array();
$rsUsers = CUser::GetList($by, $order, $filter); // выбираем пользователей
while($rsUsers->NavNext(true, "f_")) :
    echo "[".$f_ID."] (".$f_LOGIN.") ".$f_NAME." ".$f_LAST_NAME."<br>";
	$user_ID = $f_ID;    	
endwhile;

if($user_ID == false){
	$user = new CUser;
	$arFields = Array(
	  "NAME"              => $ws_contact['name'],
	  // "LAST_NAME"         => "",
	  "EMAIL"             => $ws_contact['email'],
	  "LOGIN"             => $ws_contact['email'],
	  "PERSONAL_PHONE"	  => $ws_contact['phone'],
	  // "PHONE_NUMBER"	  => $ws_contact['phone'],
	  "LID"               => "s1",
	  "ACTIVE"            => "Y",
	  "GROUP_ID"          => array(2),
	  "PASSWORD"          => "Vcv9EwXgP9J~(=v[",
	  "CONFIRM_PASSWORD"  => "Vcv9EwXgP9J~(=v[",
	  "PERSONAL_PHOTO"    => '',
	  "UF_EXTERNAL_ID"    => $ws_contact['id'],
	);

	$user_ID = $user->Add($arFields);
	if (intval($user_ID) > 0)
	    echo "$user_ID Пользователь успешно добавлен.";
	else
	    echo $user->LAST_ERROR;
}

// exit;


	$siteId = 's1'; // код сайта
	$order = \Bitrix\Sale\Order::create($siteId, $user_ID);
	// var_dump($order->setField('PRICE', '1000'));
	// var_dump($order->getPrice());
//	$order = \Bitrix\Sale\Order::load(31);
        
        
	$collection = $order->getPropertyCollection();
	
        $counter = 99;
        foreach ($ws_order_params  as $key  => $param){
            $counter++;
            $propertyValue = $collection->createItem([
                'ID' => $counter,
                'NAME' => $key,
                'TYPE' => 'STRING',
                'CODE' => $key,
            ]);

            $propertyValue->setField('VALUE', $param);
        }

        $all_items = $all_goods = array(
	        'PRODUCT_ID' => 5649, // Конфитюр «Клубника-Грейпфрут» 
	        'PRODUCT_PROVIDER_CLASS' => '\Bitrix\Catalog\Product\CatalogProvider',
	        'NAME' => 'Конфитюр «Клубника-Грейпфрут»', 
	        'PRICE' => 1000, 
	        'CURRENCY' => 'RUB', 
	        'QUANTITY' => 3, 
			);
    $products = [
		$all_goods
	];

	$basket = Bitrix\Sale\Basket::create('s1');
       $order->setBasket($basket);
    // $basket = $order->getBasket();
        
	foreach ($products as $product)
	{
	    $item = $basket->createItem("catalog", $product["PRODUCT_ID"]);
	    unset($product["PRODUCT_ID"]); 
	    $item->setFields($product);
	}
        $order->setPersonTypeId(1); 


	/**
	 * [$shipmentCollection description]
	 * @var [type]
	 */
//        var_dump($CACHE['arDeliveries']);
//        var_dump($CACHE['arDeliveries'][$ws_order_params['shipping_id']]);
	$shipmentCollection = $order->getShipmentCollection();
	$shipment = $shipmentCollection->createItem(
	    Bitrix\Sale\Delivery\Services\Manager::getObjectById($CACHE['arDeliveries'][$ws_order_params['shipping_id']]) // 1 - ID службы доставки
	);

	$shipmentItemCollection = $shipment->getShipmentItemCollection();

	foreach ($basket as $basketItem)
	{
	    $item = $shipmentItemCollection->createItem($basketItem);
	    $item->setQuantity($basketItem->getQuantity());
	}
        	/**
	 * [$paymentCollection description]
	 * @var [type]
	 */
	$paymentCollection = $order->getPaymentCollection();
	$payment = $paymentCollection->createItem(
	    Bitrix\Sale\PaySystem\Manager::getObjectById($CACHE['arrPayments'][$ws_order_params['payment_id']]) // 1 - ID платежной системы
	);

	$payment->setField("SUM", $ws_order['total']);
	$order->setField('PRICE', $ws_order['total']);
//	$payment->setField("tax", $ws_order['tax']);
//	$payment->setField("shipping", $ws_order['shipping']);
//	$payment->setField("discount", $ws_order['discount']);
	$payment->setField("CURRENCY", 'RUB');


	$order->save();
	// die();


