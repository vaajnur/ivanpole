<?
$payments = array (
  0 => 
  array (
    'id' => '9',
    'type' => 'payment',
    'plugin' => 'sb',
    'name' => 'Банковской картой онлайн',
    'description' => 'Оплата онлайн через платёжный шлюз крупнейшего банка России',
    'logo' => '',
    'status' => '1',
    'sort' => '0',
    'options' => 
    array (
      'customer_type' => '',
      'shipping_type' => 
      array (
        'pickup' => 'pickup',
        'todoor' => 'todoor',
        'post' => 'post',
      ),
    ),
    'available' => true,
  ),
  1 => 
  array (
    'id' => '5',
    'type' => 'payment',
    'plugin' => 'cash',
    'name' => 'Наличные',
    'description' => 'Оплата наличными при получении',
    'logo' => '',
    'status' => '1',
    'sort' => '2',
    'options' => 
    array (
      'customer_type' => '',
      'shipping_type' => 
      array (
        'todoor' => 'todoor',
      ),
    ),
    'available' => true,
  ),
  2 => 
  array (
    'id' => '2',
    'type' => 'payment',
    'plugin' => 'dummy',
    'name' => 'Банковской картой при получении',
    'description' => 'Оплата картой курьеру при доставке до двери. Обратите внимание: не все пункты самовывоза оборудованы терминалами приёма карт. Уточните возможность оплаты на самом пункте самовывоза.',
    'logo' => '',
    'status' => '1',
    'sort' => '3',
    'options' => 
    array (
      'customer_type' => '',
      'shipping_type' => 
      array (
        'todoor' => 'todoor',
      ),
    ),
    'available' => true,
  ),
  3 => 
  array (
    'id' => '34',
    'type' => 'payment',
    'plugin' => 'bill',
    'name' => 'Счёт (для юридических лиц)',
    'description' => 'Оплата по счёту',
    'logo' => '',
    'status' => '1',
    'sort' => '4',
    'options' => 
    array (
      'customer_type' => '',
      'shipping_type' => 
      array (
        'pickup' => 'pickup',
        'todoor' => 'todoor',
        'post' => 'post',
      ),
    ),
    'available' => true,
  ),
  4 => 
  array (
    'id' => '35',
    'type' => 'payment',
    'plugin' => 'invoicephys',
    'name' => 'Оплата по квитанции',
    'description' => 'Оплата наличными по квитанции для физических лиц (РФ)',
    'logo' => '/wa-plugins/payment/invoicephys/img/invoicephys.png',
    'status' => '1',
    'sort' => '5',
    'options' => 
    array (
      'customer_type' => '',
      'shipping_type' => 
      array (
        'pickup' => 'pickup',
        'todoor' => 'todoor',
        'post' => 'post',
      ),
    ),
    'available' => true,
  ),
  5 => 
  array (
    'id' => '66',
    'type' => 'payment',
    'plugin' => 'yandexkassa',
    'name' => 'Яндекс.Касса (новый протокол)',
    'description' => 'Приём платежей через сервис «Яндекс.Касса» (<a href="https://kassa.yandex.ru/">kassa.yandex.ru</a>).',
    'logo' => '/wa-plugins/payment/yandexkassa/img/yandexkassa.png',
    'status' => '0',
    'sort' => '6',
    'options' => 
    array (
      'customer_type' => '',
      'shipping_type' => 
      array (
        'pickup' => 'pickup',
        'todoor' => 'todoor',
        'post' => 'post',
      ),
    ),
    'available' => true,
  ),
);