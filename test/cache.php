<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$cacheTime = '8640000';
$cacheId = 'catalog_elems';
$cacheDir = 'catalog_elems';

$cache = Bitrix\Main\Data\Cache::createInstance();

// удалить!!!
// $obCache = new CPHPCache(); 
// $obCache->CleanDir($cacheDir);
// exit;

global $CACHE;

if ($cache->initCache($cacheTime, $cacheId, $cacheDir))
{
	// var_dump(111111111111);
    $CACHE = $cache->getVars();
}
elseif ($cache->startDataCache())
{
	// var_dump(22222222222);
    $CACHE = array();
    // ...
    if ($isInvalid)
    {
        $cache->abortDataCache();
    }
    // ...

        // CATALOG PRODS
    $res1 = ciblockelement::getlist(array(), array('IBLOCK_ID' => 4, 'IBLOCK_TYPE' => 'catalog'), false, false, array('ID', 'NAME', 'IBLOCK_ID'));
    $prods_list = [];
    while ($ob = $res1->getnextelement()) {
        $f = $ob->getfields();
        $props = $ob->getproperties();
        if($props['ARTNUMBER']['VALUE'] != '')
            $f['ARTNUMBER'] = $props['ARTNUMBER']['VALUE'];
        else
            $f['ARTNUMBER'] = '';
        $prods_list[] = $f;
    }
    
            // DELIVERIES
    include 'get_deliveries.php';
    
        // PAYMENTS
    include 'get_payments.php';


    

    $CACHE['prods_list'] = $prods_list;
    $CACHE['arDeliveries'] = $arDeliveries;
    $CACHE['arrPayments'] = $arrPayments;
    
    
    $cache->endDataCache($CACHE);
} 
