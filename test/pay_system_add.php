<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Тест");
$APPLICATION->RestartBuffer();

use Bitrix\Sale;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;
use Bitrix\Main\Application;
use Bitrix\Sale\Internals\PaySystemActionTable;
use Bitrix\Main\Page\Asset;
use Bitrix\Sale\Helpers\Admin\BusinessValueControl;
use Bitrix\Sale\Services\PaySystem\Restrictions;
use Bitrix\Sale\PaySystem;
use Bitrix\Sale\BusinessValue;
use Bitrix\Main\SystemException;
use Bitrix\Main\IO;

\Bitrix\Main\Loader::includeModule('sale');

$instance = Application::getInstance();
$context = $instance->getContext();
$request = $context->getRequest();
$server = $context->getServer();
$documentRoot = Application::getDocumentRoot();
$paySystem = array();



class class1{

	public $POST;

	function __construct($post){
		$this->POST = $post;
	}

 function get($var)
	{
		// var_dump($POST);
		return $this->POST[$var];
	}
}


include 'payments.php';

foreach ($payments as $key => $value) {
	$POST = [
	"filter" => "Y",
	"set_filter" => "Y",
	"Update" => "Y",
	"lang" => "ru",
	"ID" => "0",
	"sessid" => "32678c6d24c4abf85cfc7931f378eeba",
	"autosave_id" => "243149746fd510121918030964df7b0de",
	"ACTION_FILE" => "",
	"PRIOR_ACTION_FILE" => "",
	"PRIOR_PS_MODE" => "",
	"PSA_NAME" => $value['name'],
	"NAME" => $value['name'],
	"ACTIVE" => "N",
	"SORT" => "10",
	"DESCRIPTION" => $value['description'],
	"LOGOTIP" => "",
	"IS_CASH" => "N",
	"ALLOW_EDIT_PAYMENT" => "Y",
	"ENCODING" => "",
	"CODE" => "",
	"XML_ID" => $value['id'],
	"tabControl_active_tab" => "edit1",
	];
	$request = new class1($POST);
	
	// create_pay_system($request);
}



function create_pay_system($request){
	$id = (int)$request->get('ID');
	$name = trim($request->get('NAME'));
	$sort = (int)$request->get('SORT');

	// var_dump($request->get('PSA_NAME'));

	if ($sort <= 0)
		$sort = 100;


	if ($name == '')
		$errorMessage .= "ERROR_NO_NAME"."<br>";

		$fields = array(
			"NAME" => $name,
			"PSA_NAME" => $request->get('PSA_NAME'),
			"ACTIVE" => ($request->get('ACTIVE') != 'Y') ? 'N' : $request->get('ACTIVE'),
			"CAN_PRINT_CHECK" => ($request->get('CAN_PRINT_CHECK') != 'Y') ? 'N' : $request->get('CAN_PRINT_CHECK'),
			"CODE" => $request->get('CODE'),
			"NEW_WINDOW" => ($request->get('NEW_WINDOW') != 'Y') ? 'N' : $request->get('NEW_WINDOW'),
			"ALLOW_EDIT_PAYMENT" => ($request->get('ALLOW_EDIT_PAYMENT') != 'Y') ? 'N' : $request->get('ALLOW_EDIT_PAYMENT'),
			"IS_CASH" => (!in_array($request->get('IS_CASH'), array('Y', 'A'))) ? 'N' : $request->get('IS_CASH'),
			"ENTITY_REGISTRY_TYPE" => \Bitrix\Sale\Registry::REGISTRY_TYPE_ORDER,
			"SORT" => $sort,
			"ENCODING" => $request->get('ENCODING'),
			"DESCRIPTION" => htmlspecialcharsback($request->get('DESCRIPTION')),
			"ACTION_FILE" => $actionFile,
			'PS_MODE' => ($request->get('PS_MODE')) ? $request->get('PS_MODE') : '',
			'XML_ID' => ($request->get('XML_ID')) ?: PaySystem\Manager::generateXmlId()
		);
		if ($request->get('AUTO_CHANGE_1C') == 'Y')
			$fields['AUTO_CHANGE_1C'] = 'Y';
		else
			$fields['AUTO_CHANGE_1C'] = 'N';


		if ($id > 0)
		{
			$result = PaySystemActionTable::update($id, $fields);

			if (!$result->isSuccess())
				$errorMessage .= join(',', $result->getErrorMessages()).".<br>";
		}
		else
		{
			$result = PaySystemActionTable::add($fields);
			if (!$result->isSuccess())
			{
				$errorMessage .= join(',', $result->getErrorMessages());
			}
			else
			{
				$id = $result->getId();
				var_dump('New id ');
				var_dump($id);
				if ($id > 0)
				{
					$fields = array(
						'PARAMS' => serialize(array('BX_PAY_SYSTEM_ID' => $id)),
						'PAY_SYSTEM_ID' => $id
					);

					$result = PaySystemActionTable::update($id, $fields);
					if (!$result->isSuccess())
						$errorMessage .= join(',', $result->getErrorMessages());

					$service = PaySystem\Manager::getObjectById($id);
					$currency = $service->getCurrency();
					if ($currency)
					{
						$params = array(
							'SERVICE_ID' => $id,
							'SERVICE_TYPE' => Restrictions\Manager::SERVICE_TYPE_PAYMENT,
							'PARAMS' => array('CURRENCY' => $currency)
						);
						Restrictions\Manager::getClassesList();
						$saveResult = \Bitrix\Sale\Services\PaySystem\Restrictions\Currency::save($params);
						if (!$saveResult->isSuccess())
							$errorMessage .= 'SALE_PSE_ERROR_RSRT_CURRENCY_SAVE';
					}
				}
			}
		}
	var_export($errorMessage);
}