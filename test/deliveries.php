<?

$deliveries = array (
  0 => 
  array (
    'id' => '49',
    'type' => 'shipping',
    'plugin' => 'courier',
    'name' => 'Самовывоз',
    'description' => 'Московская область, г. Красногорск, Ильинский тупик д.6<br>
Заказы выдаются строго с 9:00 до 17:00 с пн -вс',
    'logo' => '',
    'status' => '1',
    'sort' => '0',
    'options' => 
    array (
      'tax_id' => '',
      'assembly_time' => '0',
      'customer_type' => '',
    ),
    'available' => true,
  ),
  1 => 
  array (
    'id' => '14',
    'type' => 'shipping',
    'plugin' => 'courier',
    'name' => 'Самовывоз',
    'description' => 'Московская область, г. Красногорск, Ильинский тупик д.6<br>
Заказы выдаются строго с 9:00 до 19:00 с пн -вс',
    'logo' => '',
    'status' => '1',
    'sort' => '1',
    'options' => 
    array (
      'tax_id' => '',
      'assembly_time' => '0',
      'customer_type' => '',
    ),
    'available' => true,
  ),
  2 => 
  array (
    'id' => '57',
    'type' => 'shipping',
    'plugin' => 'courier',
    'name' => 'Доставка курьером сегодня',
    'description' => 'Доставка на указанный в контактной информации адрес в этот же день, если заказ оформлен до 13.00.',
    'logo' => '',
    'status' => '1',
    'sort' => '2',
    'options' => 
    array (
      'tax_id' => '1',
      'assembly_time' => '0',
      'customer_type' => '',
    ),
    'available' => true,
  ),
  3 => 
  array (
    'id' => '51',
    'type' => 'shipping',
    'plugin' => 'courier',
    'name' => 'Доставка курьером завтра',
    'description' => 'Доставка на указанный в контактной информации адрес на следующий день после совершения заказа, если он был оформлен после 18.00.',
    'logo' => '',
    'status' => '1',
    'sort' => '4',
    'options' => 
    array (
      'tax_id' => '1',
      'assembly_time' => '0',
      'customer_type' => '',
    ),
    'available' => true,
  ),
  4 => 
  array (
    'id' => '32',
    'type' => 'shipping',
    'plugin' => 'sydsek',
    'name' => 'Служба доставки СДЭК',
    'description' => 'Выберите пункт самовывоза из выпадающего меню.</br></br>',
    'logo' => '',
    'status' => '1',
    'sort' => '5',
    'options' => 
    array (
      'tax_id' => '1',
      'assembly_time' => '0',
      'customer_type' => '',
    ),
    'available' => true,
  ),
  5 => 
  array (
    'id' => '36',
    'type' => 'shipping',
    'plugin' => 'sydsek',
    'name' => 'Служба доставки СДЭК (СНГ)',
    'description' => 'Расчет стоимости доставки курьерской компанией СДЭК',
    'logo' => '',
    'status' => '1',
    'sort' => '6',
    'options' => 
    array (
      'tax_id' => '1',
      'assembly_time' => '0',
      'customer_type' => '',
    ),
    'available' => true,
  ),
  6 => 
  array (
    'id' => '6',
    'type' => 'shipping',
    'plugin' => 'bxb',
    'name' => 'Служба доставки Boxberry',
    'description' => 'Пожалуйста, дождитесь окончания загрузки списка пунктов доставки. Это может занять несколько минут в зависимости от нагрузки на сервер транспортной компании.',
    'logo' => '',
    'status' => '1',
    'sort' => '7',
    'options' => 
    array (
      'tax_id' => '1',
      'assembly_time' => '0',
      'customer_type' => '',
    ),
    'available' => true,
  ),
  7 => 
  array (
    'id' => '69',
    'type' => 'shipping',
    'plugin' => 'bxb',
    'name' => 'Служба доставки Boxberry - Беларусь',
    'description' => 'Пожалуйста, дождитесь окончания загрузки списка пунктов доставки. Это может занять несколько минут в зависимости от нагрузки на сервер транспортной компании.',
    'logo' => '',
    'status' => '1',
    'sort' => '8',
    'options' => 
    array (
      'tax_id' => '1',
      'assembly_time' => '',
      'customer_type' => '',
    ),
    'available' => true,
  ),
  8 => 
  array (
    'id' => '70',
    'type' => 'shipping',
    'plugin' => 'bxb',
    'name' => 'Служба доставки Boxberry - Казахстан',
    'description' => 'Пожалуйста, дождитесь окончания загрузки списка пунктов доставки. Это может занять несколько минут в зависимости от нагрузки на сервер транспортной компании.',
    'logo' => '',
    'status' => '1',
    'sort' => '9',
    'options' => 
    array (
      'tax_id' => '1',
      'assembly_time' => '0',
      'customer_type' => '',
    ),
    'available' => true,
  ),
  9 => 
  array (
    'id' => '4',
    'type' => 'shipping',
    'plugin' => 'courier',
    'name' => ' Dalli Service Курьер по Москве',
    'description' => 'Доставка на указанный в контактной информации адрес',
    'logo' => '',
    'status' => '1',
    'sort' => '10',
    'options' => 
    array (
      'tax_id' => '1',
      'assembly_time' => '0',
      'customer_type' => '',
    ),
    'available' => true,
  ),
  10 => 
  array (
    'id' => '10',
    'type' => 'shipping',
    'plugin' => 'courier',
    'name' => ' Dalli Service Курьер по Московской области (до 10 км от МКАД) ',
    'description' => 'Доставка на указанный в контактной информации адрес',
    'logo' => '',
    'status' => '1',
    'sort' => '11',
    'options' => 
    array (
      'tax_id' => '1',
      'assembly_time' => '0',
      'customer_type' => '',
    ),
    'available' => true,
  ),
  11 => 
  array (
    'id' => '11',
    'type' => 'shipping',
    'plugin' => 'courier',
    'name' => ' Dalli Service  Курьер по Санкт-Петербургу (В пределах КАД )',
    'description' => '',
    'logo' => '',
    'status' => '1',
    'sort' => '12',
    'options' => 
    array (
      'tax_id' => '1',
      'assembly_time' => '0',
      'customer_type' => '',
    ),
    'available' => true,
  ),
  12 => 
  array (
    'id' => '48',
    'type' => 'shipping',
    'plugin' => 'russianpost',
    'name' => 'Почта России',
    'description' => 'Расчет стоимости доставки по алгоритму, опубликованному <a href="https://www.pochta.ru/parcels" target="_blank">на сайте Почты России</a> для отправления посылок.',
    'logo' => '/wa-plugins/shipping/russianpost/img/RussianPost.png',
    'status' => '1',
    'sort' => '13',
    'options' => 
    array (
      'tax_id' => '1',
      'assembly_time' => '0',
      'customer_type' => '',
    ),
    'available' => true,
  ),
  13 => 
  array (
    'id' => '47',
    'type' => 'shipping',
    'plugin' => 'boxberry',
    'name' => 'Boxberry',
    'description' => 'Расчет стоимости доставки сервисом <a href="https://boxberry.ru/">Boxberry</a>.',
    'logo' => '',
    'status' => '0',
    'sort' => '14',
    'options' => 
    array (
      'tax_id' => '1',
      'assembly_time' => '3',
      'customer_type' => '',
    ),
    'available' => true,
  ),
  14 => 
  array (
    'id' => '59',
    'type' => 'shipping',
    'plugin' => 'courier',
    'name' => 'Доставка в филиал Санкт-Петербург',
    'description' => 'Доставка в филиал Санкт-Петербург для сотрудников Союзснаб',
    'logo' => '',
    'status' => '0',
    'sort' => '18',
    'options' => 
    array (
      'tax_id' => '',
      'assembly_time' => '0',
      'customer_type' => 'company',
    ),
    'available' => true,
  ),
  15 => 
  array (
    'id' => '61',
    'type' => 'shipping',
    'plugin' => 'courier',
    'name' => 'Доставка в филиал Краснодар',
    'description' => 'Доставка в филиал Краснодар для сотрудников Союзснаб',
    'logo' => '',
    'status' => '0',
    'sort' => '19',
    'options' => 
    array (
      'tax_id' => '',
      'assembly_time' => '0',
      'customer_type' => 'company',
    ),
    'available' => true,
  ),
  16 => 
  array (
    'id' => '62',
    'type' => 'shipping',
    'plugin' => 'courier',
    'name' => 'Доставка в филиал Омск',
    'description' => 'Доставка в филиал Омск для сотрудников Союзснаб',
    'logo' => '',
    'status' => '0',
    'sort' => '20',
    'options' => 
    array (
      'tax_id' => '',
      'assembly_time' => '0',
      'customer_type' => 'company',
    ),
    'available' => true,
  ),
  17 => 
  array (
    'id' => '63',
    'type' => 'shipping',
    'plugin' => 'courier',
    'name' => 'Доставка в филиал Екатеринбург',
    'description' => 'Доставка в филиал Екатеринбург для сотрудников Союзснаб',
    'logo' => '',
    'status' => '0',
    'sort' => '21',
    'options' => 
    array (
      'tax_id' => '',
      'assembly_time' => '0',
      'customer_type' => 'company',
    ),
    'available' => true,
  ),
  18 => 
  array (
    'id' => '64',
    'type' => 'shipping',
    'plugin' => 'courier',
    'name' => 'Доставка в филиал Новосибирск',
    'description' => 'Доставка в филиал Новосибирск для сотрудников Союзснаб',
    'logo' => '',
    'status' => '0',
    'sort' => '22',
    'options' => 
    array (
      'tax_id' => '',
      'assembly_time' => '0',
      'customer_type' => 'company',
    ),
    'available' => true,
  ),
  19 => 
  array (
    'id' => '65',
    'type' => 'shipping',
    'plugin' => 'courier',
    'name' => 'Доставка в филиал Калуга',
    'description' => 'Доставка в филиал Калуга для сотрудников Союзснаб',
    'logo' => '',
    'status' => '0',
    'sort' => '23',
    'options' => 
    array (
      'tax_id' => '',
      'assembly_time' => '0',
      'customer_type' => 'company',
    ),
    'available' => true,
  ),
  20 => 
  array (
    'id' => '12',
    'type' => 'shipping',
    'plugin' => 'courier',
    'name' => 'Доставка ОЗОН',
    'description' => '',
    'logo' => '',
    'status' => '0',
    'sort' => '24',
    'options' => 
    array (
      'tax_id' => '1',
      'assembly_time' => '0',
      'customer_type' => '',
    ),
    'available' => true,
  ),
  21 => 
  array (
    'id' => '56',
    'type' => 'shipping',
    'plugin' => 'pochta',
    'name' => 'Почта России',
    'description' => 'Расчет стоимости доставки через онлайн-сервис "Отправка" Почты России.',
    'logo' => '/wa-plugins/shipping/pochta/img/pochta.png',
    'status' => '0',
    'sort' => '25',
    'options' => 
    array (
      'tax_id' => '1',
      'assembly_time' => '0',
      'customer_type' => '',
    ),
    'available' => true,
  ),
  22 => 
  array (
    'id' => '15',
    'type' => 'shipping',
    'plugin' => 'courier',
    'name' => 'Доставка Wildberries',
    'description' => '',
    'logo' => '',
    'status' => '0',
    'sort' => '26',
    'options' => 
    array (
      'tax_id' => '',
      'assembly_time' => '0',
      'customer_type' => '',
    ),
    'available' => true,
  ),
  23 => 
  array (
    'id' => '16',
    'type' => 'shipping',
    'plugin' => 'courier',
    'name' => 'Самовывоз',
    'description' => 'г. Барнаул, ул. 1-я Западная, д.50<br>Заказы выдаются строго с 9:00 до 16:30',
    'logo' => '',
    'status' => '0',
    'sort' => '27',
    'options' => 
    array (
      'tax_id' => '',
    ),
    'available' => true,
  ),
  24 => 
  array (
    'id' => '17',
    'type' => 'shipping',
    'plugin' => 'courier',
    'name' => 'Самовывоз',
    'description' => 'г. Екатеринбург, ул. Новинская д.2, оф.212, 215<br>Заказы выдаются строго с 9:00 до 16:30',
    'logo' => '',
    'status' => '0',
    'sort' => '28',
    'options' => 
    array (
      'tax_id' => '',
    ),
    'available' => true,
  ),
  25 => 
  array (
    'id' => '18',
    'type' => 'shipping',
    'plugin' => 'courier',
    'name' => 'Самовывоз',
    'description' => 'г. Челябинск, Комсомольский проспект, д.10/2 оф.302<br>Заказы выдаются строго с 9:00 до 16:30',
    'logo' => '',
    'status' => '0',
    'sort' => '29',
    'options' => 
    array (
      'tax_id' => '',
    ),
    'available' => true,
  ),
  26 => 
  array (
    'id' => '19',
    'type' => 'shipping',
    'plugin' => 'courier',
    'name' => 'Самовывоз',
    'description' => 'г. Самара, пр. Мальцева д.7 оф.201, 202<br>Заказы выдаются строго с 9:00 до 16:30',
    'logo' => '',
    'status' => '0',
    'sort' => '30',
    'options' => 
    array (
      'tax_id' => '',
    ),
    'available' => true,
  ),
  27 => 
  array (
    'id' => '20',
    'type' => 'shipping',
    'plugin' => 'courier',
    'name' => 'Самовывоз',
    'description' => 'Республика Татарстан, г. Казань, ул. Портовая д.19<br>Заказы выдаются строго с 9:00 до 16:30',
    'logo' => '',
    'status' => '0',
    'sort' => '31',
    'options' => 
    array (
      'tax_id' => '',
    ),
    'available' => true,
  ),
  28 => 
  array (
    'id' => '21',
    'type' => 'shipping',
    'plugin' => 'courier',
    'name' => 'Самовывоз',
    'description' => 'г. Новосибирск, ул. Сухарная д.35 корп.3<br>Заказы выдаются строго с 9:00 до 16:30',
    'logo' => '',
    'status' => '0',
    'sort' => '32',
    'options' => 
    array (
      'tax_id' => '',
    ),
    'available' => true,
  ),
  29 => 
  array (
    'id' => '22',
    'type' => 'shipping',
    'plugin' => 'courier',
    'name' => 'Самовывоз',
    'description' => 'Ростов-на-Дону, Мясниковский район, Юго-Восточная Промзона 8/1<br>Заказы выдаются строго с 9:00 до 16:30',
    'logo' => '',
    'status' => '0',
    'sort' => '33',
    'options' => 
    array (
      'tax_id' => '',
    ),
    'available' => true,
  ),
  30 => 
  array (
    'id' => '23',
    'type' => 'shipping',
    'plugin' => 'courier',
    'name' => 'Самовывоз',
    'description' => 'г. Краснодар, ул. Бородинская д.150/11<br>Заказы выдаются строго с 9:00 до 16:30',
    'logo' => '',
    'status' => '0',
    'sort' => '34',
    'options' => 
    array (
      'tax_id' => '',
    ),
    'available' => true,
  ),
  31 => 
  array (
    'id' => '24',
    'type' => 'shipping',
    'plugin' => 'courier',
    'name' => 'Самовывоз',
    'description' => 'г. Волгоград, ул. Козловская д.54.<br>Заказы выдаются строго с 9:00 до 16:30',
    'logo' => '',
    'status' => '0',
    'sort' => '35',
    'options' => 
    array (
      'tax_id' => '',
    ),
    'available' => true,
  ),
  32 => 
  array (
    'id' => '25',
    'type' => 'shipping',
    'plugin' => 'courier',
    'name' => 'Самовывоз',
    'description' => 'г. Хабаровск, ул. Центральная д.22А<br>Заказы выдаются строго с 9:00 до 16:30',
    'logo' => '',
    'status' => '0',
    'sort' => '36',
    'options' => 
    array (
      'tax_id' => '',
    ),
    'available' => true,
  ),
  33 => 
  array (
    'id' => '26',
    'type' => 'shipping',
    'plugin' => 'courier',
    'name' => 'Самовывоз',
    'description' => 'г. Воронеж, ул. Пирогова д.15<br>Заказы выдаются строго с 9:00 до 16:30',
    'logo' => '',
    'status' => '0',
    'sort' => '37',
    'options' => 
    array (
      'tax_id' => '',
    ),
    'available' => true,
  ),
  34 => 
  array (
    'id' => '27',
    'type' => 'shipping',
    'plugin' => 'courier',
    'name' => 'Самовывоз',
    'description' => 'г. Иркутск, проезд Удинский д.18<br>Заказы выдаются строго с 9:00 до 16:30',
    'logo' => '',
    'status' => '0',
    'sort' => '38',
    'options' => 
    array (
      'tax_id' => '',
    ),
    'available' => true,
  ),
  35 => 
  array (
    'id' => '28',
    'type' => 'shipping',
    'plugin' => 'courier',
    'name' => 'Самовывоз',
    'description' => 'г. Нижний Новгород, ул. Кузбасская д.1<br>Заказы выдаются строго с 9:00 до 16:30',
    'logo' => '',
    'status' => '0',
    'sort' => '39',
    'options' => 
    array (
      'tax_id' => '',
    ),
    'available' => true,
  ),
  36 => 
  array (
    'id' => '29',
    'type' => 'shipping',
    'plugin' => 'courier',
    'name' => 'Самовывоз',
    'description' => 'г. Омск, ул. Лермонтова д.81<br>Заказы выдаются строго с 9:00 до 16:30',
    'logo' => '',
    'status' => '0',
    'sort' => '40',
    'options' => 
    array (
      'tax_id' => '',
    ),
    'available' => true,
  ),
  37 => 
  array (
    'id' => '30',
    'type' => 'shipping',
    'plugin' => 'courier',
    'name' => 'Самовывоз',
    'description' => 'г. Пенза, ул. Урицкого д.123<br>Заказы выдаются строго с 9:00 до 16:30',
    'logo' => '',
    'status' => '0',
    'sort' => '41',
    'options' => 
    array (
      'tax_id' => '',
    ),
    'available' => true,
  ),
  38 => 
  array (
    'id' => '31',
    'type' => 'shipping',
    'plugin' => 'courier',
    'name' => 'Самовывоз',
    'description' => 'г. Санкт-Петербург, ул. Автомобильная, д.8<br>Заказы выдаются строго с 9:00 до 16:30',
    'logo' => '',
    'status' => '0',
    'sort' => '42',
    'options' => 
    array (
      'tax_id' => '',
    ),
    'available' => true,
  ),
  39 => 
  array (
    'id' => '33',
    'type' => 'shipping',
    'plugin' => 'courier',
    'name' => 'Доставка по Казахстану',
    'description' => 'Доставка ограничивается только определенной страной и регионом. Стоимость доставки рассчитывается на основании итоговой стоимости заказа либо веса отправления.',
    'logo' => '',
    'status' => '0',
    'sort' => '43',
    'options' => 
    array (
      'tax_id' => '',
    ),
    'available' => true,
  ),
  40 => 
  array (
    'id' => '13',
    'type' => 'shipping',
    'plugin' => 'courier',
    'name' => 'Бесплатная доставка до 1 февраля',
    'description' => '',
    'logo' => '',
    'status' => '0',
    'sort' => '44',
    'options' => 
    array (
      'tax_id' => '',
    ),
    'available' => true,
  ),
  41 => 
  array (
    'id' => '37',
    'type' => 'shipping',
    'plugin' => 'courier',
    'name' => 'Курьер по Москве',
    'description' => 'Доставка на указанный в контактной информации адрес',
    'logo' => '',
    'status' => '0',
    'sort' => '45',
    'options' => 
    array (
      'tax_id' => '',
    ),
    'available' => true,
  ),
  42 => 
  array (
    'id' => '41',
    'type' => 'shipping',
    'plugin' => 'courier',
    'name' => 'Курьер по Москве',
    'description' => 'Доставка на указанный в контактной информации адрес',
    'logo' => '',
    'status' => '0',
    'sort' => '49',
    'options' => 
    array (
      'tax_id' => '',
      'assembly_time' => '0',
      'customer_type' => '',
    ),
    'available' => true,
  ),
  43 => 
  array (
    'id' => '45',
    'type' => 'shipping',
    'plugin' => 'courier',
    'name' => 'Самовывоз',
    'description' => 'Московская область, г. Красногорск, Ильинский тупик д.6<br>
Заказы выдаются строго с 9:00 до 16:30 в будние дни',
    'logo' => '',
    'status' => '0',
    'sort' => '52',
    'options' => 
    array (
      'tax_id' => '',
      'assembly_time' => '',
      'customer_type' => '',
    ),
    'available' => true,
  ),
  44 => 
  array (
    'id' => '52',
    'type' => 'shipping',
    'plugin' => 'courier',
    'name' => 'Самовывоз',
    'description' => 'Московская область, г. Красногорск, Ильинский тупик д.6<br>
Заказы выдаются строго с 9:00 до 16:30 в будние дни',
    'logo' => '',
    'status' => '0',
    'sort' => '53',
    'options' => 
    array (
      'tax_id' => '',
      'assembly_time' => '0',
      'customer_type' => '',
    ),
    'available' => true,
  ),
  45 => 
  array (
    'id' => '53',
    'type' => 'shipping',
    'plugin' => 'courier',
    'name' => 'Самовывоз',
    'description' => 'Московская область, г. Красногорск, Ильинский тупик д.6<br>
Заказы выдаются строго с 9:00 до 16:30 в будние дни',
    'logo' => '',
    'status' => '0',
    'sort' => '54',
    'options' => 
    array (
      'tax_id' => '',
      'assembly_time' => '',
      'customer_type' => '',
    ),
    'available' => true,
  ),
  46 => 
  array (
    'id' => '60',
    'type' => 'shipping',
    'plugin' => 'bxb',
    'name' => 'Служба доставки Boxberry',
    'description' => 'Пожалуйста, дождитесь окончания загрузки списка пунктов доставки. Это может занять несколько минут в зависимости от нагрузки на сервер транспортной компании.',
    'logo' => '',
    'status' => '0',
    'sort' => '55',
    'options' => 
    array (
      'tax_id' => '1',
      'assembly_time' => '0',
      'customer_type' => '',
    ),
    'available' => true,
  ),
  47 => 
  array (
    'id' => '67',
    'type' => 'shipping',
    'plugin' => 'boxberry',
    'name' => 'Boxberry - СНГ',
    'description' => 'Расчет стоимости доставки сервисом <a href="https://boxberry.ru/">Boxberry</a>.',
    'logo' => '/wa-plugins/shipping/boxberry/img/boxberry60x32.png',
    'status' => '0',
    'sort' => '56',
    'options' => 
    array (
      'tax_id' => '1',
      'assembly_time' => '3',
      'customer_type' => '',
    ),
    'available' => true,
  ),
);