<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Тест");
$APPLICATION->RestartBuffer();

use Bitrix\Sale\Delivery;
use Bitrix\Main\Page\Asset;
use Bitrix\Main\Localization\Loc;
use Bitrix\Sale\Delivery\Services;
use Bitrix\Sale\Delivery\ExtraServices;
use Bitrix\Currency;


// use \Bitrix\Sale\Helpers\Admin\BusinessValueControl;

\Bitrix\Main\Loader::includeModule('sale');

// require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/sale/prolog.php");
// require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/sale/lib/helpers/admin/businessvalue.php');


include 'deliveries.php';

foreach ($deliveries as $key => $value) {
	$POST = [
	"lang" =>  "ru" ,
	// "ID" =>  "78" ,
	"ID" =>  "0" ,
	"CODE" =>  "" ,
	"PARENT_ID" =>  "0" ,
	"sessid" =>  "5a48af7b6007de2abeac3ba746605980" ,
	"RIGHTS" =>  "YYY" ,
	"autosave_id" =>  "2228a0b7dac0064d603d7b908d2c524f8" ,
	"NAME" =>  $value['name'] ,
	"CLASS_NAME" =>  "\\Bitrix\\Sale\\Delivery\\Services\\Configurable" ,
	"ACTIVE" =>  "N" ,
	"SORT" =>  "10" ,
	"DESCRIPTION" =>  $value['description'] ,
	"PARENT_ID" =>  "0" ,
	"GROUP_NAME" =>  "" ,
	"LOGOTIP" =>  "" ,
	"LOGOTIP_FILE_ID" =>  "" ,
	"CURRENCY" =>  "RUB" ,
	"ALLOW_EDIT_SHIPMENT" =>  "Y" ,
	"VAT_ID" =>  "0" ,
	"XML_ID" =>  $value['id'] ,
	"tabControl_active_tab" =>  "edit_main" ,
	];
	// "CONFIG[MAIN][CURRENCY]" =>  "RUB" ,
	// "CONFIG[MAIN][PRICE]" =>  "0" ,
	// "CONFIG[MAIN][PERIOD][FROM]" =>  "0" ,
	// "CONFIG[MAIN][PERIOD][TO]" =>  "0" ,
	// "CONFIG[MAIN][PERIOD][TYPE]" =>  "D" ,
		$POST['CONFIG'] = array ( 
			'MAIN' => array ( 
				'CURRENCY' => 'RUB', 
				'PRICE' => '0',
				'PERIOD' => ['FROM' => 0, 'TO' => 0, 'TYPE' => 'D']
			) 
		) ;

	// create_delivery($POST);

}

function create_delivery($POST){


$srvStrError = '';
	if(isset($POST["ID"]))             $fields["ID"] = $ID = intval($POST["ID"]);
	if(isset($POST["CODE"]))           $fields["CODE"] = trim($POST["CODE"]);
	if(isset($POST["SORT"]))           $fields["SORT"] = intval($POST["SORT"]);
	if(isset($POST["NAME"]))           $fields["NAME"] = trim($POST["NAME"]);
	if(isset($POST["VAT_ID"]))         $fields["VAT_ID"] = intval($POST["VAT_ID"]);
	if(isset($POST["CONFIG"]))         $fields["CONFIG"] = $POST["CONFIG"];
	if(isset($POST["CURRENCY"]))       $fields["CURRENCY"] = trim($POST["CURRENCY"]);
	if(isset($POST["PARENT_ID"]))      $fields["PARENT_ID"] = intval($POST["PARENT_ID"]);
	if(isset($POST["CLASS_NAME"]))     $fields["CLASS_NAME"] = trim($POST["CLASS_NAME"]);
	if(isset($POST["DESCRIPTION"]))    $fields["DESCRIPTION"] = htmlspecialcharsback(trim($POST["DESCRIPTION"]));
	if(isset($POST["TRACKING_PARAMS"]) && is_array($POST["TRACKING_PARAMS"]))
		$fields["TRACKING_PARAMS"] = $POST["TRACKING_PARAMS"];
	else
		$fields["TRACKING_PARAMS"] = array();

	if(isset($POST["CHANGED_FIELDS"]) && is_array($POST["CHANGED_FIELDS"]))
		$changedFields = $POST["CHANGED_FIELDS"];
	else
		$changedFields = array();

	if(isset($POST["ACTIVE"]) && $POST["ACTIVE"] == "Y")
		$fields["ACTIVE"] = "Y";
	else
		$fields["ACTIVE"] = "N";

	if(isset($POST["XML_ID"]) && $POST["XML_ID"])
		$fields["XML_ID"] = trim($POST["XML_ID"]);
	else
		$fields["XML_ID"] = Services\Manager::generateXmlId();

	if(isset($POST["ALLOW_EDIT_SHIPMENT"]) && $POST["ALLOW_EDIT_SHIPMENT"] == "Y")
		$fields["ALLOW_EDIT_SHIPMENT"] = "Y";
	else
		$fields["ALLOW_EDIT_SHIPMENT"] = "N";


	if($fields["NAME"] == '' )
		$srvStrError .= "SALE_DSE_ERROR_NO_NAME";

	if($fields["CLASS_NAME"] == '' )
		$srvStrError .= "SALE_DSE_ERROR_NO_CLASS_NAME";

		if($srvStrError == '')
		{
			try
			{
				$service = Services\Manager::createObject($fields);

				if($service)
					$fields = $service->prepareFieldsForSaving($fields);
				else
					$srvStrError = 'SALE_DSE_DELIVERY_SERVICE_CREATE_ERROR';
			}
			catch(\Bitrix\Main\SystemException $e)
			{
				$srvStrError = $e->getMessage();
			}

			if($ID > 0 )
				$res = Services\Manager::update($ID, $fields);
			else
				$res = Services\Manager::add($fields);

			global $APPLICATION;
			if($ex = $APPLICATION->getexception())
				echo $ex->getstring();

			// var_dump($res->getId());
			// var_dump($res->issuccess());

			if ($res->isSuccess())
			{
				$ID = $res->getId();
				var_dump($ID);

				if(!$fields["CLASS_NAME"]::isInstalled())
					$fields["CLASS_NAME"]::install();
			}
			else
			{
				$srvStrError .= "SALE_DSE_ERROR_ADD_DELIVERY";
			}
			var_dump($srvStrError);
		}
}