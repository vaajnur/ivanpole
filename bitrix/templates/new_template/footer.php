	<footer class="footer">
		<div class="footer__top">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-lg-4">
						<div class="subscribe">
							<h4 class="footer__title">Подписка</h4>
							<span class="subscribe__text">Оставьте свой e-mail, и мы будем сообщать вам об акциях и новинках</span>
							<?$APPLICATION->IncludeComponent("bitrix:subscribe.form", "subscribe_bottom", Array(
	"USE_PERSONALIZATION" => "Y",	// Определять подписку текущего пользователя
		"PAGE" => "#SITE_DIR#personal/subscribe/subscr_edit.php",	// Страница редактирования подписки (доступен макрос #SITE_DIR#)
		"SHOW_HIDDEN" => "Y",	// Показать скрытые рубрики подписки
		"CACHE_TYPE" => "A",	// Тип кеширования
		"CACHE_TIME" => "3600",	// Время кеширования (сек.)
	),
	false
);?>

							<p class="subscribe__politic">
								Нажимая на кнопку "Подписаться", вы соглашаетесь на получение информационных и/или рекламных сообщений в соответствии с Политикой конфиденциальности.
							</p>
						</div>
					</div>
					<div class="col-md-6 col-lg-3">
						<h4 class="footer__title">Покупателям</h4>
<?
$APPLICATION->IncludeComponent("bitrix:menu", "bottom_menu", Array(
	"ROOT_MENU_TYPE" => "bottom",	// Тип меню для первого уровня
		"MAX_LEVEL" => "1",	// Уровень вложенности меню
		"CHILD_MENU_TYPE" => "bottom",	// Тип меню для остальных уровней
		"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
		"DELAY" => "N",	// Откладывать выполнение шаблона меню
		"ALLOW_MULTI_SELECT" => "Y",	// Разрешить несколько активных пунктов одновременно
		"MENU_CACHE_TYPE" => "N",	// Тип кеширования
		"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
		"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
		"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
		"COMPONENT_TEMPLATE" => "",
		"MENU_THEME" => "site"
	),
	false
);?>
					</div>
					<div class="col-md-6 col-lg-2">
						<h4 class="footer__title">О нас</h4>
						<ul class="footer-menu">
<?
$APPLICATION->IncludeComponent("bitrix:menu", "bottom_menu", Array(
	"ROOT_MENU_TYPE" => "bottom2",	// Тип меню для первого уровня
		"MAX_LEVEL" => "1",	// Уровень вложенности меню
		"CHILD_MENU_TYPE" => "bottom2",	// Тип меню для остальных уровней
		"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
		"DELAY" => "N",	// Откладывать выполнение шаблона меню
		"ALLOW_MULTI_SELECT" => "Y",	// Разрешить несколько активных пунктов одновременно
		"MENU_CACHE_TYPE" => "N",	// Тип кеширования
		"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
		"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
		"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
		"COMPONENT_TEMPLATE" => "",
		"MENU_THEME" => "site"
	),
	false
);?>
						</ul>
					</div>
					<div class="col-md-6 col-lg-3 d-flex align-items-lg-end justify-content-md-end">
						<div class="footer-contacts">
							<?$APPLICATION->IncludeFile(
                                SITE_DIR."include/contactinfo.php",
                                Array(),
                                Array("MODE"=>"html")
                            );
                            ?>
							
							<ul class="socials">
								<li class="socials__item">
									<a href="#">
										<img src="<?=SITE_TEMPLATE_PATH?>/img/icons/socials-telegram.svg" alt="Telegram">
									</a>
								</li>
								<li class="socials__item">
									<a href="#">
										<img src="<?=SITE_TEMPLATE_PATH?>/img/icons/socials-whatsapp.svg" alt="Whatsapp">
									</a>
								</li>
								<li class="socials__item">
									<a href="#">
										<img src="<?=SITE_TEMPLATE_PATH?>/img/icons/socials-instagram.svg" alt="Instagram">
									</a>
								</li>
								<li class="socials__item">
									<a href="#">
										<img src="<?=SITE_TEMPLATE_PATH?>/img/icons/socials-facebook.svg" alt="Facebook">
									</a>
								</li>
								<li class="socials__item">
									<a href="#">
										<img src="<?=SITE_TEMPLATE_PATH?>/img/icons/socials-vk.svg" alt="Vk">
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="footer__bottom">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="footer-bottom-wrap">
							<span class="order-lg-1 order-3">© Иван-Поле, 2020</span>
							<a href="/privacy_policy/" class="site-link order-lg-2 order-1">Пользовательское соглашение и условия</a>
							<a href="/personal_data/" class="site-link order-lg-3 order-2">Обработка персональных данных</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>


</body>
</html>