<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>
<div class=""  id="subscribe-form">
<?
$frame = $this->createFrame("subscribe-form", false)->begin();
?>
	<form class="subscribe-form" action="<?=$arResult["FORM_ACTION"]?>">
		<input type="text"  placeholder="Введите ваш E-mail"  class="subscribe-form__input" name="sf_EMAIL" size="20" value="<?=$arResult["EMAIL"]?>" title="<?=GetMessage("subscr_form_email_title")?>" />
		<button  type="submit"  name="OK" value="1" class="subscribe-form__btn">
			<img src="img/icons/arrow-footer.svg" alt=">">
		</button>
	</form>
<?
$frame->beginStub();
?>
	<form class="subscribe-form" action="<?=$arResult["FORM_ACTION"]?>">
		<input type="text"  placeholder="Введите ваш E-mail" class="subscribe-form__input" name="sf_EMAIL" size="20" value="" title="<?=GetMessage("subscr_form_email_title")?>" />
		<button  type="submit"  name="OK" value="1" class="subscribe-form__btn">
			<img src="img/icons/arrow-footer.svg" alt=">">
		</button>
		<!-- <input type="submit" name="OK" value="<?=GetMessage("subscr_form_button")?>" /> -->
	</form>
<?
$frame->end();
?>
</div>
