<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $item
 * @var array $actualItem
 * @var array $minOffer
 * @var array $itemIds
 * @var array $price
 * @var array $measureRatio
 * @var bool $haveOffers
 * @var bool $showSubscribe
 * @var array $morePhoto
 * @var bool $showSlider
 * @var bool $itemHasDetailUrl
 * @var string $imgTitle
 * @var string $productTitle
 * @var string $buttonSizeClass
 * @var CatalogSectionComponent $component
 */
 
?>
<div class="product__item">
	<div class="stickers">
		<span class="stickers__item stickers__item_new">
			New
		</span>
	</div>
	<a href="<?=$item['DETAIL_PAGE_URL'];?>" class="product__wishlist">
		<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt="heart">
	</a>
	<a href="<?=$item['DETAIL_PAGE_URL'];?>" class="product-images products__image"  data-entity="image-wrapper">
		<div class=""  id="<?=$itemIds['PICT_SLIDER']?>">
			<div class="product-images__item"  id="<?=(isset($item['SECOND_PICT']) ? $itemIds['SECOND_PICT'] : $itemIds['PICT'])?>">
				<img style="width: 255px;height:274px;" src="<?=$item['PREVIEW_PICTURE']['SRC']?>" alt="">
			</div>
			<? foreach ($morePhoto as $key => $photo)
					{
						?>
			<div class="product-images__item  <?=($key == 0 ? 'active' : '')?>">
				<img style="width: 255px;height:274px;" src="<?=$photo['SRC']?>" alt="">
			</div>
			<? }
				?>
		</div>
	</a>
	<?
	// var_dump($item['PROPERTIES']['KALORIY']);
	// var_dump($item['PROPERTIES']['BELKI']);
	// var_dump($item['PROPERTIES']['KHE_NA_100_G']);
	// var_dump($item['PROPERTIES']['UGLEVODY']);
	?>
	<ul class="product-attributes">
		<li class="product-attributes__item">
			<span><?=$item['PROPERTIES']['KALORIY']['VALUE'];?></span>
			<span>ккал</span>
		</li>
		<li class="product-attributes__item">
			<span><?=$item['PROPERTIES']['BELKI']['VALUE'];?></span>
			<span>белки</span>
		</li>
		<li class="product-attributes__item">
			<span><?=$item['PROPERTIES']['KHE_NA_100_G']['VALUE'];?></span>
			<span>хе</span>
		</li>
		<li class="product-attributes__item">
			<span><?=$item['PROPERTIES']['UGLEVODY']['VALUE'];?></span>
			<span>углеводы</span>
		</li>
	</ul>
	<a href="<?=$item['DETAIL_PAGE_URL']?>" class="products__title">
		<?=$productTitle?>
	</a>
	<div class="product__bottom">
		<div class="product-price" data-entity="price-block">
			<?
						if ($arParams['SHOW_OLD_PRICE'] === 'Y')
						{
							?>
			<span class="product-price__old"  id="<?=$itemIds['PRICE_OLD']?>"
								<?=($price['RATIO_PRICE'] >= $price['RATIO_BASE_PRICE'] ? 'style="display: none;"' : '')?>><?=$price['PRINT_RATIO_BASE_PRICE']?></span>
			<?
						}
						?>
			<span class="product-price__new"  id="<?=$itemIds['PRICE']?>"><?
			if (!empty($price))
			{
				if ($arParams['PRODUCT_DISPLAY_MODE'] === 'N' && $haveOffers)
				{
					echo Loc::getMessage(
						'CT_BCI_TPL_MESS_PRICE_SIMPLE_MODE',
						array(
							'#PRICE#' => $price['PRINT_RATIO_PRICE'],
							'#VALUE#' => $measureRatio,
							'#UNIT#' => $minOffer['ITEM_MEASURE']['TITLE']
						)
					);
				}
				else
				{
					echo $price['PRINT_RATIO_PRICE'];
				}
			}
			?></span>
		</div>
		<div class="product-quantity" data-entity="quantity-block">
			<a href="javascript:void(0)" class="product-quantity__left" id="<?=$itemIds['QUANTITY_DOWN']?>"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/slider-left.svg" alt="arrow-left"></a>
			<input type="text" class="product-quantity__number" name="<?=$arParams['PRODUCT_QUANTITY_VARIABLE']?>" value="<?=$measureRatio?>"  id="<?=$itemIds['QUANTITY']?>">
			<!-- <span class="product-quantity__number" name="<?=$arParams['PRODUCT_QUANTITY_VARIABLE']?>" value="<?=$measureRatio?>"  id="<?=$itemIds['QUANTITY']?>"><?=$measureRatio?></span> -->
			<a href="javascript:void(0)" class="product-quantity__right" id="<?=$itemIds['QUANTITY_UP']?>"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/slider-right.svg" alt="arrow-right"></a>
			<!-- <span class="product-item-amount-description-container">
				<span id="<?=$itemIds['QUANTITY_MEASURE']?>">
					<?=$actualItem['ITEM_MEASURE']['TITLE']?>
				</span>
				<span id="<?=$itemIds['PRICE_TOTAL']?>"></span>
			</span> -->
		</div>
		<a href="javascript:void(0)" class="product__cart-mobile">
			<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/mobile-cart.svg" alt="cart">
		</a>
	</div>
	<div class="" data-entity="buttons-block">
		<div id="<?=$itemIds['BASKET_ACTIONS']?>">
			<a href="javascript:void(0)" class="product__buy"  id="<?=$itemIds['BUY_LINK']?>"><?=($arParams['ADD_TO_BASKET_ACTION'] === 'BUY' ? $arParams['MESS_BTN_BUY'] : $arParams['MESS_BTN_ADD_TO_BASKET'])?></a>
		</div>
	</div>
</div>