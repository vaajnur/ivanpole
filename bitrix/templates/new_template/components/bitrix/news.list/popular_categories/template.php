<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="popular-categories popular-categories-desktop">
	<div class="container">
		<div class="row">
			<div class="col-lg-4">
				<div class="left-wrap">
					<h3 class="title-left popular-categories__title">
						Популярные категории товаров
					</h3>
					<a href="/catalog" class="site-link">Посмотреть все</a>
				</div>
			</div>
			<?foreach($arResult["ITEMS"] as $arItem):?>
				<?
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
				?>
					<div class="col-md-6 col-lg-4"  id="<?=$this->GetEditAreaId($arItem['ID']);?>">
						<div class="popular-categories__item">
							<div class="popular-categories__background">
								<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?echo $arItem["NAME"]?>">
							</div>
							<div class="popular-categories__content">
								<a href="#" class="subcategories-title"><?echo $arItem["NAME"]?> <img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/arrow-right.svg" alt="arrow-right"></a>
								<ul class="subcategories">
									<?foreach($arItem["DISPLAY_PROPERTIES"]['SUBSECTIONS']['DISPLAY_VALUE'] as $val):?>
										<li class="subcategories__item">
											<?=$val;?>
										</li>
									<?endforeach;?>
								</ul>	
							</div>
						</div>
					</div>
				<?endforeach;?>		
		</div>
	</div>
</section>