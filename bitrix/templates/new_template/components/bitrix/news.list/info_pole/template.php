<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="info-field">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="section-top">
					<div class="section-top__wrap">
						<h3 class="section-top__title">Инфо-поле</h3>
						<a href="#" class="section-top__link site-link">Перейти в раздел</a>
					</div>
				</div>
			</div>
		</div>
		<div class="info-list">
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
			<a href="#" class="info-list__item"  id="<?=$this->GetEditAreaId($arItem['ID']);?>">
				<div class="info-list__image">
					<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?echo $arItem["NAME"]?>">
				</div>
				<div class="info-list__wrap">
					<div class="info-list__stat">
						<span class="info-list__category"><?=$arItem["DISPLAY_PROPERTIES"]['CATEGORY']['VALUE'];?></span>
						<?
						// var_dump($arItem['TIMESTAMP_X']);
							$new_date = $DB->FormatDate($arItem['TIMESTAMP_X'], CSite::GetDateFormat("SHORT", "ru"), 'DD M YYYY');
							// var_dump($new_date);
							$new_date = FormatDate('d M Y', MakeTimeStamp($arItem['TIMESTAMP_X']));
						?>
						<span class="info-list__date"><?=$new_date;?></span>
					</div>
					<h4 class="info-list__title"><?echo $arItem["NAME"]?></h4>
					<p class="info-list__text"><?echo $arItem["PREVIEW_TEXT"];?></p>
				</div>
			</a>
<?endforeach;?>			
		</div>
	</div>
</section>