<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="main-banner">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="owl-carousel mainbanner">

<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
					<div class="mainbanner__item"  id="<?=$this->GetEditAreaId($arItem['ID']);?>" style="background:url('<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>');background-repeat:no-repeat;background-size:cover;">
						<p class="mainbanner__about"><?=html_entity_decode($arItem["PROPERTIES"]['ABOUT']['VALUE']);?></p>
						<h2 class="mainbanner__h2"><?echo $arItem["NAME"]?></h2>
						<span class="mainbanner__subtitle"><?=$arItem["PROPERTIES"]['SUBTITLE']['VALUE'];?></span>
						<p class="mainbanner__text"><?=html_entity_decode($arItem["PROPERTIES"]['ABOUT2']['VALUE']);?></p>
						<img src="<?=SITE_TEMPLATE_PATH;?>/img/mobile-banner.jpg" alt="Джемы">
						<a href="<?=$arItem["PROPERTIES"]['LINK']['VALUE'];?>" class="btn mainbanner__btn">Купить</a>
					</div>
<?endforeach;?>


				</div>
			</div>
		</div>
	</div>
</section>