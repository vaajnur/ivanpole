<?php
define('STOP_STATISTICS', true);
define('NO_KEEP_STATISTIC', 'Y');
define('NO_AGENT_STATISTIC','Y');
define('DisableEventsCheck', true);
define('BX_SECURITY_SHOW_MESSAGE', true);
define('NOT_CHECK_PERMISSIONS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

use Bitrix\Sale;
use Bitrix\Main\Loader;

Loader::includemodule('sale');

$siteId = isset($_REQUEST['SITE_ID']) && is_string($_REQUEST['SITE_ID']) ? $_REQUEST['SITE_ID'] : '';
$siteId = mb_substr(preg_replace('/[^a-z0-9_]/i', '', $siteId), 0, 2);
if (!empty($siteId) && is_string($siteId))
{
	define('SITE_ID', $siteId);
}


$request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();

$post = $request->getpostlist();
// file_put_contents(__DIR__.'/filename.log', var_export($post, true));

$basket = Bitrix\Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());
if(isset($request['quant']) && isset($request['id'])){
	// 	QUANT + - 
	if ($item = $basket->getItemById($request['id'])) {
	    $item->setField('QUANTITY', $request['quant']);
		$basket->save();
		echo "success update";
	}
}elseif( isset($request['del']) && isset($request['id']) ){
	// DELETE
	$basket->getItemById($request['id'])->delete();
	$basket->save();
	echo "success delete";
}else{
	echo "empty fields!";
}