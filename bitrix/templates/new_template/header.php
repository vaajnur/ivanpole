<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

use \Bitrix\Main\Localization;
use \Bitrix\Main\Page\Asset;
use \Bitrix\Main\Loader;

$Asset = Asset::getInstance();
// $Asset->addString('<link href="'.SITE_DIR.'favicon.ico" rel="shortcut icon"  type="image/x-icon" />');
// $Asset->addCss(SITE_TEMPLATE_PATH.'/css/jquery.css');	
$Asset->addCss(SITE_TEMPLATE_PATH.'/css/main.min.css?2');
$Asset->addJs(SITE_TEMPLATE_PATH."/js/jquery.sticky-sidebar.min.js");
$Asset->addJs(SITE_TEMPLATE_PATH."/js/scripts.min.js?1");
$Asset->addJs(SITE_TEMPLATE_PATH."/libs/bootstrap/js/bootstrap.min.js");

Loader::includemodule('iblock');
?>
<!DOCTYPE html>
<html lang="ru">

<head>

	<meta charset="utf-8">
	<!-- <base href="/"> -->

	<title><?$APPLICATION->ShowTitle()?></title>
	<? $APPLICATION->ShowHead();?>
	<meta name="description" content="">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<!-- Template Basic Images Start -->
	<meta property="og:image" content="path/to/image.jpg">
	<link rel="icon" href="<?=SITE_TEMPLATE_PATH?>/img/favicon/favicon.ico">
	<link rel="apple-touch-icon" sizes="180x180" href="<?=SITE_TEMPLATE_PATH?>/img/favicon/apple-touch-icon-180x180.png">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;600;700;800;900&display=swap" rel="stylesheet">
	<!-- Template Basic Images End -->
	
	<!-- Custom Browsers Color Start -->
	<meta name="theme-color" content="#fff">
	<!-- Custom Browsers Color End -->

<body>
<?=$APPLICATION->ShowPanel()?>
	<header class="header">
		<div id="mobile-menu">
			<div class="mobile-line-wrap">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<div class="mobile-line">
								<div id="menu-close">
									<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/mobile-close.svg" alt="close">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="top-line">
				<div class="container">
					<div class="top-line__wrap">
						<div class="row justify-content-between">
							<div class="col-md-3 col-xl-2">
								<a href="#" class="location line-group">
									<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/pin.svg" alt="Город">
									<span><?=getCity()['city'];?></span>
								</a>
							</div>
							<div class="col-md-9 col-xl-5">
								<div class="d-sm-flex justify-content-end">
									<div class="contacts top-line__contacts">
										<?$APPLICATION->IncludeFile(
			                                SITE_DIR."include/contactsintop.php",
			                                Array(),
			                                Array("MODE"=>"html")
			                            );
			                            ?>
									</div>
								</div>
							</div>
						</div>
					</div>			
				</div>
			</div>
			<div class="top-nav">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-md-12 col-xl-10">
							<nav>
<?
$APPLICATION->IncludeComponent("bitrix:menu", "top_menu", Array(
	"ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
		"MAX_LEVEL" => "1",	// Уровень вложенности меню
		"CHILD_MENU_TYPE" => "top",	// Тип меню для остальных уровней
		"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
		"DELAY" => "N",	// Откладывать выполнение шаблона меню
		"ALLOW_MULTI_SELECT" => "Y",	// Разрешить несколько активных пунктов одновременно
		"MENU_CACHE_TYPE" => "N",	// Тип кеширования
		"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
		"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
		"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
		"COMPONENT_TEMPLATE" => "horizontal_multilevel",
		"MENU_THEME" => "site"
	),
	false
);?>
							</nav>
						</div>
					</div>
				</div>
			</div>
			<div>
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<!-- тип питания -->
							<ul class="mobile-catalog-list">
								<li class="mobile-catalog-list__item">
									<a href="#">Акции</a>
								</li>
								<li class="mobile-catalog-list__item">
									<a href="#">Новинки</a>
								</li>
								<li class="mobile-catalog-list__item">
									<div><span>Тип питания</span>
									<ul>
										<li>
											<a href="#">Снижение веса, похудение</a>
										</li>
										<li>
											<a href="#">Сахар в крови, диета</a>
										</li>
										<li>
											<a href="#">Набор мышечной массы</a>
										</li>
										<li>
											<a href="#">Восстановление</a>
										</li>
										<li>
											<a href="#">Увеличение силы</a>
										</li>
										<li>
											<a href="#">Красота и здоровье</a>
										</li>
										<li>
											<a href="#">Красота и здоровье</a>
										</li>
										<li>
											<a href="#">Красота и здоровье</a>
										</li>
									</ul>
								</li>
							</ul>


						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="main-nav">
			<div class="container">
				<div class="row">
					<div class="col-12 mobile-navs">
						<div class="hamburger-menu" id="menu-btn">
							<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/mobile-hamburger.svg" alt="Меню">
						</div>
						<div class="controls">
							<a href="#" class="controls__item">
								<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/mobile-account.svg" alt="account">
							</a>
							<a href="#" class="wishlist controls__item">
								<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt="Избранное">
								<span class="wishlist__count">
									10
								</span>
							</a>
							<a href="#" class="cart controls__item">
								<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/cart.svg" alt="Корзина">
								<span class="wishlist__count">
									10
								</span>
							</a>
							
						</div>
					</div>
					<div class="col-lg-3 col-xl-2">
						<?$APPLICATION->IncludeFile(
			                                SITE_DIR."include/logo.php",
			                                Array(),
			                                Array("MODE"=>"html")
			                            );
			                            ?>
						
						<a href="#" class="logo-mobile">
							<img src="<?=SITE_TEMPLATE_PATH;?>/img/mobile-logo.svg" alt="Иван Поле">
						</a>
					</div>
					<div class="col-lg-9 col-xl-10">
						<div class="d-flex">
<?$APPLICATION->IncludeComponent("bitrix:search.title", "search_temp", Array(
	"SHOW_INPUT" => "Y",	// Показывать форму ввода поискового запроса
		"INPUT_ID" => "title-search-input",	// ID строки ввода поискового запроса
		"CONTAINER_ID" => "title-search",	// ID контейнера, по ширине которого будут выводиться результаты
		"PRICE_CODE" => array(
			0 => "BASE",
			1 => "RETAIL",
		),
		"PRICE_VAT_INCLUDE" => "Y",
		"PREVIEW_TRUNCATE_LEN" => "150",
		"SHOW_PREVIEW" => "Y",
		"PREVIEW_WIDTH" => "75",
		"PREVIEW_HEIGHT" => "75",
		"CONVERT_CURRENCY" => "Y",
		"CURRENCY_ID" => "RUB",
		"PAGE" => "#SITE_DIR#search/index.php",	// Страница выдачи результатов поиска (доступен макрос #SITE_DIR#)
		"NUM_CATEGORIES" => "3",	// Количество категорий поиска
		"TOP_COUNT" => "10",	// Количество результатов в каждой категории
		"ORDER" => "date",	// Сортировка результатов
		"USE_LANGUAGE_GUESS" => "Y",	// Включить автоопределение раскладки клавиатуры
		"CHECK_DATES" => "Y",	// Искать только в активных по дате документах
		"SHOW_OTHERS" => "Y",	// Показывать категорию "прочее"
		"CATEGORY_0_TITLE" => "Новости",	// Название категории
		"CATEGORY_0" => array(	// Ограничение области поиска
			0 => "iblock_news",
		),
		"CATEGORY_0_iblock_news" => array(	// Искать в информационных блоках типа "iblock_news"
			0 => "all",
		),
		"CATEGORY_1_TITLE" => "Форумы",	// Название категории
		"CATEGORY_1" => array(	// Ограничение области поиска
			0 => "forum",
		),
		"CATEGORY_1_forum" => array(	// Форумы для поиска
			0 => "all",
		),
		"CATEGORY_2_TITLE" => "Каталоги",	// Название категории
		"CATEGORY_2" => array(	// Ограничение области поиска
			0 => "iblock_books",
		),
		"CATEGORY_2_iblock_books" => "all",	// Искать в информационных блоках типа "iblock_books"
		"CATEGORY_OTHERS_TITLE" => "Прочее",	// Название категории
	),
	false
);?>
							<div class="controls main-nav__controls">
								<a href="#" class="wishlist controls__item">
									<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt="Избранное">
									<span class="wishlist__count">
										?
									</span>
								</a>
								<?$APPLICATION->IncludeComponent(
	"bitrix:sale.basket.basket.line", 
	"small_basket", 
	array(
		"PATH_TO_BASKET" => SITE_DIR."personal/cart/",
		"PATH_TO_PERSONAL" => SITE_DIR."personal/",
		"SHOW_PERSONAL_LINK" => "N",
		"SHOW_NUM_PRODUCTS" => "Y",
		"SHOW_TOTAL_PRICE" => "N",
		"SHOW_PRODUCTS" => "Y",
		"POSITION_FIXED" => "N",
		"SHOW_AUTHOR" => "N",
		"PATH_TO_REGISTER" => SITE_DIR."login/",
		"PATH_TO_PROFILE" => SITE_DIR."personal/",
		"COMPONENT_TEMPLATE" => "destiny_only_basket",
		"PATH_TO_ORDER" => SITE_DIR."personal/cart/",
		"SHOW_EMPTY_VALUES" => "N",
		"PATH_TO_AUTHORIZE" => "",
		"SHOW_REGISTRATION" => "N",
		"SHOW_DELAY" => "N",
		"SHOW_NOTAVAIL" => "N",
		"SHOW_IMAGE" => "Y",
		"SHOW_PRICE" => "N",
		"SHOW_SUMMARY" => "N",
		"HIDE_ON_BASKET_PAGES" => "Y"
	),
	false
);?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="header-catalog">
			<div class="container">
				<div class="row">
					<div class="catalog-wrap">
						<input type="checkbox" id="open-menu">
						<label for="open-menu" class="catalog-btn" id="catalog-btn">
							<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/hamburger.svg" alt="Каталог">
							Каталог
						</label>

<!-- ///////////////////// MENU ////////////////////  -->
						<div class="menu-dropdown" id="catalog-menu"> 
							<div class="mobile-tabs">
								<div class="close-catalog">
									<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/close-icon.svg" alt="close">
								</div>
								<!-- <div class="to-top" id="to-top">
									<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/to-top.svg" alt="top">
								</div> -->
								<div class="mobile-tabs__item active" onclick="openTab(event, 'Popular')">
									Популярные продукты
								</div>
								<div class="mobile-tabs__item" onclick="openTab(event, 'Types')">
									Типы питания
								</div>
								<!-- <div class="tab">
									<button class="tablinks" onclick="openCity(event, 'London')">London</button>
									<button class="tablinks" onclick="openCity(event, 'Paris')">Paris</button>
									<button class="tablinks" onclick="openCity(event, 'Tokyo')">Tokyo</button>
								  </div> -->
							</div>
							<div class="mobile-menu">
								<div class="tab-popular tabcontent" id="Popular">
									<ul class="menu-list">
										<li class="menu-list__item">
											<a href="#">Соусы</a>										
										</li>
										<li class="menu-list__item">
											<a href="#">Конфитюры</a>
										</li>
										<li class="menu-list__item">
											<a href="#">Напитки</a>
										</li>
										<li class="menu-list__item">
											<a href="#">Мороженое</a>
										</li>
										<li class="menu-list__item">
											<a href="#">Десерты</a>
										</li>
										<li class="menu-list__item">
											<a href="#">Творчество</a>
										</li>
										<li class="menu-list__item">
											<a href="#">Смеси для выпечки</a>
										</li>
										<li class="menu-list__item">
											<a href="#">Шоколадная паста</a>
										</li>
									</ul>
									
								</div>
								<div class="tab-popular tabcontent" id="Types">
									<ul class="menu-list">
										<li class="menu-list__item">
											<a href="#">Спортивное питание 123</a>
											<ul class="mobile-additional">
												<li><a href="#">Протеин</a></li>
												<li><a href="#">Гейнеры</a></li>
												<li><a href="#">Протеиновые батончики</a></li>
												<li><a href="#">Жиросжигатели</a></li>
												<li><a href="#">Коллаген</a></li>
												<li><a href="#">Аксессуары</a></li>
												<li><a href="#">Аминокислоты</a></li>
												<li><a href="#">Спортивные напитки</a></li>
											</ul>
										</li>
										<li class="menu-list__item">
											<a href="#">Низкокалорийные продукты 456</a>
											<ul class="mobile-additional">
												
												<li><a href="#">Высокобелковые смеси</a></li>
												<li><a href="#">Закваски и пробиотики</a></li>
												<li><a href="#">Высокобелковый завтрак</a></li>
												<li><a href="#">Низкокалорийные соусы</a></li>
												<li><a href="#">Низкокалорийные соусы</a></li>
												<li><a href="#">Шоколадные пасты без сахара</a></li>
											</ul>
										</li>
										<li class="menu-list__item">
											<a href="#">Постные продукты</a>
										</li>
										<li class="menu-list__item">
											<a href="#">Наборы для кулинарного творчества</a>
										</li>
									</ul>
								</div>
							</div>


							<!-- DESKTOP MENU -->
							<?
$res = ciblocksection::getlist(array(), array('IBLOCK_ID' => '4', 'ACTIVE' => 'Y', 'GLOBAL_ACTIVE' => 'Y', 'DEPTH_LEVEL' => '1'), false, array('*'));
$res2 = ciblocksection::getlist(array(), array('IBLOCK_ID' => '4', 'ACTIVE' => 'Y', 'GLOBAL_ACTIVE' => 'Y', 'DEPTH_LEVEL' => '2'), false, array('*'), array('nTopCount' => 10));

							?>
							<ul class="menu-list menu-list-desktop">
								<? while($a = getSectionNames($res)->current()):?>
								<li class="menu-list__item">
									<a href="<?=$a['SECTION_PAGE_URL'];?>"><?=$a['NAME'];?></a>
								</li>
								<? endwhile;?>
							</ul>
							<div class="menu-products">
								<? while($a = getSectionNames($res2)->current()):?>
								<a href="<?=$a['SECTION_PAGE_URL'];?>" class="menu-products__item">
									<img src="<?=$a['pic']['src'];?>" alt="<?=$a['NAME'];?>">
									<span><?=$a['NAME'];?></span>
								</a>
								<? endwhile;?>
							</div>

						</div>
						
<?
$IBLOCK_ID = 4;
$property_enums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>$IBLOCK_ID, "CODE"=>"TYPE_OF_FOOD"));
?>
						<ul class="catalog-list">
							<li class="catalog-list__item">
								<a href="#">Акции</a>
							</li>
							<li class="catalog-list__item">
								<a href="#">Новинки</a>
							</li>
							<li class="catalog-list__item dropdown">
								<div><span>Тип питания</span> <img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/poly-arrow.svg" alt=">" class="dropdown-arrow"></div>
								<ul>
									<?
									do{ 
										if($res1 !== NULL){?>
									<li>
										<a href="#"><?=$res1;?></a>
									</li>
									<? }}while($res1 =  prop_enum_list($property_enums)->current());
									?>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</header>	