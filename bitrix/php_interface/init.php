<?

function pr($arg){
  // $debug = debug_backtrace();
	echo "<pre>";
  // echo 'файл '. $debug[0]['file'] . ':' . $debug[0]['line'];
 // echo "<br>";
	print_r($arg);
	echo "</pre>";
}

function getSectionNames($var){
	while ($ob = $var->Getnext(true, false)) {
		$ob['pic'] = cfile::resizeimageget($ob['PICTURE'], array('width'=>170, 'height'=>150), BX_RESIZE_IMAGE_PROPORTIONAL);
		yield $ob;
	}
}

function prop_enum_list($list){
	while($enum_fields = $list->GetNext())
	{
	  yield $enum_fields["VALUE"];
	}
} 

function getIPYou(){
    if (!empty($_SERVER['HTTP_CLIENT_IP'])){
        //check ip from share internet
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
        //to check ip is pass from proxy
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;    
}

function getCity(){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'http://ip-api.com/json/'.getIPYou() . '?lang=ru');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $city = curl_exec($ch);
    $city = json_decode($city, true);
    curl_close($ch);
    return $city;
}

\Bitrix\Main\EventManager::getInstance()->addEventHandler( 
    'sale', 
    'OnSaleOrderEntitySaved', 
    '\MyClass::onSaleOrderEntitySaved'
); 

class MyClass 
{
    function onSaleOrderEntitySaved(\Bitrix\Main\Event $event)
    {
        foreach($event->getResults() as $previousResult)
            if($previousResult->getType()!=\Bitrix\Main\EventResult::SUCCESS)
                return;
      $order = $event->getParameter("ENTITY");
      $propertyCollection = $order->getPropertyCollection();
      // $ar = $propertyCollection->getArray(); // все сразу
      $addr = $propertyCollection->getAddress();
      if($addr != false){
          // $somePropValue = $propertyCollection->getItemByOrderPropertyId(7); // по id
          $addr_str = $addr->getValue();
          if($addr_str != false){
            $addr_str = preg_replace('%#S+%', '#', $addr_str);
            $addr->setValue($addr_str);
          }
      }
       
        if($order->getId())
            return;      
    }    
}

/*Bitrix\Main\EventManager::getInstance()->addEventHandler('sale', 'onSaleDeliveryServiceCalculate', 'yourHandler');

function yourHandler(\Bitrix\Main\Event $event)
{
    $calcResult = $event->getParameter('RESULT');
    $shipment = $event->getParameter('SHIPMENT');
    
    return new \Bitrix\Main\EventResult(
            \Bitrix\Main\EventResult::ERROR,
            \Bitrix\Sale\ResultError::create(new \Bitrix\Main\Error($calcResult, "MINIMUM_PRICE_ERROR"))
        );  
    
    return new \Bitrix\Main\EventResult(
        \Bitrix\Main\EventResult::SUCCESS,
        array(
            "RESULT" => $calcResult,
        )
    );
}*/


// Bitrix\Main\EventManager::getInstance()->addEventHandler('sale', 'OnSaleComponentOrderOneStepDelivery', 'yourHandler2');
// Bitrix\Main\EventManager::getInstance()->addEventHandler('sale', 'OnSaleComponentOrderOneStepProcess', 'yourHandler2');
// Bitrix\Main\EventManager::getInstance()->addEventHandler('sale', 'OnSaleComponentOrderOneStepPersonType', 'yourHandler2');

/*function yourHandler2(&$arResult, $arUserResult, $arParams)
{
  // file_put_contents(__DIR__.'/filename.LOG', '');
  // file_put_contents(__DIR__.'/filename.LOG', var_export($arResult, true) . PHP_EOL . '====================================' . PHP_EOL , FILE_APPEND);
  // file_put_contents(__DIR__.'/filename.LOG', var_export($arUserResult, true) . PHP_EOL , FILE_APPEND);
  $arResult['DELIVERY']['92']['PRICE'] = 12399;
  $arResult['DELIVERY']['92']['PRICE_FORMATED'] = 12399;
  $arResult['JS_DATA']['DELIVERY']['92']['PRICE'] = 12399;
  // var_dump($arResult['JS_DATA']['DELIVERY']['92']['PRICE'] );
  $arResult['JS_DATA']['DELIVERY']['92']['PRICE_FORMATED'] = 12399;
  $arResult['JS_DATA']['TOTAL']['DELIVERY_PRICE'] = 12399;
  $arResult['DELIVERY_PRICE'] = 12399;
}*/

Bitrix\Main\EventManager::getInstance()->addEventHandler('sale', 'OnSaleComponentOrderResultPrepared', 'yourHandler3');

function yourHandler3($order, $arUserResult, $request, &$arParams, &$arResult)
{
  if(date('H') >= 13){
    // удаляю Доставка курьером сегодня
    unset($arResult['DELIVERY']['2']);
    unset($arResult['JS_DATA']['DELIVERY']['2']);    
  }elseif(date('H') < 13){
    // удаляю Доставка курьером завтра
    unset($arResult['DELIVERY']['138']);
    unset($arResult['JS_DATA']['DELIVERY']['138']);    
  }

}


Bitrix\Main\EventManager::getInstance()->addEventHandler('sale', 'onSaleDeliveryServiceCalculate', 'yourHandler');

function yourHandler(\Bitrix\Main\Event $event)
{
    // $order = $event->getParameter('ENTITY');
    $calcResult = $event->getParameter('RESULT');
    $shipment = $event->getParameter('SHIPMENT');
    $w = $shipment->getWeight();
    $w = $w/1000;;
    $del_id = $shipment->getDeliveryId();
    // var_dump($w);
    // var_dump($del_id);
    $price_cust = false;
    switch ($del_id) {
       // ------------------------- Москва
      case 92:
        if($w > 15 && $w < 20) {
          $price_cust = '450';
        }elseif($w > 20 && $w < 25){
          $price_cust = '500';
        }elseif($w > 25){
          $price_cust = '600';
        }
        break;
        // -------------------------- / МО
      case 141:
        if($w > 11 && $w < 16) {
          $price_cust = '450';
        }elseif($w > 16 && $w < 21){
          $price_cust = '500';
        }elseif($w > 21){
          $price_cust = '600';
        }
        break;
        // ------------------------- / Питер
      case 142:
        if($w > 10 && $w < 15) {
          $price_cust = '450';
        }elseif($w > 15 && $w < 20) {
          $price_cust = '550';
        }elseif($w > 20 && $w < 25){
          $price_cust = '650';
        }elseif($w > 25){
          $price_cust = '700';
        }
        break;      
      default:
        # code...
        break;
    }

    if($price_cust != false)
      $calcResult->setDeliveryPrice($price_cust);


    return new \Bitrix\Main\EventResult(
        \Bitrix\Main\EventResult::SUCCESS,
        array(
            "RESULT" => $calcResult,
        )
    );
}