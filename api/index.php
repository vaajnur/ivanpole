<?
use Bitrix\Main;
use Bitrix\Main\Context;
use Bitrix\Main\Web\Cookie;
use Bitrix\Sale;
use Bitrix\Currency\CurrencyManager;
use Bitrix\Sale\Order;
use Bitrix\Sale\Basket;
use Bitrix\Sale\Delivery;
use Bitrix\Sale\PaySystem;

define("NO_KEEP_STATISTIC", true);
define('STOP_STATISTICS', true);
define("NOT_CHECK_PERMISSIONS", true);

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

header('Content-type: application/json');
global $APPLICATION;
$APPLICATION->SetTitle("api 1.0");
$APPLICATION->RestartBuffer();
global $USER;
if (!is_object($USER)) $USER = new CUser;

define('IBLOCK_ID', 4); 

\Bitrix\Main\Loader::includeModule('iblock');
\Bitrix\Main\Loader::includeModule('catalog');
\Bitrix\Main\Loader::includeModule("sale");

$request = Context::getCurrent()->getRequest();
$response = Context::getCurrent()->getResponse();
$server = Context::getCurrent()->getServer();

function product_format($arProduct){
    global $USER;
    $arMeasure = \Bitrix\Catalog\ProductTable::getCurrentRatioWithMeasure($arProduct["ID"]);    
    $arProduct['MEASURE_NAME'] = $arMeasure[$arProduct["ID"]]['MEASURE']['SYMBOL_RUS'];
    $arProduct['MEASURE_RATIO'] = $arMeasure[$arProduct["ID"]]['RATIO'];
    
    $arPrice = CCatalogProduct::GetOptimalPrice($arProduct["ID"], 1, $USER->GetUserGroupArray(), SITE_ID);
    if (!$arPrice || count($arPrice) <= 0)
    {
        if ($nearestQuantity = CCatalogProduct::GetNearestQuantityPrice($arProduct["ID"], 1, $USER->GetUserGroupArray()))
        {
            $quantity = $nearestQuantity;
            $arPrice = CCatalogProduct::GetOptimalPrice($arProduct["ID"], 1, $USER->GetUserGroupArray(),  SITE_ID);
        }
    }
    
    $arProduct['PRICE'] = $arPrice['RESULT_PRICE']['BASE_PRICE'];
    $arProduct['DISCOUNT_PRICE'] = $arPrice['RESULT_PRICE']['DISCOUNT_PRICE'];
    $arProduct['CURRENCY'] = $arPrice['RESULT_PRICE']['CURRENCY'];
    
    $res = CIBlockElement::GetList(Array(), ['IBLOCK_ID'=>IBLOCK_ID,'ID'=>$arProduct["ID"]], false, false, ["ID","IBLOCK_ID","NAME","PREVIEW_PICTURE","PREVIEW_TEXT","DETAIL_PICTURE","DETAIL_TEXT","CODE","PROPERTY_*"]);
    if($ob = $res->GetNextElement())
    {
        $arFields = $ob->GetFields();     
        
        unset($arFields['SUBSCRIBE_ORIG']);
        unset($arFields['QUANTITY_TRACE_ORIG']);
        unset($arFields['CAN_BUY_ZERO_ORIG']);
        unset($arFields['TYPE']);
        
        
        $arProduct['NAME'] = $arFields["NAME"];
        $arProduct['PREVIEW_PICTURE'] = CFile::GetPath($arFields["PREVIEW_PICTURE"]);
        $arProduct['PREVIEW_TEXT'] = $arFields["PREVIEW_TEXT"];
        $arProduct['DETAIL_PICTURE'] = CFile::GetPath($arFields["DETAIL_PICTURE"]);
        $arProduct['DETAIL_TEXT'] = $arFields["DETAIL_TEXT"];
        $arProduct['CODE'] = $arFields["CODE"];        
        $arProduct['PROPERTIES'] = [];        
        foreach($ob->GetProperties() as $prop){
            if(empty($prop["VALUE"]))continue;
            if(in_array($prop["CODE"],['ZAKUPOCHNAYA_TSENA','ZACHERKNUTAYA_TSENA','NAIMENOVANIE_ARTIKULA','DOSTUPEN_DLYA_ZAKAZA','OSNOVNOY_ARTIKUL','IDENTIFIKATOR_1S','FLEXDISCOUNTMINIMALDISCOUNTCURRENCY','OSNOVNOY_ARTIKUL','V_NALICHII']))continue;
           
            if($prop['MULTIPLE'] == 'Y'){
                $values = [];
                $decriptions = [];
                $db_props = CIBlockElement::GetProperty(IBLOCK_ID, $arProduct["ID"], array("sort" => "asc"), Array("CODE"=>$prop["CODE"]));
                while($ar_props = $db_props->Fetch()){
                    if($prop["PROPERTY_TYPE"] == 'F'){
                        $values[] = CFile::GetPath($ar_props['VALUE']);
                    }else{
                        $values[] = $ar_props['VALUE'];
                    }
                }
            }else{
                $arProduct['PROPERTIES'][$prop["CODE"]] = [
                    "NAME"=>$prop["NAME"],
                    "DESCRIPTION"=>$prop["DESCRIPTION"],
                    "VALUE"=>($prop["PROPERTY_TYPE"] == 'F')?CFile::GetPath($prop['VALUE']):$prop["VALUE"],
                    "CODE"=>$prop["CODE"]
                ];
            }
        }   
    }
    
    
    return $arProduct;
}



/*class BXapi
{
    
    function getUser(){
        
    }
    
    
    function tojson($msg){
        echo json_encode($msg);
    }
    
}*/

/*
if($_SERVER['PHP_AUTH_USER'] != '' && $_SERVER['PHP_AUTH_PW'] != ''){
    global $USER;
    if (!is_object($USER)) $USER = new CUser;
    $arAuthResult = $USER->Login($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW'], "Y");
    if($arAuthResult === true){
    }else{
		header('HTTP/1.0 401 Unauthorized');
        echo "login or password incorrect!";
        die();
    }
}else{
    header('HTTP/1.0 401 Unauthorized');
    echo "login or password incorrect!";
    die();
}
 */


//pr($USER);
switch($request->getQuery("action")){
    //GET user   => error/user   
    case 'user': 
        if($USER->IsAuthorized()){
            if($arUsers = CUser::GetList(($by = "NAME"), ($order = "desc"), ["ID"=>$USER->GetID()])->Fetch()){
                echo json_encode(['user'=>$arUsers]);
            }else{
                echo json_encode(['succes'=>false, 'error'=>'user error']);
            }
        }else{
            echo json_encode(['user'=>false]);
        }
    break;
    //POST авторизация  login pass => error/user
    case 'auth':
        if($USER->IsAuthorized()){            
            echo json_encode(['succes'=>false, 'error'=>'user is authorized']);
        }else{
            if ($request->isPost() && !empty($request->getPost('login')) && !empty($request->getPost('pass'))) {
                $arAuthResult = $USER->Login($request->getPost('login'), $request->getPost('pass'));
                $APPLICATION->arAuthResult = $arAuthResult;
                $success = ($arAuthResult == 1);
                if($success){
                    if($arUsers = CUser::GetList(($by = "NAME"), ($order = "desc"), ["ID"=>$USER->GetID()])->Fetch()){
                        echo json_encode(['succes'=>true, 'user'=>$arUsers]);
                    }else{
                        echo json_encode(['succes'=>false, 'error'=>'user error']);
                    }
                }else{
                    echo json_encode(['succes'=>false, 'error'=>'incorrect login or password']);
                }
            }else{
                echo json_encode(['succes'=>false, 'error'=>'auth error']);
            }
        }
    break;
    // user   => error/user
    case 'catalog':
        //GET каталог   $POST = filter sort_order sort_by page on_page query category   
        
        $arSecFilter = Array('IBLOCK_ID'=>IBLOCK_ID, 'ACTIVE'=>'Y');
        $arSecSelect = ["ID","NAME","CODE","DESCRIPTION","PICTURE","IBLOCK_SECTION_ID"];
        
        $arFilter = ['IBLOCK_ID'=>IBLOCK_ID, "ACTIVE"=>"Y"];
        $arSelect = ["ID","QUANTITY","AVAILABLE","WEIGHT"];
        $arSort = [$request->getQuery('sort_order')?$request->getQuery('sort_order'):"AVAILABLE"=>$request->getQuery('sort_by')?$request->getQuery('sort_by'):"DESC"];        
        $on_page = $request->getQuery('on_page')?$request->getQuery('on_page'):50;
        $page = $request->getQuery('page')?$request->getQuery('page'):1;
        
        
        if ($request->getQuery('category')){
            $arFilter["IBLOCK_SECTION_ID"] = $request->getQuery('category');
            $arFilter["INCLUDE_SUBSECTIONS"] = "Y";  
            $arSecFilter["SECTION_ID"] = $request->getQuery('category');
        }
        if ($request->getQuery('query')){
            
        }
        if ($request->getQuery('filter')){
            
        }        
        
        $arSections= [];
        $arSecFilter = Array('IBLOCK_ID'=>IBLOCK_ID, 'ACTIVE'=>'Y');
        $db_list = CIBlockSection::GetList(Array($by=>$order), $arSecFilter, true, $arSecSelect);
        while($ar_result = $db_list->GetNext())
        {
            
            foreach(['~ID', '~IBLOCK_SECTION_ID', '~NAME', '~CODE', '~DESCRIPTION', '~PICTURE', 'DESCRIPTION_TYPE', '~DESCRIPTION_TYPE', 'TSX_TMP', '~TSX_TMP', '~ELEMENT_CNT'] as $code){
                unset($ar_result[$code]);
            }
            $ar_result["SECTION_ID"] = $ar_result["IBLOCK_SECTION_ID"];
            if(!empty($ar_result["PICTURE"])){
                $ar_result["PICTURE"] = CFile::GetPath($ar_result["PICTURE"]);
            }
            unset($ar_result["IBLOCK_SECTION_ID"]);
            
            $arSections[] = $ar_result;
        }
        
        $arProducts = [];
        $res = CCatalogProduct::GetList($arSort, $arFilter, false, Array("nPageSize"=>$on_page, 'iNumPage'=>$page),$arSelect);
        while($arFields = $res->Fetch())
        {
            $arProducts[] = product_format($arFields);
        }
        
        
        echo json_encode(['category'=>$arSections,'products'=>$arProducts, 'on_page'=>$on_page, 'page'=>$page]);
        
    break;
    case 'product':        
        if(!empty($request->getQuery("id"))){        
            $arFilter = ['IBLOCK_ID'=>IBLOCK_ID, "ID"=>$request->getQuery("id")];
            $arSelect = ["ID","QUANTITY","AVAILABLE","WEIGHT"];
            
            $ind = 0;
            $db_res = CCatalogProduct::GetList(
                [],
                $arFilter,
                false,
                false,
                $arSelect
                );
            if ($ar_res = $db_res->Fetch())
            {                      
                echo json_encode(['product'=>product_format($ar_res)]);
            }else{
                echo json_encode(['error'=>'product not found']);
            }
        }else{
            echo json_encode(['error'=>'product id error']);
        }        
    break;
    case 'favorite': 
        if ($request->isPost()){            
            
        }else{
            
        }
    break;
    case 'cart':
        
        $basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());
        $res = ['count'=>count($basket->getQuantityList()),'final_price'=>$basket->getPrice(),'weight'=>$basket->getWeight(),'price'=>$basket->getBasePrice(),'products'=>[]];
        
        $basketItems = $basket->getBasketItems();
        foreach($basketItems as $item){
            $res['products'][] = ['id'=>$item->getProductId(),"name"=>$item->getField('NAME'),"quantity"=>$item->getQuantity(),"price"=>$item->getPrice(),"final_price"=>$item->getFinalPrice()];
        }
        echo json_encode($res);
        
    break;
    case 'cart-add':
       if ($request->isPost()){
            
            Add2BasketByProductID($request->getPost('product_id'),$request->getPost('quantity'));
            
            $res['succes'] = true;
            echo json_encode($res);
        }
       
        echo json_encode(['error'=>'post only']);
        
        break;
    case 'cart-remove':
        if ($request->isPost()){
            $basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());
            /** @var Sale\BasketItem $basketItem */
            foreach ($basket as $basketItem) {
                if ($basketItem->getProductId() == $request->getPost('product_id')) {
                    $basketItem->setField('QUANTITY', $basketItem->getQuantity() - $request->getPost('quantity'));
                }
            }
            $basket->save();
            $res['succes'] = true;
            echo json_encode($res);
        }
        
        echo json_encode(['error'=>'post only']);
        
        break;
    case 'order-filds':
        if ($request->isPost()){
           
            
            echo json_encode(['error'=>'get only']);
            
           
        }else{
            //order filds
            
            $return = [];
            
            $return['fields'] = [];
            $db_props = CSaleOrderProps::GetList(
                array("SORT" => "ASC"),
                array(
                    "PERSON_TYPE_ID" => 1,
                    "ACTIVE" => 'Y',
                    'UTIL' => 'N',
                ),
                false,
                false,
                array()
                );
            
            while ($props = $db_props->Fetch())
            {
               
                $return['fields'][$props["CODE"]] = ['name' => $props["NAME"],'code' => $props["CODE"],'description' => $props["DESCRIPTION"],'value' => $props["DEFAULT_VALUE_ORIG"],'requied'=> $props["REQUIED"]   ];
            }
            
            /*$return['locations'] = [];
            $db_vars = CSaleLocation::GetList(
                array(
                    "SORT" => "ASC",
                    "COUNTRY_NAME_LANG" => "ASC",
                    "CITY_NAME_LANG" => "ASC"
                ),
                array("LID" => LANGUAGE_ID),
                false,
                false,
                array()
                );
            while ($vars = $db_vars->Fetch()){
                
                $return['locations'][] = $vars;
            }*/
            
            
            
            echo json_encode($return);
        }
    break;
    case 'order':
        if ($request->isPost()){
            $order = Order::create(SITE_ID, $USER->isAuthorized() ? $USER->GetID() : 539);            
            $order->getFields();
            
            $paymentIds = $order->getPaymentSystemId(); // массив id способов оплат
            $deliveryIds = $order->getDeliverySystemId(); // массив id способов доставки*/
        }else{
            echo json_encode(['error'=>'post only']);
        }
        break;
    case 'order-list':
        $db_sales = CSaleOrder::GetList(["DATE_INSERT" => "ASC"], ["USER_ID" => $USER->GetID()]);
        while ($ar_sales = $db_sales->Fetch())
        {           
            echo json_encode(['orders'=>$ar_sales]);
        }
    break;
}

