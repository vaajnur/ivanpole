<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
//$APPLICATION->SetTitle("Персональный раздел");
?>

<!-- Breadcrumbs -->
<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "breadcrumb", Array(
	"PATH" => "",	// Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
		"SITE_ID" => "s1",	// Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
		"START_FROM" => "0",	// Номер пункта, начиная с которого будет построена навигационная цепочка
	),
	false
);?>
<!-- Breadcrumbs end-->

	
	<section class="section-cabinet">
		<div class="container cabinet_desctop">
			<div class="row">
				<div class="col">
					<div class="sect-top">
						<div class="sect-top__wrap">
							<h3 class="sect-top__title">Личный кабинет</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- container -->


		<div class="container cabinet_desctop">
			<div class="row">
				<div class="col">
					<ul class="nav cabinet-tabs nav-tabs" id="cabinet">
						<li class="nav-item tab_active">
							<a class="nav-link active" href="#profile">Профиль</a>
						</li>
						<!-- nav-item -->
						<li class="nav-item">
							<a class="nav-link" href="#orders">Мои заказы</a>
						</li>
						<!-- nav-item -->
						<li class="nav-item">
							<a class="nav-link" href="#bonuses">Бонусы</a>
						</li>
						<!-- nav-item -->
						<li class="nav-item">
							<a class="nav-link" href="#offers">Персональные предложения</a>
						</li>
						<!-- nav-item -->
						<li class="nav-item">
							<a class="nav-link" href="#reviews">Мои отзывы</a>
						</li>
						<!-- nav-item -->
						<li class="nav-item has_slider">
							<a class="nav-link" href="#subscriptions">Подписки</a>
						</li>
						<!-- nav-item -->
					</ul>
					<!-- product-info__tab-content -->

					<div class="tab-content p-2">
						<div class="tab-pane fade show active" id="profile">
						<?$APPLICATION->IncludeComponent("bitrix:main.profile", "user_profile", Array(
							"CHECK_RIGHTS" => "N",	// Проверять права доступа
								"SEND_INFO" => "N",	// Генерировать почтовое событие
								"SET_TITLE" => "N",	// Устанавливать заголовок страницы
								"USER_PROPERTY" => "",	// Показывать доп. свойства
								"USER_PROPERTY_NAME" => "",	// Название закладки с доп. свойствами
								"COMPONENT_TEMPLATE" => ".default"
							),
							false
						);?>
							<?/*<form class="cab-prof">
								<!-- User photo -->
								<label class="cab-prof__photo">
									<input type="file" name="file" hidden>
									<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/cabinet/user-photo.svg" alt="">
								</label>

								<!-- User info -->
								<div class="cab-prof__data">
									<div class="personal__data__wrapper">
										<h5 class="personal__data__title">Личные данные</h5>
										<div class="personal__data__inputs">
											<div class="personal__data__input personal__data__input__name">
												<label for="name">Ф. И .О.*</label>
												<input type="text" required id="name" placeholder="Константинопольский Константин Константинович">
											</div>

											<div class="personal__data__input personal__data__input_gender">
												<span>Пол</span>
												<div>
													<label class="radio__wrapper">
														<div>
															<span>Мужской</span>
															<input type="radio" name="get-method" checked="">
															<span class="radio__checkmark"></span>
														</div>
													</label>

													<label class="radio__wrapper">
														<div>
															<span>Женский</span>
															<input type="radio" name="get-method" checked="">
															<span class="radio__checkmark"></span>
														</div>
													</label>
												</div>
											</div>

											<div class="personal__data__input personal__data__input__tel">
												<label for="tel">Телефон *</label>
												<input type="tel" required id="tel" placeholder="+7 (000) 000 00 00">
											</div>

											<div class="personal__data__input personal__data__input__email">
												<label for="email">E-mail *</label>
												<input type="email" required id="email" placeholder="anyname@anydomain.xyz">
											</div>
										</div>
									</div>

									<div class="personal__data__wrapper">
										<h5 class="personal__data__title">Адрес доставки</h5>

										<div class="personal__data__inputs">
											<div class="personal__data__input personal__data__input__country">
												<label for="country">Страна*</label>
												<input type="text" required id="country" placeholder="Российская Федерация">
											</div>

											<div class="personal__data__input personal__data__input__city">
												<label for="city">Город*</label>
												<input type="text" required id="city" placeholder="Санкт-Петербург">
											</div>

											<div class="personal__data__input personal__data__input__index">
												<label for="index">Индекс*</label>
												<input type="text" required id="index" placeholder="000000">
											</div>

											<div class="personal__data__input personal__data__input__street">
												<label for="street">Улица*</label>
												<input type="text" required id="street" placeholder="Проспект имени Владимира Ильича Ленина ">
											</div>

											<div class="personal__data__input personal__data__input__house">
												<label for="house">Дом/корпус*</label>
												<input type="text" required id="house" placeholder="1234/123">
											</div>

											<div class="personal__data__input personal__data__input__apartment">
												<label for="apartment">Квартира*</label>
												<input type="text" required id="apartment" placeholder="1234">
											</div>

										</div>
									</div>

									<div class="personal__data__wrapper">
										<h5 class="personal__data__title">Пароль</h5>

										<div class="personal__data__inputs personal__data__inputs_pass">
											<div class="personal__data__input">
												<label for="country">Текущий</label>
												<input type="text" id="country" value="+7 (812) 212 85 06">
												<span class="pass-eye open"></span>
											</div>

											<div class="personal__data__input">
												<label for="city">Новый</label>
												<input type="password" id="city" value="123456789">
												<span class="pass-eye"></span>
											</div>

											<div class="personal__data__input">
												<label for="index">Повторите </label>
												<input type="password" id="index" value="123456789">
												<span class="pass-eye"></span>
											</div>

										</div>
									</div>
									<button type="submit" class="cab-prof__btn">Сохранить изменения</button>

								</div>
							</form>*/?>
						</div>
						<!-- tab-pane -->
						<div class="tab-pane fade" id="orders">
							<div class="cab-order">
								<ul class="nav order-tabs nav-tabs_inner" id="cabinet_2">
									<li class="nav-item tab_active">
										<a class="nav-link active" href="#allOrders">Все</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" href="#currentOrders">Текущие</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" href="#completeOrders">Выполненные</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" href="#cancelOrders">Отмененные</a>
									</li>
								</ul>
								<?$APPLICATION->IncludeComponent(
									"bitrix:sale.personal.order", 
									".default", 
									array(
										"ACTIVE_DATE_FORMAT" => "d.m.Y",
										"ALLOW_INNER" => "N",
										"CACHE_GROUPS" => "Y",
										"CACHE_TIME" => "3600",
										"CACHE_TYPE" => "A",
										"CUSTOM_SELECT_PROPS" => array(
										),
										"DETAIL_HIDE_USER_INFO" => array(
											0 => "0",
										),
										"DISALLOW_CANCEL" => "N",
										"HISTORIC_STATUSES" => array(
											0 => "F",
										),
										"NAV_TEMPLATE" => "",
										"ONLY_INNER_FULL" => "N",
										"ORDERS_PER_PAGE" => "20",
										"ORDER_DEFAULT_SORT" => "STATUS",
										"PATH_TO_BASKET" => "/personal/cart",
										"PATH_TO_CATALOG" => "/catalog/",
										"PATH_TO_PAYMENT" => "/personal/order/payment/",
										"PROP_1" => array(
										),
										"PROP_2" => array(
										),
										"REFRESH_PRICES" => "N",
										"RESTRICT_CHANGE_PAYSYSTEM" => array(
											0 => "0",
										),
										"SAVE_IN_SESSION" => "Y",
										"SEF_MODE" => "Y",
										"SET_TITLE" => "Y",
										"STATUS_COLOR_EE" => "gray",
										"STATUS_COLOR_F" => "gray",
										"STATUS_COLOR_GT" => "gray",
										"STATUS_COLOR_HD" => "gray",
										"STATUS_COLOR_HJ" => "gray",
										"STATUS_COLOR_HQ" => "gray",
										"STATUS_COLOR_HY" => "gray",
										"STATUS_COLOR_KI" => "gray",
										"STATUS_COLOR_KO" => "gray",
										"STATUS_COLOR_KS" => "gray",
										"STATUS_COLOR_LA" => "gray",
										"STATUS_COLOR_LC" => "gray",
										"STATUS_COLOR_LK" => "gray",
										"STATUS_COLOR_LO" => "gray",
										"STATUS_COLOR_N" => "green",
										"STATUS_COLOR_NG" => "gray",
										"STATUS_COLOR_NM" => "gray",
										"STATUS_COLOR_OT" => "gray",
										"STATUS_COLOR_P" => "yellow",
										"STATUS_COLOR_PSEUDO_CANCELLED" => "red",
										"STATUS_COLOR_QL" => "gray",
										"COMPONENT_TEMPLATE" => ".default",
										"SEF_FOLDER" => "/personal/",
										"SEF_URL_TEMPLATES" => array(
											"list" => "",
											"detail" => "detail/#ID#",
											"cancel" => "cancel/#ID#",
										)
									),
									false
								);?>

								<?/*<div class="tab-content p-2">
									<div class="tab-pane fade show active" id="allOrders">
										<div class="cab-order__item">
											<ul id="accordion_4">

												<li>
													<div class="head">
														<div class="cab-order__top">
															<div class="cab-order__box">
																<div class="cab-order__status cab-order__status_confirm"><span>№1125005</span>| Подтвержден</div>
																<ul class="cab-order__deal">
																	<li><a href="#!" class="cab-order__cancel">Отменить заказ</a></li>
																	<li><a href="#!" class="cab-order__repeat">Повторить заказ</a></li>
																</ul>
															</div>
															<ul class="cab-order__data">
																<li>
																	<b>Дата создания</b>
																	<span>07.10.2020</span>
																</li>
																<li>
																	<b>Способ доставки</b>
																	<span>СДЭК (ПВЗ: Большая Академическая)</span>
																</li>
																<li>
																	<b>Трек-номер</b>
																	<span>не доступен</span>
																</li>
																<li>
																	<b>Оплата</b>
																	<span>При получении</span>
																</li>
																<li>
																	<b>Сумма</b>
																	<strong>20 642 ₽</strong>
																</li>
															</ul>
														</div>
													</div>

													<div class="hidden">
														<div class="cart__item">
															<div class="cart__wrapper">
																<div class="cart__like-del cart__desctop">
																	<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt=""></a>
																</div>
																<div class="cart__img"><img src="<?=SITE_TEMPLATE_PATH;?>/img/cart/1.png" alt=""></div>
																<div class="cart__info-box">	
																	<div class="cart__info">
																		<div class="cart__heading">
																			<span>АРТ. 30085057</span>
																			<h3>Набор для «Кейк-попс со взрывной карамелью» в белой шоколадной глазури</h3>
																		</div>
																		<div class="cart__number cart__desctop">
																			<input type="number" min="1" max="20" value="1">
																		</div>
																		<div class="cart__price cart__desctop">
																			<del>280 ₽</del>
																			<span>200 ₽</span>
																		</div>
																	</div>
																	<div class="cart__item-total cart__desctop">400 ₽</div> 
																</div> 
															</div>

															<!-- mob-wrap -->
															<div class="cart__mob-wrap">
																<div class="cart__mob-box">
																	<div class="cart__like-del">
																		<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt=""></a>
																	</div>
																	<div class="cart__price">
																		<del>280 ₽</del>
																		<span>200 ₽</span>
																	</div>
																</div>
																<div class="cart__mob-box">
																	<div class="cart__number">
																		<input type="number" min="1" max="20" value="1">
																	</div>
																	<div class="cart__item-total">400 ₽</div> 
																</div>
															</div>
															<!-- mob-wrap end -->
														</div>


														<div class="cart__item">
															<div class="cart__wrapper">
																<div class="cart__like-del cart__desctop">
																	<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt=""></a>
																</div>
																<div class="cart__img"><img src="<?=SITE_TEMPLATE_PATH;?>/img/cart/1.png" alt=""></div>
																<div class="cart__info-box">	
																	<div class="cart__info">
																		<div class="cart__heading">
																			<span>АРТ. 30085057</span>
																			<h3>Набор для «Кейк-попс со взрывной карамелью» в белой шоколадной глазури</h3>
																		</div>
																		<div class="cart__number cart__desctop">
																			<input type="number" min="1" max="20" value="1">
																		</div>
																		<div class="cart__price cart__desctop">
																			<del>280 ₽</del>
																			<span>200 ₽</span>
																		</div>
																	</div>
																	<div class="cart__item-total cart__desctop">400 ₽</div> 
																</div> 
															</div>

															<!-- mob-wrap -->
															<div class="cart__mob-wrap">
																<div class="cart__mob-box">
																	<div class="cart__like-del">
																		<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt=""></a>
																	</div>
																	<div class="cart__price">
																		<del>280 ₽</del>
																		<span>200 ₽</span>
																	</div>
																</div>
																<div class="cart__mob-box">
																	<div class="cart__number">
																		<input type="number" min="1" max="20" value="1">
																	</div>
																	<div class="cart__item-total">400 ₽</div> 
																</div>
															</div>
															<!-- mob-wrap end -->
														</div>
													</div>
												</li>

												<li>
													<div class="head">
														<div class="cab-order__top">
															<div class="cab-order__box">
																<div class="cab-order__status cab-order__status_new"><span>№1125005</span>| Новый</div>
																<ul class="cab-order__deal">
																	<li><a href="#!" class="cab-order__cancel">Отменить заказ</a></li>
																	<li><a href="#!" class="cab-order__repeat">Повторить заказ</a></li>
																</ul>
															</div>
															<ul class="cab-order__data">
																<li>
																	<b>Дата создания</b>
																	<span>07.10.2020</span>
																</li>
																<li>
																	<b>Способ доставки</b>
																	<span>СДЭК (ПВЗ: Большая Академическая)</span>
																</li>
																<li>
																	<b>Трек-номер</b>
																	<span>не доступен</span>
																</li>
																<li>
																	<b>Оплата</b>
																	<span>При получении</span>
																</li>
																<li>
																	<b>Сумма</b>
																	<strong>20 642 ₽</strong>
																</li>
															</ul>
														</div>
													</div>

													<div class="hidden">
														<div class="cart__item">
															<div class="cart__wrapper">
																<div class="cart__like-del cart__desctop">
																	<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt=""></a>
																</div>
																<div class="cart__img"><img src="<?=SITE_TEMPLATE_PATH;?>/img/cart/1.png" alt=""></div>
																<div class="cart__info-box">	
																	<div class="cart__info">
																		<div class="cart__heading">
																			<span>АРТ. 30085057</span>
																			<h3>Набор для «Кейк-попс со взрывной карамелью» в белой шоколадной глазури</h3>
																		</div>
																		<div class="cart__number cart__desctop">
																			<input type="number" min="1" max="20" value="1">
																		</div>
																		<div class="cart__price cart__desctop">
																			<del>280 ₽</del>
																			<span>200 ₽</span>
																		</div>
																	</div>
																	<div class="cart__item-total cart__desctop">400 ₽</div> 
																</div> 
															</div>

															<!-- mob-wrap -->
															<div class="cart__mob-wrap">
																<div class="cart__mob-box">
																	<div class="cart__like-del">
																		<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt=""></a>
																	</div>
																	<div class="cart__price">
																		<del>280 ₽</del>
																		<span>200 ₽</span>
																	</div>
																</div>
																<div class="cart__mob-box">
																	<div class="cart__number">
																		<input type="number" min="1" max="20" value="1">
																	</div>
																	<div class="cart__item-total">400 ₽</div> 
																</div>
															</div>
															<!-- mob-wrap end -->
														</div>

														
														<div class="cart__item">
															<div class="cart__wrapper">
																<div class="cart__like-del cart__desctop">
																	<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt=""></a>
																</div>
																<div class="cart__img"><img src="<?=SITE_TEMPLATE_PATH;?>/img/cart/1.png" alt=""></div>
																<div class="cart__info-box">	
																	<div class="cart__info">
																		<div class="cart__heading">
																			<span>АРТ. 30085057</span>
																			<h3>Набор для «Кейк-попс со взрывной карамелью» в белой шоколадной глазури</h3>
																		</div>
																		<div class="cart__number cart__desctop">
																			<input type="number" min="1" max="20" value="1">
																		</div>
																		<div class="cart__price cart__desctop">
																			<del>280 ₽</del>
																			<span>200 ₽</span>
																		</div>
																	</div>
																	<div class="cart__item-total cart__desctop">400 ₽</div> 
																</div> 
															</div>

															<!-- mob-wrap -->
															<div class="cart__mob-wrap">
																<div class="cart__mob-box">
																	<div class="cart__like-del">
																		<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt=""></a>
																	</div>
																	<div class="cart__price">
																		<del>280 ₽</del>
																		<span>200 ₽</span>
																	</div>
																</div>
																<div class="cart__mob-box">
																	<div class="cart__number">
																		<input type="number" min="1" max="20" value="1">
																	</div>
																	<div class="cart__item-total">400 ₽</div> 
																</div>
															</div>
															<!-- mob-wrap end -->
														</div>
													</div>
												</li>

												<li>
													<div class="head">
														<div class="cab-order__top">
															<div class="cab-order__box">
																<div class="cab-order__status cab-order__status_cancel"><span>№1125005</span>| Отменен</div>
																<ul class="cab-order__deal">
																	<li><a href="#!" class="cab-order__cancel">Отменить заказ</a></li>
																	<li><a href="#!" class="cab-order__repeat">Повторить заказ</a></li>
																</ul>
															</div>
															<ul class="cab-order__data">
																<li>
																	<b>Дата создания</b>
																	<span>07.10.2020</span>
																</li>
																<li>
																	<b>Способ доставки</b>
																	<span>СДЭК (ПВЗ: Большая Академическая)</span>
																</li>
																<li>
																	<b>Трек-номер</b>
																	<span>не доступен</span>
																</li>
																<li>
																	<b>Оплата</b>
																	<span>При получении</span>
																</li>
																<li>
																	<b>Сумма</b>
																	<strong>20 642 ₽</strong>
																</li>
															</ul>
														</div>
													</div>

													<div class="hidden">
														<div class="cart__item">
															<div class="cart__wrapper">
																<div class="cart__like-del cart__desctop">
																	<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt=""></a>
																</div>
																<div class="cart__img"><img src="<?=SITE_TEMPLATE_PATH;?>/img/cart/1.png" alt=""></div>
																<div class="cart__info-box">	
																	<div class="cart__info">
																		<div class="cart__heading">
																			<span>АРТ. 30085057</span>
																			<h3>Набор для «Кейк-попс со взрывной карамелью» в белой шоколадной глазури</h3>
																		</div>
																		<div class="cart__number cart__desctop">
																			<input type="number" min="1" max="20" value="1">
																		</div>
																		<div class="cart__price cart__desctop">
																			<del>280 ₽</del>
																			<span>200 ₽</span>
																		</div>
																	</div>
																	<div class="cart__item-total cart__desctop">400 ₽</div> 
																</div> 
															</div>

															<!-- mob-wrap -->
															<div class="cart__mob-wrap">
																<div class="cart__mob-box">
																	<div class="cart__like-del">
																		<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt=""></a>
																	</div>
																	<div class="cart__price">
																		<del>280 ₽</del>
																		<span>200 ₽</span>
																	</div>
																</div>
																<div class="cart__mob-box">
																	<div class="cart__number">
																		<input type="number" min="1" max="20" value="1">
																	</div>
																	<div class="cart__item-total">400 ₽</div> 
																</div>
															</div>
															<!-- mob-wrap end -->
														</div>

														
														<div class="cart__item">
															<div class="cart__wrapper">
																<div class="cart__like-del cart__desctop">
																	<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt=""></a>
																</div>
																<div class="cart__img"><img src="<?=SITE_TEMPLATE_PATH;?>/img/cart/1.png" alt=""></div>
																<div class="cart__info-box">	
																	<div class="cart__info">
																		<div class="cart__heading">
																			<span>АРТ. 30085057</span>
																			<h3>Набор для «Кейк-попс со взрывной карамелью» в белой шоколадной глазури</h3>
																		</div>
																		<div class="cart__number cart__desctop">
																			<input type="number" min="1" max="20" value="1">
																		</div>
																		<div class="cart__price cart__desctop">
																			<del>280 ₽</del>
																			<span>200 ₽</span>
																		</div>
																	</div>
																	<div class="cart__item-total cart__desctop">400 ₽</div> 
																</div> 
															</div>

															<!-- mob-wrap -->
															<div class="cart__mob-wrap">
																<div class="cart__mob-box">
																	<div class="cart__like-del">
																		<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt=""></a>
																	</div>
																	<div class="cart__price">
																		<del>280 ₽</del>
																		<span>200 ₽</span>
																	</div>
																</div>
																<div class="cart__mob-box">
																	<div class="cart__number">
																		<input type="number" min="1" max="20" value="1">
																	</div>
																	<div class="cart__item-total">400 ₽</div> 
																</div>
															</div>
															<!-- mob-wrap end -->
														</div>
													</div>
												</li>


											</ul>
										</div>
									</div>
									<div class="tab-pane fade" id="currentOrders">
										<div class="cab-order__item">
											<ul id="accordion_4">

												<li>
													<div class="head">
														<div class="cab-order__top">
															<div class="cab-order__box">
																<div class="cab-order__status cab-order__status_cancel"><span>№1125005</span>| Отменен</div>
																<ul class="cab-order__deal">
																	<li><a href="#!" class="cab-order__cancel">Отменить заказ</a></li>
																	<li><a href="#!" class="cab-order__repeat">Повторить заказ</a></li>
																</ul>
															</div>
															<ul class="cab-order__data">
																<li>
																	<b>Дата создания</b>
																	<span>07.10.2020</span>
																</li>
																<li>
																	<b>Способ доставки</b>
																	<span>СДЭК (ПВЗ: Большая Академическая)</span>
																</li>
																<li>
																	<b>Трек-номер</b>
																	<span>не доступен</span>
																</li>
																<li>
																	<b>Оплата</b>
																	<span>При получении</span>
																</li>
																<li>
																	<b>Сумма</b>
																	<strong>20 642 ₽</strong>
																</li>
															</ul>
														</div>
													</div>

													<div class="hidden">
														<div class="cart__item">
															<div class="cart__wrapper">
																<div class="cart__like-del cart__desctop">
																	<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt=""></a>
																</div>
																<div class="cart__img"><img src="<?=SITE_TEMPLATE_PATH;?>/img/cart/1.png" alt=""></div>
																<div class="cart__info-box">	
																	<div class="cart__info">
																		<div class="cart__heading">
																			<span>АРТ. 30085057</span>
																			<h3>Набор для «Кейк-попс со взрывной карамелью» в белой шоколадной глазури</h3>
																		</div>
																		<div class="cart__number cart__desctop">
																			<input type="number" min="1" max="20" value="1">
																		</div>
																		<div class="cart__price cart__desctop">
																			<del>280 ₽</del>
																			<span>200 ₽</span>
																		</div>
																	</div>
																	<div class="cart__item-total cart__desctop">400 ₽</div> 
																</div> 
															</div>

															<!-- mob-wrap -->
															<div class="cart__mob-wrap">
																<div class="cart__mob-box">
																	<div class="cart__like-del">
																		<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt=""></a>
																	</div>
																	<div class="cart__price">
																		<del>280 ₽</del>
																		<span>200 ₽</span>
																	</div>
																</div>
																<div class="cart__mob-box">
																	<div class="cart__number">
																		<input type="number" min="1" max="20" value="1">
																	</div>
																	<div class="cart__item-total">400 ₽</div> 
																</div>
															</div>
															<!-- mob-wrap end -->
														</div>

														
														<div class="cart__item">
															<div class="cart__wrapper">
																<div class="cart__like-del cart__desctop">
																	<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt=""></a>
																</div>
																<div class="cart__img"><img src="<?=SITE_TEMPLATE_PATH;?>/img/cart/1.png" alt=""></div>
																<div class="cart__info-box">	
																	<div class="cart__info">
																		<div class="cart__heading">
																			<span>АРТ. 30085057</span>
																			<h3>Набор для «Кейк-попс со взрывной карамелью» в белой шоколадной глазури</h3>
																		</div>
																		<div class="cart__number cart__desctop">
																			<input type="number" min="1" max="20" value="1">
																		</div>
																		<div class="cart__price cart__desctop">
																			<del>280 ₽</del>
																			<span>200 ₽</span>
																		</div>
																	</div>
																	<div class="cart__item-total cart__desctop">400 ₽</div> 
																</div> 
															</div>

															<!-- mob-wrap -->
															<div class="cart__mob-wrap">
																<div class="cart__mob-box">
																	<div class="cart__like-del">
																		<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt=""></a>
																	</div>
																	<div class="cart__price">
																		<del>280 ₽</del>
																		<span>200 ₽</span>
																	</div>
																</div>
																<div class="cart__mob-box">
																	<div class="cart__number">
																		<input type="number" min="1" max="20" value="1">
																	</div>
																	<div class="cart__item-total">400 ₽</div> 
																</div>
															</div>
															<!-- mob-wrap end -->
														</div>
													</div>
												</li>

											</ul>
										</div>
									</div>
									<div class="tab-pane fade" id="completeOrders">
										<div class="cab-order__item">
											<ul id="accordion_4">

												<li>
													<div class="head">
														<div class="cab-order__top">
															<div class="cab-order__box">
																<div class="cab-order__status cab-order__status_cancel"><span>№1125005</span>| Отменен</div>
																<ul class="cab-order__deal">
																	<li><a href="#!" class="cab-order__cancel">Отменить заказ</a></li>
																	<li><a href="#!" class="cab-order__repeat">Повторить заказ</a></li>
																</ul>
															</div>
															<ul class="cab-order__data">
																<li>
																	<b>Дата создания</b>
																	<span>07.10.2020</span>
																</li>
																<li>
																	<b>Способ доставки</b>
																	<span>СДЭК (ПВЗ: Большая Академическая)</span>
																</li>
																<li>
																	<b>Трек-номер</b>
																	<span>не доступен</span>
																</li>
																<li>
																	<b>Оплата</b>
																	<span>При получении</span>
																</li>
																<li>
																	<b>Сумма</b>
																	<strong>20 642 ₽</strong>
																</li>
															</ul>
														</div>
													</div>

													<div class="hidden">
														<div class="cart__item">
															<div class="cart__wrapper">
																<div class="cart__like-del cart__desctop">
																	<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt=""></a>
																</div>
																<div class="cart__img"><img src="<?=SITE_TEMPLATE_PATH;?>/img/cart/1.png" alt=""></div>
																<div class="cart__info-box">	
																	<div class="cart__info">
																		<div class="cart__heading">
																			<span>АРТ. 30085057</span>
																			<h3>Набор для «Кейк-попс со взрывной карамелью» в белой шоколадной глазури</h3>
																		</div>
																		<div class="cart__number cart__desctop">
																			<input type="number" min="1" max="20" value="1">
																		</div>
																		<div class="cart__price cart__desctop">
																			<del>280 ₽</del>
																			<span>200 ₽</span>
																		</div>
																	</div>
																	<div class="cart__item-total cart__desctop">400 ₽</div> 
																</div> 
															</div>

															<!-- mob-wrap -->
															<div class="cart__mob-wrap">
																<div class="cart__mob-box">
																	<div class="cart__like-del">
																		<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt=""></a>
																	</div>
																	<div class="cart__price">
																		<del>280 ₽</del>
																		<span>200 ₽</span>
																	</div>
																</div>
																<div class="cart__mob-box">
																	<div class="cart__number">
																		<input type="number" min="1" max="20" value="1">
																	</div>
																	<div class="cart__item-total">400 ₽</div> 
																</div>
															</div>
															<!-- mob-wrap end -->
														</div>

														
														<div class="cart__item">
															<div class="cart__wrapper">
																<div class="cart__like-del cart__desctop">
																	<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt=""></a>
																</div>
																<div class="cart__img"><img src="<?=SITE_TEMPLATE_PATH;?>/img/cart/1.png" alt=""></div>
																<div class="cart__info-box">	
																	<div class="cart__info">
																		<div class="cart__heading">
																			<span>АРТ. 30085057</span>
																			<h3>Набор для «Кейк-попс со взрывной карамелью» в белой шоколадной глазури</h3>
																		</div>
																		<div class="cart__number cart__desctop">
																			<input type="number" min="1" max="20" value="1">
																		</div>
																		<div class="cart__price cart__desctop">
																			<del>280 ₽</del>
																			<span>200 ₽</span>
																		</div>
																	</div>
																	<div class="cart__item-total cart__desctop">400 ₽</div> 
																</div> 
															</div>

															<!-- mob-wrap -->
															<div class="cart__mob-wrap">
																<div class="cart__mob-box">
																	<div class="cart__like-del">
																		<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt=""></a>
																	</div>
																	<div class="cart__price">
																		<del>280 ₽</del>
																		<span>200 ₽</span>
																	</div>
																</div>
																<div class="cart__mob-box">
																	<div class="cart__number">
																		<input type="number" min="1" max="20" value="1">
																	</div>
																	<div class="cart__item-total">400 ₽</div> 
																</div>
															</div>
															<!-- mob-wrap end -->
														</div>
													</div>
												</li>

											</ul>
										</div>
									</div>
									<div class="tab-pane fade" id="cancelOrders">
										<div class="cab-order__item">
											<ul id="accordion_4">

												<li>
													<div class="head">
														<div class="cab-order__top">
															<div class="cab-order__box">
																<div class="cab-order__status cab-order__status_cancel"><span>№1125005</span>| Отменен</div>
																<ul class="cab-order__deal">
																	<li><a href="#!" class="cab-order__cancel">Отменить заказ</a></li>
																	<li><a href="#!" class="cab-order__repeat">Повторить заказ</a></li>
																</ul>
															</div>
															<ul class="cab-order__data">
																<li>
																	<b>Дата создания</b>
																	<span>07.10.2020</span>
																</li>
																<li>
																	<b>Способ доставки</b>
																	<span>СДЭК (ПВЗ: Большая Академическая)</span>
																</li>
																<li>
																	<b>Трек-номер</b>
																	<span>не доступен</span>
																</li>
																<li>
																	<b>Оплата</b>
																	<span>При получении</span>
																</li>
																<li>
																	<b>Сумма</b>
																	<strong>20 642 ₽</strong>
																</li>
															</ul>
														</div>
													</div>

													<div class="hidden">
														<div class="cart__item">
															<div class="cart__wrapper">
																<div class="cart__like-del cart__desctop">
																	<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt=""></a>
																</div>
																<div class="cart__img"><img src="<?=SITE_TEMPLATE_PATH;?>/img/cart/1.png" alt=""></div>
																<div class="cart__info-box">	
																	<div class="cart__info">
																		<div class="cart__heading">
																			<span>АРТ. 30085057</span>
																			<h3>Набор для «Кейк-попс со взрывной карамелью» в белой шоколадной глазури</h3>
																		</div>
																		<div class="cart__number cart__desctop">
																			<input type="number" min="1" max="20" value="1">
																		</div>
																		<div class="cart__price cart__desctop">
																			<del>280 ₽</del>
																			<span>200 ₽</span>
																		</div>
																	</div>
																	<div class="cart__item-total cart__desctop">400 ₽</div> 
																</div> 
															</div>

															<!-- mob-wrap -->
															<div class="cart__mob-wrap">
																<div class="cart__mob-box">
																	<div class="cart__like-del">
																		<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt=""></a>
																	</div>
																	<div class="cart__price">
																		<del>280 ₽</del>
																		<span>200 ₽</span>
																	</div>
																</div>
																<div class="cart__mob-box">
																	<div class="cart__number">
																		<input type="number" min="1" max="20" value="1">
																	</div>
																	<div class="cart__item-total">400 ₽</div> 
																</div>
															</div>
															<!-- mob-wrap end -->
														</div>

														
														<div class="cart__item">
															<div class="cart__wrapper">
																<div class="cart__like-del cart__desctop">
																	<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt=""></a>
																</div>
																<div class="cart__img"><img src="<?=SITE_TEMPLATE_PATH;?>/img/cart/1.png" alt=""></div>
																<div class="cart__info-box">	
																	<div class="cart__info">
																		<div class="cart__heading">
																			<span>АРТ. 30085057</span>
																			<h3>Набор для «Кейк-попс со взрывной карамелью» в белой шоколадной глазури</h3>
																		</div>
																		<div class="cart__number cart__desctop">
																			<input type="number" min="1" max="20" value="1">
																		</div>
																		<div class="cart__price cart__desctop">
																			<del>280 ₽</del>
																			<span>200 ₽</span>
																		</div>
																	</div>
																	<div class="cart__item-total cart__desctop">400 ₽</div> 
																</div> 
															</div>

															<!-- mob-wrap -->
															<div class="cart__mob-wrap">
																<div class="cart__mob-box">
																	<div class="cart__like-del">
																		<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt=""></a>
																	</div>
																	<div class="cart__price">
																		<del>280 ₽</del>
																		<span>200 ₽</span>
																	</div>
																</div>
																<div class="cart__mob-box">
																	<div class="cart__number">
																		<input type="number" min="1" max="20" value="1">
																	</div>
																	<div class="cart__item-total">400 ₽</div> 
																</div>
															</div>
															<!-- mob-wrap end -->
														</div>
													</div>
												</li>

											</ul>
										</div>
									</div>
								</div>*/?>
							</div>
						</div>
						<!-- tab-pane -->
						<div class="tab-pane fade" id="bonuses">
							<div class="cab-bonuse">

								<ul id="accordion_4">
									<li>
										<div class="head">
											<img src="<?=SITE_TEMPLATE_PATH;?>/img/bonuses.png" alt="">
											<div class="cab-bonuse__program"><span>Бонусная программа</span></div>
										</div>

										<div class="hidden">
											<div class="cab-bonuse__text">
												<p><b>Lorem ipsum dolor</b> sit amet, consectetur adipiscing elit. Imperdiet tortor facilisi nibh vitae tellus nunc. Pretium proin aliquet curabitur sed a ac. Augue elementum, odio id viverra lectus eget quis vel fames. Risus nisi dictum mauris tincidunt vestibulum leo nunc arcu.</p>
												<p>Mauris cursus dignissim lacus scelerisque ornare morbi. Magna pulvinar id mollis <b>risus eget maecenas</b>  fermentum vulputate. Magnis vel arcu in nisi leo. Auctor urna, id non lorem feugiat cras scelerisque cras. Mi ut morbi maecenas duis sed semper fusce. Porta adipiscing augue commodo, pellentesque id egestas vivamus quis. Quis vel elit nibh in metus. Tristique donec eu, nunc scelerisque purus dignissim arcu malesuada. Vestibulum fringilla ut mauris varius senectus pharetra. Lobortis libero, convallis tellus, lectus. Adipiscing morbi aliquet a pretium ac. In pharetra adipiscing ut aliquam magna in. Consequat, at viverra id pellentesque.</p>
												<p>Facilisi fermentum et mi eget nibh viverra sem. Risus interdum et, lectus sagittis <a href="#!">lobortis nunc</a> , rhoncus. Blandit ultrices varius ornare sit vitae. Fringilla sed quis sapien, gravida in sit egestas adipiscing enim. Vulputate bibendum bibendum quis ultrices duis. Tellus commodo in viverra ut volutpat metus sagittis, amet. Tempus, nulla mus suspendisse ornare nibh. Quisque eget purus, in commodo ultrices augue. Purus non in ipsum, eu dolor lobortis consequat sit convallis. Nibh orci, vel aliquam vel facilisis platea. Pharetra, sit semper urna at. Varius lacus adipiscing a aliquam. Morbi id vestibulum porta lorem in sit ante enim. Eget neque, magna tempor nulla diam malesuada orci. Cras dignissim iaculis nunc adipiscing.</p>
											</div>
										</div>
									</li>

								</ul>

								<div class="cab-bonuse__history">
									<div class="sect-top">
										<div class="sect-top__wrap">
											<h3 class="sect-top__title">История зачислений</h3>
										</div>
									</div>
									<div class="cab-bonuse__table-item">
										<ul class="cab-bonuse__table">
											<li class="cab-bonuse__item">
												<ul>
													<li>Дата зачисления</li>
													<li>07.10.2020</li>
													<li>07.10.2020</li>
													<li>07.10.2020</li>
												</ul>
											</li>

											<li class="cab-bonuse__item">
												<ul>
													<li>Заказ</li>
													<li>№26425821</li>
													<li>№26425821</li>
													<li>№26425821</li>
												</ul>
											</li>

											<li class="cab-bonuse__item">
												<ul>
													<li>Действие</li>
													<li>Начислены баллы за покупку</li>
													<li>Начислены баллы за покупку</li>
													<li>Списаны устаревшие баллы за участие в акции</li>
												</ul>
											</li>

											<li class="cab-bonuse__item">
												<ul>
													<li>Баллы</li>
													<li>+ 200</li>
													<li>+ 120</li>
													<li>- 50</li>
												</ul>
											</li>
										</ul >
										<div class="cab-bonuse__total">Всего: <span>170</span></div>
									</div>
								</div>
							</div>
							<!-- cab-bonuse -->
						</div>
						<!-- tab-pane -->
						<div class="tab-pane fade" id="offers">
							<div class="cab-offers">
								<div class="sect-top">
									<div class="sect-top__wrap">
										<h3 class="sect-top__title">Товары со скидкой для вас</h3>
									</div>
								</div>

								<div class="cab-offers-products" id="CabOffersProductsSale">
									<div class="product__item">
										<div class="stickers">
											<span class="stickers__item stickers__item_new">
												New
											</span>
											<span class="stickers__item stickers__item_calc">1+4=3</span>
										</div>
										<a href="#" class="product__wishlist">
											<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt="heart">
										</a>
										<a href="#" class="product-images products__image">
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-3.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-2.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
										</a>
										<ul class="product-attributes">
											<li class="product-attributes__item">
												<span>150</span>
												<span>ккал</span>
											</li>
											<li class="product-attributes__item">
												<span>4</span>
												<span>белки</span>
											</li>
											<li class="product-attributes__item">
												<span>0,3</span>
												<span>хе</span>
											</li>
											<li class="product-attributes__item">
												<span>11</span>
												<span>углеводы</span>
											</li>
										</ul>
										<a href="#" class="products__title">
											Мармелад «венское кафе»со вкусом карамели, 180 г
										</a>
										<div class="product__bottom">
											<div class="product-price">
												<span class="product-price__old">400 ₽</span>
												<span class="product-price__new">200 ₽</span>
											</div>
											<div class="product-quantity">
												<a href="#" class="product-quantity__left"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/slider-left.svg" alt="arrow-left"></a>
												<span class="product-quantity__number">2</span>
												<a href="#" class="product-quantity__right"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/slider-right.svg" alt="arrow-right"></a>
											</div>
											<a href="#" class="product__cart-mobile">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/mobile-cart.svg" alt="cart">
											</a>
										</div>
										<a href="#" class="product__buy">В корзину</a>
									</div>
									<!-- product__item -->
									<div class="product__item">
										<div class="stickers">
											<span class="stickers__item stickers__item_new">
												New
											</span>
											<span class="stickers__item stickers__item_calc">1+4=3</span>

											<span class="stickers__item stickers__item_count">
												<b>осталось</b>
												<div class="the-final-countdown">
													<p></p>
												</div>
											</span>
										</div>
										<a href="#" class="product__wishlist">
											<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt="heart">
										</a>
										<a href="#" class="product-images products__image">
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-2.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-3.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
										</a>
										<ul class="product-attributes">
											<li class="product-attributes__item">
												<span>150</span>
												<span>ккал</span>
											</li>
											<li class="product-attributes__item">
												<span>4</span>
												<span>белки</span>
											</li>
											<li class="product-attributes__item">
												<span>0,3</span>
												<span>хе</span>
											</li>
											<li class="product-attributes__item">
												<span>11</span>
												<span>углеводы</span>
											</li>
										</ul>
										<a href="#" class="products__title">
											Мармелад «венское кафе»со вкусом карамели, 180 г
										</a>
										<div class="product__bottom">
											<div class="product-price">
												<span class="product-price__old">400 ₽</span>
												<span class="product-price__new">200 ₽</span>
											</div>
											<div class="product-quantity">
												<a href="#" class="product-quantity__left"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/slider-left.svg" alt="arrow-left"></a>
												<span class="product-quantity__number">2</span>
												<a href="#" class="product-quantity__right"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/slider-right.svg" alt="arrow-right"></a>
											</div>
											<a href="#" class="product__cart-mobile">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/mobile-cart.svg" alt="cart">
											</a>
										</div>
										<a href="#" class="product__buy">В корзину</a>
									</div>
									<!-- product__item -->
									<div class="product__item">
										<div class="stickers">
											<span class="stickers__item stickers__item_new">
												New
											</span>
											<span class="stickers__item stickers__item_calc">1+4=3</span>
										</div>
										<a href="#" class="product__wishlist">
											<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt="heart">
										</a>
										<a href="#" class="product-images products__image">
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-2.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-3.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
										</a>
										<ul class="product-attributes">
											<li class="product-attributes__item">
												<span>150</span>
												<span>ккал</span>
											</li>
											<li class="product-attributes__item">
												<span>4</span>
												<span>белки</span>
											</li>
											<li class="product-attributes__item">
												<span>0,3</span>
												<span>хе</span>
											</li>
											<li class="product-attributes__item">
												<span>11</span>
												<span>углеводы</span>
											</li>
										</ul>
										<a href="#" class="products__title">
											Мармелад «венское кафе»со вкусом карамели, 180 г
										</a>
										<div class="product__bottom">
											<div class="product-price">
												<span class="product-price__old">400 ₽</span>
												<span class="product-price__new">200 ₽</span>
											</div>
											<div class="product-quantity">
												<a href="#" class="product-quantity__left"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/slider-left.svg" alt="arrow-left"></a>
												<span class="product-quantity__number">2</span>
												<a href="#" class="product-quantity__right"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/slider-right.svg" alt="arrow-right"></a>
											</div>
											<a href="#" class="product__cart-mobile">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/mobile-cart.svg" alt="cart">
											</a>
										</div>
										<a href="#" class="product__buy">В корзину</a>
									</div>
									<!-- product__item -->
									<div class="product__item">
										<div class="stickers">
											<span class="stickers__item stickers__item_new">
												New
											</span>
											<span class="stickers__item stickers__item_calc">1+4=3</span>
										</div>
										<a href="#" class="product__wishlist">
											<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt="heart">
										</a>
										<a href="#" class="product-images products__image">
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-2.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-3.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
										</a>
										<ul class="product-attributes">
											<li class="product-attributes__item">
												<span>150</span>
												<span>ккал</span>
											</li>
											<li class="product-attributes__item">
												<span>4</span>
												<span>белки</span>
											</li>
											<li class="product-attributes__item">
												<span>0,3</span>
												<span>хе</span>
											</li>
											<li class="product-attributes__item">
												<span>11</span>
												<span>углеводы</span>
											</li>
										</ul>
										<a href="#" class="products__title">
											Мармелад «венское кафе»со вкусом карамели, 180 г
										</a>
										<div class="product__bottom">
											<div class="product-price">
												<span class="product-price__old">400 ₽</span>
												<span class="product-price__new">200 ₽</span>
											</div>
											<div class="product-quantity">
												<a href="#" class="product-quantity__left"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/slider-left.svg" alt="arrow-left"></a>
												<span class="product-quantity__number">2</span>
												<a href="#" class="product-quantity__right"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/slider-right.svg" alt="arrow-right"></a>
											</div>
											<a href="#" class="product__cart-mobile">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/mobile-cart.svg" alt="cart">
											</a>
										</div>
										<a href="#" class="product__buy">В корзину</a>
									</div>
									<!-- product__item -->
									<div class="product__item">
										<div class="stickers">
											<span class="stickers__item stickers__item_new">
												New
											</span>
											<span class="stickers__item stickers__item_calc">1+4=3</span>

											<span class="stickers__item stickers__item_count">
												<b>осталось</b>
												<div class="the-final-countdown">
													<p></p>
												</div>
											</span>
										</div>
										<a href="#" class="product__wishlist">
											<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt="heart">
										</a>
										<a href="#" class="product-images products__image">
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-2.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-3.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
										</a>
										<ul class="product-attributes">
											<li class="product-attributes__item">
												<span>150</span>
												<span>ккал</span>
											</li>
											<li class="product-attributes__item">
												<span>4</span>
												<span>белки</span>
											</li>
											<li class="product-attributes__item">
												<span>0,3</span>
												<span>хе</span>
											</li>
											<li class="product-attributes__item">
												<span>11</span>
												<span>углеводы</span>
											</li>
										</ul>
										<a href="#" class="products__title">
											Мармелад «венское кафе»со вкусом карамели, 180 г
										</a>
										<div class="product__bottom">
											<div class="product-price">
												<span class="product-price__old">400 ₽</span>
												<span class="product-price__new">200 ₽</span>
											</div>
											<div class="product-quantity">
												<a href="#" class="product-quantity__left"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/slider-left.svg" alt="arrow-left"></a>
												<span class="product-quantity__number">2</span>
												<a href="#" class="product-quantity__right"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/slider-right.svg" alt="arrow-right"></a>
											</div>
											<a href="#" class="product__cart-mobile">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/mobile-cart.svg" alt="cart">
											</a>
										</div>
										<a href="#" class="product__buy">В корзину</a>
									</div>
									<!-- product__item -->
									<div class="product__item">
										<div class="stickers">
											<span class="stickers__item stickers__item_new">
												New
											</span>
											<span class="stickers__item stickers__item_calc">1+4=3</span>
										</div>
										<a href="#" class="product__wishlist">
											<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt="heart">
										</a>
										<a href="#" class="product-images products__image">
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-2.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-3.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
										</a>
										<ul class="product-attributes">
											<li class="product-attributes__item">
												<span>150</span>
												<span>ккал</span>
											</li>
											<li class="product-attributes__item">
												<span>4</span>
												<span>белки</span>
											</li>
											<li class="product-attributes__item">
												<span>0,3</span>
												<span>хе</span>
											</li>
											<li class="product-attributes__item">
												<span>11</span>
												<span>углеводы</span>
											</li>
										</ul>
										<a href="#" class="products__title">
											Мармелад «венское кафе»со вкусом карамели, 180 г
										</a>
										<div class="product__bottom">
											<div class="product-price">
												<span class="product-price__old">400 ₽</span>
												<span class="product-price__new">200 ₽</span>
											</div>
											<div class="product-quantity">
												<a href="#" class="product-quantity__left"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/slider-left.svg" alt="arrow-left"></a>
												<span class="product-quantity__number">2</span>
												<a href="#" class="product-quantity__right"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/slider-right.svg" alt="arrow-right"></a>
											</div>
											<a href="#" class="product__cart-mobile">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/mobile-cart.svg" alt="cart">
											</a>
										</div>
										<a href="#" class="product__buy">В корзину</a>
									</div>
									<!-- product__item -->
									<div class="product__item">
										<div class="stickers">
											<span class="stickers__item stickers__item_new">
												New
											</span>
											<span class="stickers__item stickers__item_calc">1+4=3</span>
										</div>
										<a href="#" class="product__wishlist">
											<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt="heart">
										</a>
										<a href="#" class="product-images products__image">
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-2.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-3.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
										</a>
										<ul class="product-attributes">
											<li class="product-attributes__item">
												<span>150</span>
												<span>ккал</span>
											</li>
											<li class="product-attributes__item">
												<span>4</span>
												<span>белки</span>
											</li>
											<li class="product-attributes__item">
												<span>0,3</span>
												<span>хе</span>
											</li>
											<li class="product-attributes__item">
												<span>11</span>
												<span>углеводы</span>
											</li>
										</ul>
										<a href="#" class="products__title">
											Мармелад «венское кафе»со вкусом карамели, 180 г
										</a>
										<div class="product__bottom">
											<div class="product-price">
												<span class="product-price__old">400 ₽</span>
												<span class="product-price__new">200 ₽</span>
											</div>
											<div class="product-quantity">
												<a href="#" class="product-quantity__left"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/slider-left.svg" alt="arrow-left"></a>
												<span class="product-quantity__number">2</span>
												<a href="#" class="product-quantity__right"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/slider-right.svg" alt="arrow-right"></a>
											</div>
											<a href="#" class="product__cart-mobile">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/mobile-cart.svg" alt="cart">
											</a>
										</div>
										<a href="#" class="product__buy">В корзину</a>
									</div>
									<!-- product__item -->
									<div class="product__item">
										<div class="stickers">
											<span class="stickers__item stickers__item_new">
												New
											</span>
											<span class="stickers__item stickers__item_calc">1+4=3</span>
										</div>
										<a href="#" class="product__wishlist">
											<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt="heart">
										</a>
										<a href="#" class="product-images products__image">
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-2.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-3.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
										</a>
										<ul class="product-attributes">
											<li class="product-attributes__item">
												<span>150</span>
												<span>ккал</span>
											</li>
											<li class="product-attributes__item">
												<span>4</span>
												<span>белки</span>
											</li>
											<li class="product-attributes__item">
												<span>0,3</span>
												<span>хе</span>
											</li>
											<li class="product-attributes__item">
												<span>11</span>
												<span>углеводы</span>
											</li>
										</ul>
										<a href="#" class="products__title">
											Мармелад «венское кафе»со вкусом карамели, 180 г
										</a>
										<div class="product__bottom">
											<div class="product-price">
												<span class="product-price__old">400 ₽</span>
												<span class="product-price__new">200 ₽</span>
											</div>
											<div class="product-quantity">
												<a href="#" class="product-quantity__left"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/slider-left.svg" alt="arrow-left"></a>
												<span class="product-quantity__number">2</span>
												<a href="#" class="product-quantity__right"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/slider-right.svg" alt="arrow-right"></a>
											</div>
											<a href="#" class="product__cart-mobile">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/mobile-cart.svg" alt="cart">
											</a>
										</div>
										<a href="#" class="product__buy">В корзину</a>
									</div>
									<!-- product__item -->
								</div>
								<!-- cab-offers-products_sale -->

								<div class="sect-top">
									<div class="sect-top__wrap">
										<h3 class="sect-top__title">Советуем попробовать</h3>
									</div>
								</div>

								<div class="cab-offers-products" id="CabOffersProductsRecommend">
									<div class="product__item">
										<div class="stickers">
											<span class="stickers__item stickers__item_new">
												New
											</span>
											<span class="stickers__item stickers__item_calc">1+4=3</span>
										</div>
										<a href="#" class="product__wishlist">
											<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt="heart">
										</a>
										<a href="#" class="product-images products__image">
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-2.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-3.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
										</a>
										<ul class="product-attributes">
											<li class="product-attributes__item">
												<span>150</span>
												<span>ккал</span>
											</li>
											<li class="product-attributes__item">
												<span>4</span>
												<span>белки</span>
											</li>
											<li class="product-attributes__item">
												<span>0,3</span>
												<span>хе</span>
											</li>
											<li class="product-attributes__item">
												<span>11</span>
												<span>углеводы</span>
											</li>
										</ul>
										<a href="#" class="products__title">
											Мармелад «венское кафе»со вкусом карамели, 180 г
										</a>
										<div class="product__bottom">
											<div class="product-price">
												<span class="product-price__old">400 ₽</span>
												<span class="product-price__new">200 ₽</span>
											</div>
											<div class="product-quantity">
												<a href="#" class="product-quantity__left"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/slider-left.svg" alt="arrow-left"></a>
												<span class="product-quantity__number">2</span>
												<a href="#" class="product-quantity__right"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/slider-right.svg" alt="arrow-right"></a>
											</div>
											<a href="#" class="product__cart-mobile">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/mobile-cart.svg" alt="cart">
											</a>
										</div>
										<a href="#" class="product__buy">В корзину</a>
									</div>
									<!-- product__item -->
									<div class="product__item">
										<div class="stickers">
											<span class="stickers__item stickers__item_new">
												New
											</span>
											<span class="stickers__item stickers__item_calc">1+4=3</span>
										</div>
										<a href="#" class="product__wishlist">
											<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt="heart">
										</a>
										<a href="#" class="product-images products__image">
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-2.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-3.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
										</a>
										<ul class="product-attributes">
											<li class="product-attributes__item">
												<span>150</span>
												<span>ккал</span>
											</li>
											<li class="product-attributes__item">
												<span>4</span>
												<span>белки</span>
											</li>
											<li class="product-attributes__item">
												<span>0,3</span>
												<span>хе</span>
											</li>
											<li class="product-attributes__item">
												<span>11</span>
												<span>углеводы</span>
											</li>
										</ul>
										<a href="#" class="products__title">
											Мармелад «венское кафе»со вкусом карамели, 180 г
										</a>
										<div class="product__bottom">
											<div class="product-price">
												<span class="product-price__old">400 ₽</span>
												<span class="product-price__new">200 ₽</span>
											</div>
											<div class="product-quantity">
												<a href="#" class="product-quantity__left"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/slider-left.svg" alt="arrow-left"></a>
												<span class="product-quantity__number">2</span>
												<a href="#" class="product-quantity__right"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/slider-right.svg" alt="arrow-right"></a>
											</div>
											<a href="#" class="product__cart-mobile">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/mobile-cart.svg" alt="cart">
											</a>
										</div>
										<a href="#" class="product__buy">В корзину</a>
									</div>
									<!-- product__item -->
									<div class="product__item">
										<div class="stickers">
											<span class="stickers__item stickers__item_new">
												New
											</span>
											<span class="stickers__item stickers__item_calc">1+4=3</span>

											<span class="stickers__item stickers__item_count">
												<b>осталось</b>
												<div class="the-final-countdown">
													<p></p>
												</div>
											</span>
										</div>
										<a href="#" class="product__wishlist">
											<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt="heart">
										</a>
										<a href="#" class="product-images products__image">
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-2.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-3.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
										</a>
										<ul class="product-attributes">
											<li class="product-attributes__item">
												<span>150</span>
												<span>ккал</span>
											</li>
											<li class="product-attributes__item">
												<span>4</span>
												<span>белки</span>
											</li>
											<li class="product-attributes__item">
												<span>0,3</span>
												<span>хе</span>
											</li>
											<li class="product-attributes__item">
												<span>11</span>
												<span>углеводы</span>
											</li>
										</ul>
										<a href="#" class="products__title">
											Мармелад «венское кафе»со вкусом карамели, 180 г
										</a>
										<div class="product__bottom">
											<div class="product-price">
												<span class="product-price__old">400 ₽</span>
												<span class="product-price__new">200 ₽</span>
											</div>
											<div class="product-quantity">
												<a href="#" class="product-quantity__left"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/slider-left.svg" alt="arrow-left"></a>
												<span class="product-quantity__number">2</span>
												<a href="#" class="product-quantity__right"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/slider-right.svg" alt="arrow-right"></a>
											</div>
											<a href="#" class="product__cart-mobile">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/mobile-cart.svg" alt="cart">
											</a>
										</div>
										<a href="#" class="product__buy">В корзину</a>
									</div>
									<!-- product__item -->
									<div class="product__item">
										<div class="stickers">
											<span class="stickers__item stickers__item_new">
												New
											</span>
											<span class="stickers__item stickers__item_calc">1+4=3</span>
										</div>
										<a href="#" class="product__wishlist">
											<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt="heart">
										</a>
										<a href="#" class="product-images products__image">
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-2.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-3.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
										</a>
										<ul class="product-attributes">
											<li class="product-attributes__item">
												<span>150</span>
												<span>ккал</span>
											</li>
											<li class="product-attributes__item">
												<span>4</span>
												<span>белки</span>
											</li>
											<li class="product-attributes__item">
												<span>0,3</span>
												<span>хе</span>
											</li>
											<li class="product-attributes__item">
												<span>11</span>
												<span>углеводы</span>
											</li>
										</ul>
										<a href="#" class="products__title">
											Мармелад «венское кафе»со вкусом карамели, 180 г
										</a>
										<div class="product__bottom">
											<div class="product-price">
												<span class="product-price__old">400 ₽</span>
												<span class="product-price__new">200 ₽</span>
											</div>
											<div class="product-quantity">
												<a href="#" class="product-quantity__left"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/slider-left.svg" alt="arrow-left"></a>
												<span class="product-quantity__number">2</span>
												<a href="#" class="product-quantity__right"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/slider-right.svg" alt="arrow-right"></a>
											</div>
											<a href="#" class="product__cart-mobile">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/mobile-cart.svg" alt="cart">
											</a>
										</div>
										<a href="#" class="product__buy">В корзину</a>
									</div>
									<!-- product__item -->
								</div>
								<!-- cab-offers-products_sale -->

							</div>
						</div>
						<!-- tab-pane -->
						<div class="tab-pane fade" id="reviews">
							<div class="cab-reviews">
								<div class="cab-review">
									<div class="cab-review_top">
										<div class="cab-review_status cab-review_status_mobile cab-review_status_good">
											Отзыв опубликован
										</div>

										<a href="#!" class="cab-review__img">
											<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-2-rem-bg.png" alt="">
										</a>

										<div class="cab-review_data">
											<div class="cab-review_status cab-review_status_desctop cab-review_status_good">
												Отзыв опубликован
											</div>
											<!-- cab-review_status -->
											<div class="cab-review_rating">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/fill-star.svg" alt="">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/fill-star.svg" alt="">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/fill-star.svg" alt="">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/fill-star.svg" alt="">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/unfill-star.svg" alt="">
											</div>
											<!-- cab-review_rating -->

											<a href="#!" class="cab-review_title">
												Высокобелковые палочки в шоколаде без сахара, 500 г (10x50 г)
											</a>
										</div>
										<!-- cab-review_data -->
									</div>
									<!-- cab-review_top -->

									<div class="cab-review__info">
										<h3>Достоинства</h3>

										<div>
											<p>Лёгкий, USB, дует в 3 мощностях</p>
										</div>
									</div>
									<!-- cab-review__info -->

									<div class="cab-review__info">
										<h3>Недостатки</h3>

										<div>
											<p>Дешёвый пластик, маленькая мощность, высокая цена</p>
										</div>
									</div>
									<!-- cab-review__info -->

									<div class="cab-review__info">
										<h3>Комментарий</h3>

										<div>
											<p>Состоит из компьютерного кулера, так называемых фильтров, которые не могут ничего фильтровать, так как стоять вдоль, а не поперёк, и пластиковой коробки. Если сомневаетесь, то лучше возьмите небольшой настольный вентилятор, по мощнее будет</p>
										</div>
									</div>
									<!-- cab-review__info -->


									<div class="cab-review_bottom">
										<div class="cab-review_left">
											<span class="cab-review__date">10 месяцев назад</span>
										</div>
										<!-- cab-review_left -->

										<div class="cab-review_right">
											<div class="cab-review__views">
												<svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
													<path d="M0.13982 10.5723C0.336359 10.3035 5.01923 3.98926 11.0001 3.98926C16.981 3.98926 21.6641 10.3035 21.8604 10.5721C22.0465 10.8271 22.0465 11.1731 21.8604 11.4282C21.6641 11.697 16.981 18.0112 11.0001 18.0112C5.01923 18.0112 0.336359 11.697 0.13982 11.4284C-0.0465794 11.1733 -0.0465794 10.8271 0.13982 10.5723ZM11.0001 16.5607C15.4057 16.5607 19.2213 12.3698 20.3509 10.9998C19.2228 9.62849 15.4151 5.4398 11.0001 5.4398C6.59477 5.4398 2.77935 9.62995 1.64936 11.0007C2.77742 12.372 6.5851 16.5607 11.0001 16.5607Z" fill="#3C464D"/>
													<path d="M11.0003 6.64844C13.3997 6.64844 15.3519 8.60064 15.3519 11.0001C15.3519 13.3996 13.3997 15.3518 11.0003 15.3518C8.6008 15.3518 6.6486 13.3996 6.6486 11.0001C6.6486 8.60064 8.6008 6.64844 11.0003 6.64844ZM11.0003 13.9012C12.6 13.9012 13.9013 12.5998 13.9013 11.0001C13.9013 9.40042 12.5999 8.09902 11.0003 8.09902C9.40058 8.09902 8.09918 9.40042 8.09918 11.0001C8.09918 12.5998 9.40053 13.9012 11.0003 13.9012Z" fill="#3C464D"/>
												</svg>

												<span>10</span>
											</div>
											<!-- cab-review__views -->

											<div class="cab-review__score">
												<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/score_top.svg" alt=""></a>
												<span>10</span>
											</div>

											<div class="cab-review__score">
												<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/score_bottom.svg" alt=""></a>
												<span>2</span>
											</div>
										</div>
										<!-- cab-review_right -->
									</div>
								</div>
								<!-- cab-review -->

								<div class="cab-review">
									<div class="cab-review_top">
										<div class="cab-review_status cab-review_status_mobile cab-review_status_good">
											Отзыв опубликован
										</div>

										<a href="#!" class="cab-review__img">
											<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-2-rem-bg.png" alt="">
										</a>

										<div class="cab-review_data">
											<div class="cab-review_status cab-review_status_desctop cab-review_status_good">
												Отзыв опубликован
											</div>
											<!-- cab-review_status -->
											<div class="cab-review_rating">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/fill-star.svg" alt="">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/fill-star.svg" alt="">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/fill-star.svg" alt="">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/fill-star.svg" alt="">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/unfill-star.svg" alt="">
											</div>
											<!-- cab-review_rating -->

											<a href="#!" class="cab-review_title">
												Высокобелковые палочки в шоколаде без сахара, 500 г (10x50 г)
											</a>
										</div>
										<!-- cab-review_data -->
									</div>
									<!-- cab-review_top -->

									<div class="cab-review__info">
										<h3>Достоинства</h3>

										<div>
											<p>Лёгкий, USB, дует в 3 мощностях</p>
										</div>
									</div>
									<!-- cab-review__info -->

									<div class="cab-review__info">
										<h3>Недостатки</h3>

										<div>
											<p>Дешёвый пластик, маленькая мощность, высокая цена</p>
										</div>
									</div>
									<!-- cab-review__info -->

									<div class="cab-review__info">
										<h3>Комментарий</h3>

										<div>
											<p>Состоит из компьютерного кулера, так называемых фильтров, которые не могут ничего фильтровать, так как стоять вдоль, а не поперёк, и пластиковой коробки. Если сомневаетесь, то лучше возьмите небольшой настольный вентилятор, по мощнее будет</p>
										</div>
									</div>
									<!-- cab-review__info -->


									<div class="cab-review_bottom">
										<div class="cab-review_left">
											<span class="cab-review__date">10 месяцев назад</span>
										</div>
										<!-- cab-review_left -->

										<div class="cab-review_right">
											<div class="cab-review__views">
												<svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
													<path d="M0.13982 10.5723C0.336359 10.3035 5.01923 3.98926 11.0001 3.98926C16.981 3.98926 21.6641 10.3035 21.8604 10.5721C22.0465 10.8271 22.0465 11.1731 21.8604 11.4282C21.6641 11.697 16.981 18.0112 11.0001 18.0112C5.01923 18.0112 0.336359 11.697 0.13982 11.4284C-0.0465794 11.1733 -0.0465794 10.8271 0.13982 10.5723ZM11.0001 16.5607C15.4057 16.5607 19.2213 12.3698 20.3509 10.9998C19.2228 9.62849 15.4151 5.4398 11.0001 5.4398C6.59477 5.4398 2.77935 9.62995 1.64936 11.0007C2.77742 12.372 6.5851 16.5607 11.0001 16.5607Z" fill="#3C464D"/>
													<path d="M11.0003 6.64844C13.3997 6.64844 15.3519 8.60064 15.3519 11.0001C15.3519 13.3996 13.3997 15.3518 11.0003 15.3518C8.6008 15.3518 6.6486 13.3996 6.6486 11.0001C6.6486 8.60064 8.6008 6.64844 11.0003 6.64844ZM11.0003 13.9012C12.6 13.9012 13.9013 12.5998 13.9013 11.0001C13.9013 9.40042 12.5999 8.09902 11.0003 8.09902C9.40058 8.09902 8.09918 9.40042 8.09918 11.0001C8.09918 12.5998 9.40053 13.9012 11.0003 13.9012Z" fill="#3C464D"/>
												</svg>

												<span>10</span>
											</div>
											<!-- cab-review__views -->

											<div class="cab-review__score">
												<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/score_top.svg" alt=""></a>
												<span>10</span>
											</div>

											<div class="cab-review__score">
												<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/score_bottom.svg" alt=""></a>
												<span>2</span>
											</div>
										</div>
										<!-- cab-review_right -->
									</div>
								</div>
								<!-- cab-review -->

								<div class="cab-review">
									<div class="cab-review_top">
										<div class="cab-review_status cab-review_status_mobile cab-review_status_good">
											Отзыв опубликован
										</div>

										<a href="#!" class="cab-review__img">
											<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-2-rem-bg.png" alt="">
										</a>

										<div class="cab-review_data">
											<div class="cab-review_status cab-review_status_desctop cab-review_status_good">
												Отзыв опубликован
											</div>
											<!-- cab-review_status -->
											<div class="cab-review_rating">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/fill-star.svg" alt="">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/fill-star.svg" alt="">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/fill-star.svg" alt="">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/fill-star.svg" alt="">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/unfill-star.svg" alt="">
											</div>
											<!-- cab-review_rating -->

											<a href="#!" class="cab-review_title">
												Высокобелковые палочки в шоколаде без сахара, 500 г (10x50 г)
											</a>
										</div>
										<!-- cab-review_data -->
									</div>
									<!-- cab-review_top -->

									<div class="cab-review__info">
										<h3>Достоинства</h3>

										<div>
											<p>Лёгкий, USB, дует в 3 мощностях</p>
										</div>
									</div>
									<!-- cab-review__info -->

									<div class="cab-review__info">
										<h3>Недостатки</h3>

										<div>
											<p>Дешёвый пластик, маленькая мощность, высокая цена</p>
										</div>
									</div>
									<!-- cab-review__info -->

									<div class="cab-review__info">
										<h3>Комментарий</h3>

										<div>
											<p>Состоит из компьютерного кулера, так называемых фильтров, которые не могут ничего фильтровать, так как стоять вдоль, а не поперёк, и пластиковой коробки. Если сомневаетесь, то лучше возьмите небольшой настольный вентилятор, по мощнее будет</p>
										</div>
									</div>
									<!-- cab-review__info -->


									<div class="cab-review_bottom">
										<div class="cab-review_left">
											<span class="cab-review__date">10 месяцев назад</span>
										</div>
										<!-- cab-review_left -->

										<div class="cab-review_right">
											<div class="cab-review__views">
												<svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
													<path d="M0.13982 10.5723C0.336359 10.3035 5.01923 3.98926 11.0001 3.98926C16.981 3.98926 21.6641 10.3035 21.8604 10.5721C22.0465 10.8271 22.0465 11.1731 21.8604 11.4282C21.6641 11.697 16.981 18.0112 11.0001 18.0112C5.01923 18.0112 0.336359 11.697 0.13982 11.4284C-0.0465794 11.1733 -0.0465794 10.8271 0.13982 10.5723ZM11.0001 16.5607C15.4057 16.5607 19.2213 12.3698 20.3509 10.9998C19.2228 9.62849 15.4151 5.4398 11.0001 5.4398C6.59477 5.4398 2.77935 9.62995 1.64936 11.0007C2.77742 12.372 6.5851 16.5607 11.0001 16.5607Z" fill="#3C464D"/>
													<path d="M11.0003 6.64844C13.3997 6.64844 15.3519 8.60064 15.3519 11.0001C15.3519 13.3996 13.3997 15.3518 11.0003 15.3518C8.6008 15.3518 6.6486 13.3996 6.6486 11.0001C6.6486 8.60064 8.6008 6.64844 11.0003 6.64844ZM11.0003 13.9012C12.6 13.9012 13.9013 12.5998 13.9013 11.0001C13.9013 9.40042 12.5999 8.09902 11.0003 8.09902C9.40058 8.09902 8.09918 9.40042 8.09918 11.0001C8.09918 12.5998 9.40053 13.9012 11.0003 13.9012Z" fill="#3C464D"/>
												</svg>

												<span>10</span>
											</div>
											<!-- cab-review__views -->

											<div class="cab-review__score">
												<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/score_top.svg" alt=""></a>
												<span>10</span>
											</div>

											<div class="cab-review__score">
												<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/score_bottom.svg" alt=""></a>
												<span>2</span>
											</div>
										</div>
										<!-- cab-review_right -->
									</div>
								</div>
								<!-- cab-review -->

								<div class="cab-review">
									<div class="cab-review_top">
										<div class="cab-review_status cab-review_status_mobile cab-review_status_good">
											Отзыв опубликован
										</div>

										<a href="#!" class="cab-review__img">
											<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-2-rem-bg.png" alt="">
										</a>

										<div class="cab-review_data">
											<div class="cab-review_status cab-review_status_desctop cab-review_status_good">
												Отзыв опубликован
											</div>
											<!-- cab-review_status -->
											<div class="cab-review_rating">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/fill-star.svg" alt="">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/fill-star.svg" alt="">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/fill-star.svg" alt="">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/fill-star.svg" alt="">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/unfill-star.svg" alt="">
											</div>
											<!-- cab-review_rating -->

											<a href="#!" class="cab-review_title">
												Высокобелковые палочки в шоколаде без сахара, 500 г (10x50 г)
											</a>
										</div>
										<!-- cab-review_data -->
									</div>
									<!-- cab-review_top -->

									<div class="cab-review__info">
										<h3>Достоинства</h3>

										<div>
											<p>Лёгкий, USB, дует в 3 мощностях</p>
										</div>
									</div>
									<!-- cab-review__info -->

									<div class="cab-review__info">
										<h3>Недостатки</h3>

										<div>
											<p>Дешёвый пластик, маленькая мощность, высокая цена</p>
										</div>
									</div>
									<!-- cab-review__info -->

									<div class="cab-review__info">
										<h3>Комментарий</h3>

										<div>
											<p>Состоит из компьютерного кулера, так называемых фильтров, которые не могут ничего фильтровать, так как стоять вдоль, а не поперёк, и пластиковой коробки. Если сомневаетесь, то лучше возьмите небольшой настольный вентилятор, по мощнее будет</p>
										</div>
									</div>
									<!-- cab-review__info -->


									<div class="cab-review_bottom">
										<div class="cab-review_left">
											<span class="cab-review__date">10 месяцев назад</span>
										</div>
										<!-- cab-review_left -->

										<div class="cab-review_right">
											<div class="cab-review__views">
												<svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
													<path d="M0.13982 10.5723C0.336359 10.3035 5.01923 3.98926 11.0001 3.98926C16.981 3.98926 21.6641 10.3035 21.8604 10.5721C22.0465 10.8271 22.0465 11.1731 21.8604 11.4282C21.6641 11.697 16.981 18.0112 11.0001 18.0112C5.01923 18.0112 0.336359 11.697 0.13982 11.4284C-0.0465794 11.1733 -0.0465794 10.8271 0.13982 10.5723ZM11.0001 16.5607C15.4057 16.5607 19.2213 12.3698 20.3509 10.9998C19.2228 9.62849 15.4151 5.4398 11.0001 5.4398C6.59477 5.4398 2.77935 9.62995 1.64936 11.0007C2.77742 12.372 6.5851 16.5607 11.0001 16.5607Z" fill="#3C464D"/>
													<path d="M11.0003 6.64844C13.3997 6.64844 15.3519 8.60064 15.3519 11.0001C15.3519 13.3996 13.3997 15.3518 11.0003 15.3518C8.6008 15.3518 6.6486 13.3996 6.6486 11.0001C6.6486 8.60064 8.6008 6.64844 11.0003 6.64844ZM11.0003 13.9012C12.6 13.9012 13.9013 12.5998 13.9013 11.0001C13.9013 9.40042 12.5999 8.09902 11.0003 8.09902C9.40058 8.09902 8.09918 9.40042 8.09918 11.0001C8.09918 12.5998 9.40053 13.9012 11.0003 13.9012Z" fill="#3C464D"/>
												</svg>

												<span>10</span>
											</div>
											<!-- cab-review__views -->

											<div class="cab-review__score">
												<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/score_top.svg" alt=""></a>
												<span>10</span>
											</div>

											<div class="cab-review__score">
												<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/score_bottom.svg" alt=""></a>
												<span>2</span>
											</div>
										</div>
										<!-- cab-review_right -->
									</div>
								</div>
								<!-- cab-review -->
							</div>
							<!-- cab-reviews -->
						</div>
						<!-- tab-pane -->

						<div class="tab-pane fade" id="subscriptions">
							<div class="cab-subscriptions">
								<div class="cab-subscriptions-info">
									<div class="cab-subscriptions_top">
										<h3>Название рассылки</h3>
										<h3>Статус</h3>
									</div>

									<div class="cab-subscriptions__item">
										<h4>Акции и предложения</h4>

										<label class="switch">
											<input type="checkbox" checked="">
											<span class="switch_round round"></span>
										</label>
									</div>
									<!-- cab-subscriptions__item -->

									<div class="cab-subscriptions__item">
										<h4>Полезные статьи, новости и анонсы мероприятий</h4>

										<label class="switch">
											<input type="checkbox">
											<span class="switch_round round"></span>
										</label>
									</div>
									<!-- cab-subscriptions__item -->

									<div class="cab-subscriptions__item">
										<h4>Уведомления о программе лояльности</h4>

										<label class="switch">
											<input type="checkbox" checked="">
											<span class="switch_round round"></span>
										</label>

									</div>
									<!-- cab-subscriptions__item -->
								</div>
								<!-- cab-subscriptions-info -->

								<div class="section-top mt-40">
									<div class="section-top__wrap">
										<h3 class="section-top__title">Подписка на поступление <span>18</span> товаров</h3>
									</div>
								</div>

								<div class="subscriptions__slider__inner products products-buy product-remember">
									<div class="product__item">
										<div class="stickers">
											<span class="stickers__item stickers__item_new">
												New
											</span>
										</div>
										<a href="#" class="product__wishlist">
											<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt="heart">
										</a>
										<a href="#" class="product-images products__image">
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-2.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-3.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
										</a>
										<ul class="product-attributes">
											<li class="product-attributes__item">
												<span>150</span>
												<span>ккал</span>
											</li>
											<li class="product-attributes__item">
												<span>4</span>
												<span>белки</span>
											</li>
											<li class="product-attributes__item">
												<span>0,3</span>
												<span>хе</span>
											</li>
											<li class="product-attributes__item">
												<span>11</span>
												<span>углеводы</span>
											</li>
										</ul>
										<a href="#" class="products__title">
											Мармелад «венское кафе»со вкусом карамели, 180 г
										</a>
										<div class="product__bottom">
											<a href="#!" class="cab-subscriptions_cancel">
												<svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
													<g clip-path="url(#clip0)">
														<path d="M8.00005 0C3.58882 0 0 3.58882 0 8.00005C0 12.4113 3.58882 16.0001 8.00005 16.0001C12.4113 16.0001 16.0001 12.4113 16.0001 8.00005C16.0001 3.58882 12.4112 0 8.00005 0ZM8.00005 14.6286C4.34511 14.6286 1.37145 11.655 1.37145 8.00005C1.37145 4.34511 4.34511 1.37145 8.00005 1.37145C11.655 1.37145 14.6286 4.34511 14.6286 8.00005C14.6286 11.655 11.6549 14.6286 8.00005 14.6286Z" fill="#3C464D"/>
														<path d="M11.0955 4.905C10.8277 4.63714 10.3933 4.63714 10.1259 4.905L8.00003 7.03118L5.87412 4.905C5.60626 4.63714 5.17237 4.63714 4.90451 4.905C4.63665 5.17286 4.63665 5.60675 4.90451 5.87461L7.03042 8.0007L4.90442 10.1269C4.63656 10.3947 4.63656 10.8286 4.90442 11.0965C5.0383 11.2304 5.21379 11.2974 5.38927 11.2974C5.56475 11.2974 5.74042 11.2305 5.87412 11.0965L8.00003 8.9704L10.1259 11.0965C10.2598 11.2304 10.4353 11.2974 10.6108 11.2974C10.7863 11.2974 10.9617 11.2305 11.0956 11.0965C11.3635 10.8286 11.3635 10.3947 11.0956 10.1269L8.96964 8.00079L11.0955 5.8747C11.3634 5.60684 11.3634 5.17286 11.0955 4.905Z" fill="#3C464D"/>
													</g>
													<defs>
														<clipPath id="clip0">
															<rect width="16" height="16" fill="white"/>
														</clipPath>
													</defs>
												</svg>

												<span>Отменить подписку</span>
											</a>
										</div>
										<!-- product__bottom -->
									</div>
									<!-- product__item -->

									<div class="product__item">
										<div class="stickers">
											<span class="stickers__item stickers__item_new">
												New
											</span>
										</div>
										<a href="#" class="product__wishlist">
											<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt="heart">
										</a>
										<a href="#" class="product-images products__image">
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-2.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-3.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
										</a>
										<ul class="product-attributes">
											<li class="product-attributes__item">
												<span>150</span>
												<span>ккал</span>
											</li>
											<li class="product-attributes__item">
												<span>4</span>
												<span>белки</span>
											</li>
											<li class="product-attributes__item">
												<span>0,3</span>
												<span>хе</span>
											</li>
											<li class="product-attributes__item">
												<span>11</span>
												<span>углеводы</span>
											</li>
										</ul>
										<a href="#" class="products__title">
											Мармелад «венское кафе»со вкусом карамели, 180 г
										</a>
										<div class="product__bottom">
											<a href="#!" class="cab-subscriptions_cancel">
												<svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
													<g clip-path="url(#clip0)">
														<path d="M8.00005 0C3.58882 0 0 3.58882 0 8.00005C0 12.4113 3.58882 16.0001 8.00005 16.0001C12.4113 16.0001 16.0001 12.4113 16.0001 8.00005C16.0001 3.58882 12.4112 0 8.00005 0ZM8.00005 14.6286C4.34511 14.6286 1.37145 11.655 1.37145 8.00005C1.37145 4.34511 4.34511 1.37145 8.00005 1.37145C11.655 1.37145 14.6286 4.34511 14.6286 8.00005C14.6286 11.655 11.6549 14.6286 8.00005 14.6286Z" fill="#3C464D"/>
														<path d="M11.0955 4.905C10.8277 4.63714 10.3933 4.63714 10.1259 4.905L8.00003 7.03118L5.87412 4.905C5.60626 4.63714 5.17237 4.63714 4.90451 4.905C4.63665 5.17286 4.63665 5.60675 4.90451 5.87461L7.03042 8.0007L4.90442 10.1269C4.63656 10.3947 4.63656 10.8286 4.90442 11.0965C5.0383 11.2304 5.21379 11.2974 5.38927 11.2974C5.56475 11.2974 5.74042 11.2305 5.87412 11.0965L8.00003 8.9704L10.1259 11.0965C10.2598 11.2304 10.4353 11.2974 10.6108 11.2974C10.7863 11.2974 10.9617 11.2305 11.0956 11.0965C11.3635 10.8286 11.3635 10.3947 11.0956 10.1269L8.96964 8.00079L11.0955 5.8747C11.3634 5.60684 11.3634 5.17286 11.0955 4.905Z" fill="#3C464D"/>
													</g>
													<defs>
														<clipPath id="clip0">
															<rect width="16" height="16" fill="white"/>
														</clipPath>
													</defs>
												</svg>

												<span>Отменить подписку</span>
											</a>
										</div>
										<!-- product__bottom -->
									</div>
									<!-- product__item -->

									<div class="product__item">
										<div class="stickers">
											<span class="stickers__item stickers__item_new">
												New
											</span>
										</div>
										<a href="#" class="product__wishlist">
											<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt="heart">
										</a>
										<a href="#" class="product-images products__image">
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-2.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-3.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
										</a>
										<ul class="product-attributes">
											<li class="product-attributes__item">
												<span>150</span>
												<span>ккал</span>
											</li>
											<li class="product-attributes__item">
												<span>4</span>
												<span>белки</span>
											</li>
											<li class="product-attributes__item">
												<span>0,3</span>
												<span>хе</span>
											</li>
											<li class="product-attributes__item">
												<span>11</span>
												<span>углеводы</span>
											</li>
										</ul>
										<a href="#" class="products__title">
											Мармелад «венское кафе»со вкусом карамели, 180 г
										</a>
										<div class="product__bottom">
											<a href="#!" class="cab-subscriptions_cancel">
												<svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
													<g clip-path="url(#clip0)">
														<path d="M8.00005 0C3.58882 0 0 3.58882 0 8.00005C0 12.4113 3.58882 16.0001 8.00005 16.0001C12.4113 16.0001 16.0001 12.4113 16.0001 8.00005C16.0001 3.58882 12.4112 0 8.00005 0ZM8.00005 14.6286C4.34511 14.6286 1.37145 11.655 1.37145 8.00005C1.37145 4.34511 4.34511 1.37145 8.00005 1.37145C11.655 1.37145 14.6286 4.34511 14.6286 8.00005C14.6286 11.655 11.6549 14.6286 8.00005 14.6286Z" fill="#3C464D"/>
														<path d="M11.0955 4.905C10.8277 4.63714 10.3933 4.63714 10.1259 4.905L8.00003 7.03118L5.87412 4.905C5.60626 4.63714 5.17237 4.63714 4.90451 4.905C4.63665 5.17286 4.63665 5.60675 4.90451 5.87461L7.03042 8.0007L4.90442 10.1269C4.63656 10.3947 4.63656 10.8286 4.90442 11.0965C5.0383 11.2304 5.21379 11.2974 5.38927 11.2974C5.56475 11.2974 5.74042 11.2305 5.87412 11.0965L8.00003 8.9704L10.1259 11.0965C10.2598 11.2304 10.4353 11.2974 10.6108 11.2974C10.7863 11.2974 10.9617 11.2305 11.0956 11.0965C11.3635 10.8286 11.3635 10.3947 11.0956 10.1269L8.96964 8.00079L11.0955 5.8747C11.3634 5.60684 11.3634 5.17286 11.0955 4.905Z" fill="#3C464D"/>
													</g>
													<defs>
														<clipPath id="clip0">
															<rect width="16" height="16" fill="white"/>
														</clipPath>
													</defs>
												</svg>

												<span>Отменить подписку</span>
											</a>
										</div>
										<!-- product__bottom -->
									</div>
									<!-- product__item -->

									<div class="product__item">
										<div class="stickers">
											<span class="stickers__item stickers__item_new">
												New
											</span>
										</div>
										<a href="#" class="product__wishlist">
											<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt="heart">
										</a>
										<a href="#" class="product-images products__image">
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-2.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-3.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
										</a>
										<ul class="product-attributes">
											<li class="product-attributes__item">
												<span>150</span>
												<span>ккал</span>
											</li>
											<li class="product-attributes__item">
												<span>4</span>
												<span>белки</span>
											</li>
											<li class="product-attributes__item">
												<span>0,3</span>
												<span>хе</span>
											</li>
											<li class="product-attributes__item">
												<span>11</span>
												<span>углеводы</span>
											</li>
										</ul>
										<a href="#" class="products__title">
											Мармелад «венское кафе»со вкусом карамели, 180 г
										</a>
										<div class="product__bottom">
											<a href="#!" class="cab-subscriptions_cancel">
												<svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
													<g clip-path="url(#clip0)">
														<path d="M8.00005 0C3.58882 0 0 3.58882 0 8.00005C0 12.4113 3.58882 16.0001 8.00005 16.0001C12.4113 16.0001 16.0001 12.4113 16.0001 8.00005C16.0001 3.58882 12.4112 0 8.00005 0ZM8.00005 14.6286C4.34511 14.6286 1.37145 11.655 1.37145 8.00005C1.37145 4.34511 4.34511 1.37145 8.00005 1.37145C11.655 1.37145 14.6286 4.34511 14.6286 8.00005C14.6286 11.655 11.6549 14.6286 8.00005 14.6286Z" fill="#3C464D"/>
														<path d="M11.0955 4.905C10.8277 4.63714 10.3933 4.63714 10.1259 4.905L8.00003 7.03118L5.87412 4.905C5.60626 4.63714 5.17237 4.63714 4.90451 4.905C4.63665 5.17286 4.63665 5.60675 4.90451 5.87461L7.03042 8.0007L4.90442 10.1269C4.63656 10.3947 4.63656 10.8286 4.90442 11.0965C5.0383 11.2304 5.21379 11.2974 5.38927 11.2974C5.56475 11.2974 5.74042 11.2305 5.87412 11.0965L8.00003 8.9704L10.1259 11.0965C10.2598 11.2304 10.4353 11.2974 10.6108 11.2974C10.7863 11.2974 10.9617 11.2305 11.0956 11.0965C11.3635 10.8286 11.3635 10.3947 11.0956 10.1269L8.96964 8.00079L11.0955 5.8747C11.3634 5.60684 11.3634 5.17286 11.0955 4.905Z" fill="#3C464D"/>
													</g>
													<defs>
														<clipPath id="clip0">
															<rect width="16" height="16" fill="white"/>
														</clipPath>
													</defs>
												</svg>

												<span>Отменить подписку</span>
											</a>
										</div>
										<!-- product__bottom -->
									</div>
									<!-- product__item -->

									<div class="product__item">
										<div class="stickers">
											<span class="stickers__item stickers__item_new">
												New
											</span>
										</div>
										<a href="#" class="product__wishlist">
											<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt="heart">
										</a>
										<a href="#" class="product-images products__image">
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-2.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-3.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
										</a>
										<ul class="product-attributes">
											<li class="product-attributes__item">
												<span>150</span>
												<span>ккал</span>
											</li>
											<li class="product-attributes__item">
												<span>4</span>
												<span>белки</span>
											</li>
											<li class="product-attributes__item">
												<span>0,3</span>
												<span>хе</span>
											</li>
											<li class="product-attributes__item">
												<span>11</span>
												<span>углеводы</span>
											</li>
										</ul>
										<a href="#" class="products__title">
											Мармелад «венское кафе»со вкусом карамели, 180 г
										</a>
										<div class="product__bottom">
											<a href="#!" class="cab-subscriptions_cancel">
												<svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
													<g clip-path="url(#clip0)">
														<path d="M8.00005 0C3.58882 0 0 3.58882 0 8.00005C0 12.4113 3.58882 16.0001 8.00005 16.0001C12.4113 16.0001 16.0001 12.4113 16.0001 8.00005C16.0001 3.58882 12.4112 0 8.00005 0ZM8.00005 14.6286C4.34511 14.6286 1.37145 11.655 1.37145 8.00005C1.37145 4.34511 4.34511 1.37145 8.00005 1.37145C11.655 1.37145 14.6286 4.34511 14.6286 8.00005C14.6286 11.655 11.6549 14.6286 8.00005 14.6286Z" fill="#3C464D"/>
														<path d="M11.0955 4.905C10.8277 4.63714 10.3933 4.63714 10.1259 4.905L8.00003 7.03118L5.87412 4.905C5.60626 4.63714 5.17237 4.63714 4.90451 4.905C4.63665 5.17286 4.63665 5.60675 4.90451 5.87461L7.03042 8.0007L4.90442 10.1269C4.63656 10.3947 4.63656 10.8286 4.90442 11.0965C5.0383 11.2304 5.21379 11.2974 5.38927 11.2974C5.56475 11.2974 5.74042 11.2305 5.87412 11.0965L8.00003 8.9704L10.1259 11.0965C10.2598 11.2304 10.4353 11.2974 10.6108 11.2974C10.7863 11.2974 10.9617 11.2305 11.0956 11.0965C11.3635 10.8286 11.3635 10.3947 11.0956 10.1269L8.96964 8.00079L11.0955 5.8747C11.3634 5.60684 11.3634 5.17286 11.0955 4.905Z" fill="#3C464D"/>
													</g>
													<defs>
														<clipPath id="clip0">
															<rect width="16" height="16" fill="white"/>
														</clipPath>
													</defs>
												</svg>

												<span>Отменить подписку</span>
											</a>
										</div>
										<!-- product__bottom -->
									</div>
									<!-- product__item -->

									<div class="product__item">
										<div class="stickers">
											<span class="stickers__item stickers__item_new">
												New
											</span>
										</div>
										<a href="#" class="product__wishlist">
											<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt="heart">
										</a>
										<a href="#" class="product-images products__image">
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-2.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-3.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
										</a>
										<ul class="product-attributes">
											<li class="product-attributes__item">
												<span>150</span>
												<span>ккал</span>
											</li>
											<li class="product-attributes__item">
												<span>4</span>
												<span>белки</span>
											</li>
											<li class="product-attributes__item">
												<span>0,3</span>
												<span>хе</span>
											</li>
											<li class="product-attributes__item">
												<span>11</span>
												<span>углеводы</span>
											</li>
										</ul>
										<a href="#" class="products__title">
											Мармелад «венское кафе»со вкусом карамели, 180 г
										</a>
										<div class="product__bottom">
											<a href="#!" class="cab-subscriptions_cancel">
												<svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
													<g clip-path="url(#clip0)">
														<path d="M8.00005 0C3.58882 0 0 3.58882 0 8.00005C0 12.4113 3.58882 16.0001 8.00005 16.0001C12.4113 16.0001 16.0001 12.4113 16.0001 8.00005C16.0001 3.58882 12.4112 0 8.00005 0ZM8.00005 14.6286C4.34511 14.6286 1.37145 11.655 1.37145 8.00005C1.37145 4.34511 4.34511 1.37145 8.00005 1.37145C11.655 1.37145 14.6286 4.34511 14.6286 8.00005C14.6286 11.655 11.6549 14.6286 8.00005 14.6286Z" fill="#3C464D"/>
														<path d="M11.0955 4.905C10.8277 4.63714 10.3933 4.63714 10.1259 4.905L8.00003 7.03118L5.87412 4.905C5.60626 4.63714 5.17237 4.63714 4.90451 4.905C4.63665 5.17286 4.63665 5.60675 4.90451 5.87461L7.03042 8.0007L4.90442 10.1269C4.63656 10.3947 4.63656 10.8286 4.90442 11.0965C5.0383 11.2304 5.21379 11.2974 5.38927 11.2974C5.56475 11.2974 5.74042 11.2305 5.87412 11.0965L8.00003 8.9704L10.1259 11.0965C10.2598 11.2304 10.4353 11.2974 10.6108 11.2974C10.7863 11.2974 10.9617 11.2305 11.0956 11.0965C11.3635 10.8286 11.3635 10.3947 11.0956 10.1269L8.96964 8.00079L11.0955 5.8747C11.3634 5.60684 11.3634 5.17286 11.0955 4.905Z" fill="#3C464D"/>
													</g>
													<defs>
														<clipPath id="clip0">
															<rect width="16" height="16" fill="white"/>
														</clipPath>
													</defs>
												</svg>

												<span>Отменить подписку</span>
											</a>
										</div>
										<!-- product__bottom -->
									</div>
									<!-- product__item -->

								</div>
								<!-- subscriptions__slider__inner -->
							</div>
							<!-- cab-subscriptions -->
						</div>
						<!-- tab-pane -->
					</div>
					<!-- tab-content -->
				</div>
			</div>
			<!-- row -->
		</div>
		<!-- container -->


		<div class="cabinet_mobile">
			
			<!-- Mobile accordeon -->
			<div class="cabinet_accordion_mob">
				<ul id="accordion_cab">

					<li>
						<p class="head_mob">Профиль</p>
						<div class="hidden_mob">
							
							<form class="cab-prof">
								<!-- User photo -->
								<label class="cab-prof__photo">
									<input type="file" name="file" hidden>
									<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/cabinet/user-photo.svg" alt="">
								</label>

								<!-- User info -->
								<div class="cab-prof__data">
									<div class="personal__data__wrapper">
										<h5 class="personal__data__title">Личные данные</h5>
										<div class="personal__data__inputs">
											<div class="personal__data__input personal__data__input__name">
												<label for="name">Ф. И .О.*</label>
												<input type="text" required id="name" placeholder="Константинопольский Константин Константинович">
											</div>

											<div class="personal__data__input personal__data__input_gender">
												<span>Пол</span>
												<div>
													<label class="radio__wrapper">
														<div>
															<span>Мужской</span>
															<input type="radio" name="get-method" checked="">
															<span class="radio__checkmark"></span>
														</div>
													</label>

													<label class="radio__wrapper">
														<div>
															<span>Женский</span>
															<input type="radio" name="get-method" checked="">
															<span class="radio__checkmark"></span>
														</div>
													</label>
												</div>
											</div>

											<div class="personal__data__input personal__data__input__tel">
												<label for="tel">Телефон *</label>
												<input type="tel" required id="tel" placeholder="+7 (000) 000 00 00">
											</div>

											<div class="personal__data__input personal__data__input__email">
												<label for="email">E-mail *</label>
												<input type="email" required id="email" placeholder="anyname@anydomain.xyz">
											</div>
										</div>
									</div>

									<div class="personal__data__wrapper">
										<h5 class="personal__data__title">Адрес доставки</h5>

										<div class="personal__data__inputs">
											<div class="personal__data__input personal__data__input__country">
												<label for="country">Страна*</label>
												<input type="text" required id="country" placeholder="Российская Федерация">
											</div>

											<div class="personal__data__input personal__data__input__city">
												<label for="city">Город*</label>
												<input type="text" required id="city" placeholder="Санкт-Петербург">
											</div>

											<div class="personal__data__input personal__data__input__index">
												<label for="index">Индекс*</label>
												<input type="text" required id="index" placeholder="000000">
											</div>

											<div class="personal__data__input personal__data__input__street">
												<label for="street">Улица*</label>
												<input type="text" required id="street" placeholder="Проспект имени Владимира Ильича Ленина ">
											</div>

											<div class="personal__data__input personal__data__input__house">
												<label for="house">Дом/корпус*</label>
												<input type="text" required id="house" placeholder="1234/123">
											</div>

											<div class="personal__data__input personal__data__input__apartment">
												<label for="apartment">Квартира*</label>
												<input type="text" required id="apartment" placeholder="1234">
											</div>

										</div>
									</div>

									<div class="personal__data__wrapper">
										<h5 class="personal__data__title">Пароль</h5>

										<div class="personal__data__inputs personal__data__inputs_pass">
											<div class="personal__data__input">
												<label for="country">Текущий</label>
												<input type="text" id="country" value="+7 (812) 212 85 06">
												<span class="pass-eye open"></span>
											</div>

											<div class="personal__data__input">
												<label for="city">Новый</label>
												<input type="password" id="city" value="123456789">
												<span class="pass-eye"></span>
											</div>

											<div class="personal__data__input">
												<label for="index">Повторите </label>
												<input type="password" id="index" value="123456789">
												<span class="pass-eye"></span>
											</div>

										</div>
									</div>
									<button type="submit" class="cab-prof__btn">Сохранить изменения</button>

								</div>
							</form>

						</div>
					</li>
					<!-- li -->

					<li>
						<p class="head_mob">Мои заказы</p>
						<div class="hidden_mob">
							
							<div class="cab-order">
								<ul class="nav order-tabs nav-tabs_inner" id="cabinet_2">
									<li class="nav-item tab_active">
										<a class="nav-link active" href="#allOrders_mobile">Все</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" href="#currentOrders_mobile">Текущие</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" href="#completeOrders_mobile">Выполненные</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" href="#cancelOrders_mobile">Отмененные</a>
									</li>
								</ul>

								<div class="tab-content p-2">
									<div class="tab-pane fade show active" id="allOrders_mobile">
										<div class="cab-order__item">
											<ul id="accordion_4">

												<li>
													<div class="head">
														<div class="cab-order__top">
															<div class="cab-order__box">
																<div class="cab-order__status cab-order__status_confirm"><span>№1125005</span>| Подтвержден</div>
																<ul class="cab-order__deal">
																	<li><a href="#!" class="cab-order__cancel">Отменить заказ</a></li>
																	<li><a href="#!" class="cab-order__repeat">Повторить заказ</a></li>
																</ul>
															</div>
															<ul class="cab-order__data">
																<li>
																	<b>Дата создания</b>
																	<span>07.10.2020</span>
																</li>
																<li>
																	<b>Способ доставки</b>
																	<span>СДЭК (ПВЗ: Большая Академическая)</span>
																</li>
																<li>
																	<b>Трек-номер</b>
																	<span>не доступен</span>
																</li>
																<li>
																	<b>Оплата</b>
																	<span>При получении</span>
																</li>
																<li>
																	<b>Сумма</b>
																	<strong>20 642 ₽</strong>
																</li>
															</ul>
														</div>
													</div>
													
													<div class="hidden">
														<div class="cart__item">
															<div class="cart__wrapper">
																<div class="cart__like-del cart__desctop">
																	<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt=""></a>
																</div>
																<div class="cart__img"><img src="<?=SITE_TEMPLATE_PATH;?>/img/cart/1.png" alt=""></div>
																<div class="cart__info-box">	
																	<div class="cart__info">
																		<div class="cart__heading">
																			<span>АРТ. 30085057</span>
																			<h3>Набор для «Кейк-попс со взрывной карамелью» в белой шоколадной глазури</h3>
																		</div>
																		<div class="cart__number cart__desctop">
																			<input type="number" min="1" max="20" value="1">
																		</div>
																		<div class="cart__price cart__desctop">
																			<del>280 ₽</del>
																			<span>200 ₽</span>
																		</div>
																	</div>
																	<div class="cart__item-total cart__desctop">400 ₽</div> 
																</div> 
															</div>
															
															<!-- mob-wrap -->
															<div class="cart__mob-wrap">
																<div class="cart__mob-box">
																	<div class="cart__like-del">
																		<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt=""></a>
																	</div>
																	<div class="cart__price">
																		<del>280 ₽</del>
																		<span>200 ₽</span>
																	</div>
																</div>
																<div class="cart__mob-box">
																	<div class="cart__number">
																		<input type="number" min="1" max="20" value="1">
																	</div>
																	<div class="cart__item-total">400 ₽</div> 
																</div>
															</div>
															<!-- mob-wrap end -->
														</div>


														<div class="cart__item">
															<div class="cart__wrapper">
																<div class="cart__like-del cart__desctop">
																	<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt=""></a>
																</div>
																<div class="cart__img"><img src="<?=SITE_TEMPLATE_PATH;?>/img/cart/1.png" alt=""></div>
																<div class="cart__info-box">	
																	<div class="cart__info">
																		<div class="cart__heading">
																			<span>АРТ. 30085057</span>
																			<h3>Набор для «Кейк-попс со взрывной карамелью» в белой шоколадной глазури</h3>
																		</div>
																		<div class="cart__number cart__desctop">
																			<input type="number" min="1" max="20" value="1">
																		</div>
																		<div class="cart__price cart__desctop">
																			<del>280 ₽</del>
																			<span>200 ₽</span>
																		</div>
																	</div>
																	<div class="cart__item-total cart__desctop">400 ₽</div> 
																</div> 
															</div>
															
															<!-- mob-wrap -->
															<div class="cart__mob-wrap">
																<div class="cart__mob-box">
																	<div class="cart__like-del">
																		<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt=""></a>
																	</div>
																	<div class="cart__price">
																		<del>280 ₽</del>
																		<span>200 ₽</span>
																	</div>
																</div>
																<div class="cart__mob-box">
																	<div class="cart__number">
																		<input type="number" min="1" max="20" value="1">
																	</div>
																	<div class="cart__item-total">400 ₽</div> 
																</div>
															</div>
															<!-- mob-wrap end -->
														</div>
													</div>
												</li>

												<li>
													<div class="head">
														<div class="cab-order__top">
															<div class="cab-order__box">
																<div class="cab-order__status cab-order__status_new"><span>№1125005</span>| Новый</div>
																<ul class="cab-order__deal">
																	<li><a href="#!" class="cab-order__cancel">Отменить заказ</a></li>
																	<li><a href="#!" class="cab-order__repeat">Повторить заказ</a></li>
																</ul>
															</div>
															<ul class="cab-order__data">
																<li>
																	<b>Дата создания</b>
																	<span>07.10.2020</span>
																</li>
																<li>
																	<b>Способ доставки</b>
																	<span>СДЭК (ПВЗ: Большая Академическая)</span>
																</li>
																<li>
																	<b>Трек-номер</b>
																	<span>не доступен</span>
																</li>
																<li>
																	<b>Оплата</b>
																	<span>При получении</span>
																</li>
																<li>
																	<b>Сумма</b>
																	<strong>20 642 ₽</strong>
																</li>
															</ul>
														</div>
													</div>
													
													<div class="hidden">
														<div class="cart__item">
															<div class="cart__wrapper">
																<div class="cart__like-del cart__desctop">
																	<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt=""></a>
																</div>
																<div class="cart__img"><img src="<?=SITE_TEMPLATE_PATH;?>/img/cart/1.png" alt=""></div>
																<div class="cart__info-box">	
																	<div class="cart__info">
																		<div class="cart__heading">
																			<span>АРТ. 30085057</span>
																			<h3>Набор для «Кейк-попс со взрывной карамелью» в белой шоколадной глазури</h3>
																		</div>
																		<div class="cart__number cart__desctop">
																			<input type="number" min="1" max="20" value="1">
																		</div>
																		<div class="cart__price cart__desctop">
																			<del>280 ₽</del>
																			<span>200 ₽</span>
																		</div>
																	</div>
																	<div class="cart__item-total cart__desctop">400 ₽</div> 
																</div> 
															</div>
															
															<!-- mob-wrap -->
															<div class="cart__mob-wrap">
																<div class="cart__mob-box">
																	<div class="cart__like-del">
																		<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt=""></a>
																	</div>
																	<div class="cart__price">
																		<del>280 ₽</del>
																		<span>200 ₽</span>
																	</div>
																</div>
																<div class="cart__mob-box">
																	<div class="cart__number">
																		<input type="number" min="1" max="20" value="1">
																	</div>
																	<div class="cart__item-total">400 ₽</div> 
																</div>
															</div>
															<!-- mob-wrap end -->
														</div>

														
														<div class="cart__item">
															<div class="cart__wrapper">
																<div class="cart__like-del cart__desctop">
																	<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt=""></a>
																</div>
																<div class="cart__img"><img src="<?=SITE_TEMPLATE_PATH;?>/img/cart/1.png" alt=""></div>
																<div class="cart__info-box">	
																	<div class="cart__info">
																		<div class="cart__heading">
																			<span>АРТ. 30085057</span>
																			<h3>Набор для «Кейк-попс со взрывной карамелью» в белой шоколадной глазури</h3>
																		</div>
																		<div class="cart__number cart__desctop">
																			<input type="number" min="1" max="20" value="1">
																		</div>
																		<div class="cart__price cart__desctop">
																			<del>280 ₽</del>
																			<span>200 ₽</span>
																		</div>
																	</div>
																	<div class="cart__item-total cart__desctop">400 ₽</div> 
																</div> 
															</div>
															
															<!-- mob-wrap -->
															<div class="cart__mob-wrap">
																<div class="cart__mob-box">
																	<div class="cart__like-del">
																		<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt=""></a>
																	</div>
																	<div class="cart__price">
																		<del>280 ₽</del>
																		<span>200 ₽</span>
																	</div>
																</div>
																<div class="cart__mob-box">
																	<div class="cart__number">
																		<input type="number" min="1" max="20" value="1">
																	</div>
																	<div class="cart__item-total">400 ₽</div> 
																</div>
															</div>
															<!-- mob-wrap end -->
														</div>
													</div>
												</li>

												<li>
													<div class="head">
														<div class="cab-order__top">
															<div class="cab-order__box">
																<div class="cab-order__status cab-order__status_cancel"><span>№1125005</span>| Отменен</div>
																<ul class="cab-order__deal">
																	<li><a href="#!" class="cab-order__cancel">Отменить заказ</a></li>
																	<li><a href="#!" class="cab-order__repeat">Повторить заказ</a></li>
																</ul>
															</div>
															<ul class="cab-order__data">
																<li>
																	<b>Дата создания</b>
																	<span>07.10.2020</span>
																</li>
																<li>
																	<b>Способ доставки</b>
																	<span>СДЭК (ПВЗ: Большая Академическая)</span>
																</li>
																<li>
																	<b>Трек-номер</b>
																	<span>не доступен</span>
																</li>
																<li>
																	<b>Оплата</b>
																	<span>При получении</span>
																</li>
																<li>
																	<b>Сумма</b>
																	<strong>20 642 ₽</strong>
																</li>
															</ul>
														</div>
													</div>
													
													<div class="hidden">
														<div class="cart__item">
															<div class="cart__wrapper">
																<div class="cart__like-del cart__desctop">
																	<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt=""></a>
																</div>
																<div class="cart__img"><img src="<?=SITE_TEMPLATE_PATH;?>/img/cart/1.png" alt=""></div>
																<div class="cart__info-box">	
																	<div class="cart__info">
																		<div class="cart__heading">
																			<span>АРТ. 30085057</span>
																			<h3>Набор для «Кейк-попс со взрывной карамелью» в белой шоколадной глазури</h3>
																		</div>
																		<div class="cart__number cart__desctop">
																			<input type="number" min="1" max="20" value="1">
																		</div>
																		<div class="cart__price cart__desctop">
																			<del>280 ₽</del>
																			<span>200 ₽</span>
																		</div>
																	</div>
																	<div class="cart__item-total cart__desctop">400 ₽</div> 
																</div> 
															</div>
															
															<!-- mob-wrap -->
															<div class="cart__mob-wrap">
																<div class="cart__mob-box">
																	<div class="cart__like-del">
																		<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt=""></a>
																	</div>
																	<div class="cart__price">
																		<del>280 ₽</del>
																		<span>200 ₽</span>
																	</div>
																</div>
																<div class="cart__mob-box">
																	<div class="cart__number">
																		<input type="number" min="1" max="20" value="1">
																	</div>
																	<div class="cart__item-total">400 ₽</div> 
																</div>
															</div>
															<!-- mob-wrap end -->
														</div>

														
														<div class="cart__item">
															<div class="cart__wrapper">
																<div class="cart__like-del cart__desctop">
																	<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt=""></a>
																</div>
																<div class="cart__img"><img src="<?=SITE_TEMPLATE_PATH;?>/img/cart/1.png" alt=""></div>
																<div class="cart__info-box">	
																	<div class="cart__info">
																		<div class="cart__heading">
																			<span>АРТ. 30085057</span>
																			<h3>Набор для «Кейк-попс со взрывной карамелью» в белой шоколадной глазури</h3>
																		</div>
																		<div class="cart__number cart__desctop">
																			<input type="number" min="1" max="20" value="1">
																		</div>
																		<div class="cart__price cart__desctop">
																			<del>280 ₽</del>
																			<span>200 ₽</span>
																		</div>
																	</div>
																	<div class="cart__item-total cart__desctop">400 ₽</div> 
																</div> 
															</div>
															
															<!-- mob-wrap -->
															<div class="cart__mob-wrap">
																<div class="cart__mob-box">
																	<div class="cart__like-del">
																		<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt=""></a>
																	</div>
																	<div class="cart__price">
																		<del>280 ₽</del>
																		<span>200 ₽</span>
																	</div>
																</div>
																<div class="cart__mob-box">
																	<div class="cart__number">
																		<input type="number" min="1" max="20" value="1">
																	</div>
																	<div class="cart__item-total">400 ₽</div> 
																</div>
															</div>
															<!-- mob-wrap end -->
														</div>
													</div>
												</li>

												
											</ul>
										</div>
									</div>
									<div class="tab-pane fade" id="currentOrders_mobile">
										<div class="cab-order__item">
											<ul id="accordion_4">

												<li>
													<div class="head">
														<div class="cab-order__top">
															<div class="cab-order__box">
																<div class="cab-order__status cab-order__status_cancel"><span>№1125005</span>| Отменен</div>
																<ul class="cab-order__deal">
																	<li><a href="#!" class="cab-order__cancel">Отменить заказ</a></li>
																	<li><a href="#!" class="cab-order__repeat">Повторить заказ</a></li>
																</ul>
															</div>
															<ul class="cab-order__data">
																<li>
																	<b>Дата создания</b>
																	<span>07.10.2020</span>
																</li>
																<li>
																	<b>Способ доставки</b>
																	<span>СДЭК (ПВЗ: Большая Академическая)</span>
																</li>
																<li>
																	<b>Трек-номер</b>
																	<span>не доступен</span>
																</li>
																<li>
																	<b>Оплата</b>
																	<span>При получении</span>
																</li>
																<li>
																	<b>Сумма</b>
																	<strong>20 642 ₽</strong>
																</li>
															</ul>
														</div>
													</div>
													
													<div class="hidden">
														<div class="cart__item">
															<div class="cart__wrapper">
																<div class="cart__like-del cart__desctop">
																	<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt=""></a>
																</div>
																<div class="cart__img"><img src="<?=SITE_TEMPLATE_PATH;?>/img/cart/1.png" alt=""></div>
																<div class="cart__info-box">	
																	<div class="cart__info">
																		<div class="cart__heading">
																			<span>АРТ. 30085057</span>
																			<h3>Набор для «Кейк-попс со взрывной карамелью» в белой шоколадной глазури</h3>
																		</div>
																		<div class="cart__number cart__desctop">
																			<input type="number" min="1" max="20" value="1">
																		</div>
																		<div class="cart__price cart__desctop">
																			<del>280 ₽</del>
																			<span>200 ₽</span>
																		</div>
																	</div>
																	<div class="cart__item-total cart__desctop">400 ₽</div> 
																</div> 
															</div>
															
															<!-- mob-wrap -->
															<div class="cart__mob-wrap">
																<div class="cart__mob-box">
																	<div class="cart__like-del">
																		<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt=""></a>
																	</div>
																	<div class="cart__price">
																		<del>280 ₽</del>
																		<span>200 ₽</span>
																	</div>
																</div>
																<div class="cart__mob-box">
																	<div class="cart__number">
																		<input type="number" min="1" max="20" value="1">
																	</div>
																	<div class="cart__item-total">400 ₽</div> 
																</div>
															</div>
															<!-- mob-wrap end -->
														</div>

														
														<div class="cart__item">
															<div class="cart__wrapper">
																<div class="cart__like-del cart__desctop">
																	<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt=""></a>
																</div>
																<div class="cart__img"><img src="<?=SITE_TEMPLATE_PATH;?>/img/cart/1.png" alt=""></div>
																<div class="cart__info-box">	
																	<div class="cart__info">
																		<div class="cart__heading">
																			<span>АРТ. 30085057</span>
																			<h3>Набор для «Кейк-попс со взрывной карамелью» в белой шоколадной глазури</h3>
																		</div>
																		<div class="cart__number cart__desctop">
																			<input type="number" min="1" max="20" value="1">
																		</div>
																		<div class="cart__price cart__desctop">
																			<del>280 ₽</del>
																			<span>200 ₽</span>
																		</div>
																	</div>
																	<div class="cart__item-total cart__desctop">400 ₽</div> 
																</div> 
															</div>
															
															<!-- mob-wrap -->
															<div class="cart__mob-wrap">
																<div class="cart__mob-box">
																	<div class="cart__like-del">
																		<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt=""></a>
																	</div>
																	<div class="cart__price">
																		<del>280 ₽</del>
																		<span>200 ₽</span>
																	</div>
																</div>
																<div class="cart__mob-box">
																	<div class="cart__number">
																		<input type="number" min="1" max="20" value="1">
																	</div>
																	<div class="cart__item-total">400 ₽</div> 
																</div>
															</div>
															<!-- mob-wrap end -->
														</div>
													</div>
												</li>

											</ul>
										</div>
									</div>
									<div class="tab-pane fade" id="completeOrders_mobile">
										<div class="cab-order__item">
											<ul id="accordion_4">

												<li>
													<div class="head">
														<div class="cab-order__top">
															<div class="cab-order__box">
																<div class="cab-order__status cab-order__status_cancel"><span>№1125005</span>| Отменен</div>
																<ul class="cab-order__deal">
																	<li><a href="#!" class="cab-order__cancel">Отменить заказ</a></li>
																	<li><a href="#!" class="cab-order__repeat">Повторить заказ</a></li>
																</ul>
															</div>
															<ul class="cab-order__data">
																<li>
																	<b>Дата создания</b>
																	<span>07.10.2020</span>
																</li>
																<li>
																	<b>Способ доставки</b>
																	<span>СДЭК (ПВЗ: Большая Академическая)</span>
																</li>
																<li>
																	<b>Трек-номер</b>
																	<span>не доступен</span>
																</li>
																<li>
																	<b>Оплата</b>
																	<span>При получении</span>
																</li>
																<li>
																	<b>Сумма</b>
																	<strong>20 642 ₽</strong>
																</li>
															</ul>
														</div>
													</div>
													
													<div class="hidden">
														<div class="cart__item">
															<div class="cart__wrapper">
																<div class="cart__like-del cart__desctop">
																	<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt=""></a>
																</div>
																<div class="cart__img"><img src="<?=SITE_TEMPLATE_PATH;?>/img/cart/1.png" alt=""></div>
																<div class="cart__info-box">	
																	<div class="cart__info">
																		<div class="cart__heading">
																			<span>АРТ. 30085057</span>
																			<h3>Набор для «Кейк-попс со взрывной карамелью» в белой шоколадной глазури</h3>
																		</div>
																		<div class="cart__number cart__desctop">
																			<input type="number" min="1" max="20" value="1">
																		</div>
																		<div class="cart__price cart__desctop">
																			<del>280 ₽</del>
																			<span>200 ₽</span>
																		</div>
																	</div>
																	<div class="cart__item-total cart__desctop">400 ₽</div> 
																</div> 
															</div>
															
															<!-- mob-wrap -->
															<div class="cart__mob-wrap">
																<div class="cart__mob-box">
																	<div class="cart__like-del">
																		<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt=""></a>
																	</div>
																	<div class="cart__price">
																		<del>280 ₽</del>
																		<span>200 ₽</span>
																	</div>
																</div>
																<div class="cart__mob-box">
																	<div class="cart__number">
																		<input type="number" min="1" max="20" value="1">
																	</div>
																	<div class="cart__item-total">400 ₽</div> 
																</div>
															</div>
															<!-- mob-wrap end -->
														</div>

														
														<div class="cart__item">
															<div class="cart__wrapper">
																<div class="cart__like-del cart__desctop">
																	<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt=""></a>
																</div>
																<div class="cart__img"><img src="<?=SITE_TEMPLATE_PATH;?>/img/cart/1.png" alt=""></div>
																<div class="cart__info-box">	
																	<div class="cart__info">
																		<div class="cart__heading">
																			<span>АРТ. 30085057</span>
																			<h3>Набор для «Кейк-попс со взрывной карамелью» в белой шоколадной глазури</h3>
																		</div>
																		<div class="cart__number cart__desctop">
																			<input type="number" min="1" max="20" value="1">
																		</div>
																		<div class="cart__price cart__desctop">
																			<del>280 ₽</del>
																			<span>200 ₽</span>
																		</div>
																	</div>
																	<div class="cart__item-total cart__desctop">400 ₽</div> 
																</div> 
															</div>
															
															<!-- mob-wrap -->
															<div class="cart__mob-wrap">
																<div class="cart__mob-box">
																	<div class="cart__like-del">
																		<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt=""></a>
																	</div>
																	<div class="cart__price">
																		<del>280 ₽</del>
																		<span>200 ₽</span>
																	</div>
																</div>
																<div class="cart__mob-box">
																	<div class="cart__number">
																		<input type="number" min="1" max="20" value="1">
																	</div>
																	<div class="cart__item-total">400 ₽</div> 
																</div>
															</div>
															<!-- mob-wrap end -->
														</div>
													</div>
												</li>

											</ul>
										</div>
									</div>
									<div class="tab-pane fade" id="cancelOrders_mobile">
										<div class="cab-order__item">
											<ul id="accordion_4">

												<li>
													<div class="head">
														<div class="cab-order__top">
															<div class="cab-order__box">
																<div class="cab-order__status cab-order__status_cancel"><span>№1125005</span>| Отменен</div>
																<ul class="cab-order__deal">
																	<li><a href="#!" class="cab-order__cancel">Отменить заказ</a></li>
																	<li><a href="#!" class="cab-order__repeat">Повторить заказ</a></li>
																</ul>
															</div>
															<ul class="cab-order__data">
																<li>
																	<b>Дата создания</b>
																	<span>07.10.2020</span>
																</li>
																<li>
																	<b>Способ доставки</b>
																	<span>СДЭК (ПВЗ: Большая Академическая)</span>
																</li>
																<li>
																	<b>Трек-номер</b>
																	<span>не доступен</span>
																</li>
																<li>
																	<b>Оплата</b>
																	<span>При получении</span>
																</li>
																<li>
																	<b>Сумма</b>
																	<strong>20 642 ₽</strong>
																</li>
															</ul>
														</div>
													</div>
													
													<div class="hidden">
														<div class="cart__item">
															<div class="cart__wrapper">
																<div class="cart__like-del cart__desctop">
																	<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt=""></a>
																</div>
																<div class="cart__img"><img src="<?=SITE_TEMPLATE_PATH;?>/img/cart/1.png" alt=""></div>
																<div class="cart__info-box">	
																	<div class="cart__info">
																		<div class="cart__heading">
																			<span>АРТ. 30085057</span>
																			<h3>Набор для «Кейк-попс со взрывной карамелью» в белой шоколадной глазури</h3>
																		</div>
																		<div class="cart__number cart__desctop">
																			<input type="number" min="1" max="20" value="1">
																		</div>
																		<div class="cart__price cart__desctop">
																			<del>280 ₽</del>
																			<span>200 ₽</span>
																		</div>
																	</div>
																	<div class="cart__item-total cart__desctop">400 ₽</div> 
																</div> 
															</div>
															
															<!-- mob-wrap -->
															<div class="cart__mob-wrap">
																<div class="cart__mob-box">
																	<div class="cart__like-del">
																		<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt=""></a>
																	</div>
																	<div class="cart__price">
																		<del>280 ₽</del>
																		<span>200 ₽</span>
																	</div>
																</div>
																<div class="cart__mob-box">
																	<div class="cart__number">
																		<input type="number" min="1" max="20" value="1">
																	</div>
																	<div class="cart__item-total">400 ₽</div> 
																</div>
															</div>
															<!-- mob-wrap end -->
														</div>

														
														<div class="cart__item">
															<div class="cart__wrapper">
																<div class="cart__like-del cart__desctop">
																	<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt=""></a>
																</div>
																<div class="cart__img"><img src="<?=SITE_TEMPLATE_PATH;?>/img/cart/1.png" alt=""></div>
																<div class="cart__info-box">	
																	<div class="cart__info">
																		<div class="cart__heading">
																			<span>АРТ. 30085057</span>
																			<h3>Набор для «Кейк-попс со взрывной карамелью» в белой шоколадной глазури</h3>
																		</div>
																		<div class="cart__number cart__desctop">
																			<input type="number" min="1" max="20" value="1">
																		</div>
																		<div class="cart__price cart__desctop">
																			<del>280 ₽</del>
																			<span>200 ₽</span>
																		</div>
																	</div>
																	<div class="cart__item-total cart__desctop">400 ₽</div> 
																</div> 
															</div>
															
															<!-- mob-wrap -->
															<div class="cart__mob-wrap">
																<div class="cart__mob-box">
																	<div class="cart__like-del">
																		<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt=""></a>
																	</div>
																	<div class="cart__price">
																		<del>280 ₽</del>
																		<span>200 ₽</span>
																	</div>
																</div>
																<div class="cart__mob-box">
																	<div class="cart__number">
																		<input type="number" min="1" max="20" value="1">
																	</div>
																	<div class="cart__item-total">400 ₽</div> 
																</div>
															</div>
															<!-- mob-wrap end -->
														</div>
													</div>
												</li>

											</ul>
										</div>
									</div>
								</div>
							</div>

						</div>
						<!-- hidden -->
					</li>
					<!-- li -->

					<li>
						<p class="head_mob">Бонусы</p>
						<div class="hidden_mob">
							
							<div class="cab-bonuse">

								<ul id="accordion_4">
									<li>
										<div class="head">
											<img src="<?=SITE_TEMPLATE_PATH;?>/img/bonuses.png" alt="" class="desctop_banner">
											<img src="<?=SITE_TEMPLATE_PATH;?>/img/mobile-cab-banner.png" alt="" class="mobile_banner">
											<div class="cab-bonuse__program"><span>Бонусная программа</span></div>
										</div>

										<div class="hidden">
											<div class="cab-bonuse__text">
												<p><b>Lorem ipsum dolor</b> sit amet, consectetur adipiscing elit. Imperdiet tortor facilisi nibh vitae tellus nunc. Pretium proin aliquet curabitur sed a ac. Augue elementum, odio id viverra lectus eget quis vel fames. Risus nisi dictum mauris tincidunt vestibulum leo nunc arcu.</p>
												<p>Mauris cursus dignissim lacus scelerisque ornare morbi. Magna pulvinar id mollis <b>risus eget maecenas</b>  fermentum vulputate. Magnis vel arcu in nisi leo. Auctor urna, id non lorem feugiat cras scelerisque cras. Mi ut morbi maecenas duis sed semper fusce. Porta adipiscing augue commodo, pellentesque id egestas vivamus quis. Quis vel elit nibh in metus. Tristique donec eu, nunc scelerisque purus dignissim arcu malesuada. Vestibulum fringilla ut mauris varius senectus pharetra. Lobortis libero, convallis tellus, lectus. Adipiscing morbi aliquet a pretium ac. In pharetra adipiscing ut aliquam magna in. Consequat, at viverra id pellentesque.</p>
												<p>Facilisi fermentum et mi eget nibh viverra sem. Risus interdum et, lectus sagittis <a href="#!">lobortis nunc</a> , rhoncus. Blandit ultrices varius ornare sit vitae. Fringilla sed quis sapien, gravida in sit egestas adipiscing enim. Vulputate bibendum bibendum quis ultrices duis. Tellus commodo in viverra ut volutpat metus sagittis, amet. Tempus, nulla mus suspendisse ornare nibh. Quisque eget purus, in commodo ultrices augue. Purus non in ipsum, eu dolor lobortis consequat sit convallis. Nibh orci, vel aliquam vel facilisis platea. Pharetra, sit semper urna at. Varius lacus adipiscing a aliquam. Morbi id vestibulum porta lorem in sit ante enim. Eget neque, magna tempor nulla diam malesuada orci. Cras dignissim iaculis nunc adipiscing.</p>
											</div>
										</div>
									</li>

								</ul>

								<div class="cab-bonuse__history">
									<div class="sect-top">
										<div class="sect-top__wrap">
											<h3 class="sect-top__title">История зачислений</h3>
										</div>
									</div>
									
									<div class="cab-bonuse_mob">
										<div class="cab-bonuse_mob__item">
											<h2>Дата зачисления</h2>

											<span>07.10.2020</span>
										</div>
										<!-- cab-bonuse_mob__item -->

										<div class="cab-bonuse_mob__item">
											<h2>Заказ</h2>

											<span>№26425821</span>
										</div>
										<!-- cab-bonuse_mob__item -->

										<div class="cab-bonuse_mob__item cab-bonuse_mob__item_action">
											<h2>Действие</h2>

											<span>Начислены баллы за покупку</span>
										</div>
										<!-- cab-bonuse_mob__item -->

										<div class="cab-bonuse_mob__item">
											<h2>Баллы</h2>

											<span>+ 200</span>
										</div>
										<!-- cab-bonuse_mob__item -->
									</div>
									<!-- cab-bonuse_mob -->

									<div class="cab-bonuse_mob">
										<div class="cab-bonuse_mob__item">
											<h2>Дата зачисления</h2>

											<span>07.10.2020</span>
										</div>
										<!-- cab-bonuse_mob__item -->

										<div class="cab-bonuse_mob__item">
											<h2>Заказ</h2>

											<span>№26425821</span>
										</div>
										<!-- cab-bonuse_mob__item -->

										<div class="cab-bonuse_mob__item cab-bonuse_mob__item_action">
											<h2>Действие</h2>

											<span>Начислены баллы за покупку</span>
										</div>
										<!-- cab-bonuse_mob__item -->

										<div class="cab-bonuse_mob__item">
											<h2>Баллы</h2>

											<span>+ 200</span>
										</div>
										<!-- cab-bonuse_mob__item -->
									</div>
									<!-- cab-bonuse_mob -->

									<div class="cab-bonuse_mob">
										<div class="cab-bonuse_mob__item">
											<h2>Дата зачисления</h2>

											<span>07.10.2020</span>
										</div>
										<!-- cab-bonuse_mob__item -->

										<div class="cab-bonuse_mob__item">
											<h2>Заказ</h2>

											<span>№26425821</span>
										</div>
										<!-- cab-bonuse_mob__item -->

										<div class="cab-bonuse_mob__item cab-bonuse_mob__item_action">
											<h2>Действие</h2>

											<span>Начислены баллы за покупку</span>
										</div>
										<!-- cab-bonuse_mob__item -->

										<div class="cab-bonuse_mob__item">
											<h2>Баллы</h2>

											<span>+ 200</span>
										</div>
										<!-- cab-bonuse_mob__item -->
									</div>
									<!-- cab-bonuse_mob -->

									<div class="cab-bonuse_mob__total">
										<h2>Всего:</h2>
										<span>170</span>
									</div>

								</div>
							</div>
							<!-- cab-bonuse -->

						</div>
						<!-- hidden_mob -->
					</li>
					<!-- li -->

					<li>
						<p class="head_mob has_offer_slider">Персональные предложения</p>
						<div class="hidden_mob">
							
							<div class="cab-offers">
								<div class="sect-top">
									<div class="sect-top__wrap">
										<h3 class="sect-top__title">Товары со скидкой для вас</h3>
									</div>
								</div>

								<div class="cab-offers-products" id="CabOffersProductsSale">
									<div class="product__item">
										<div class="stickers">
											<span class="stickers__item stickers__item_new">
												New
											</span>
											<span class="stickers__item stickers__item_calc">1+4=3</span>
										</div>
										<a href="#" class="product__wishlist">
											<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt="heart">
										</a>
										<a href="#" class="product-images products__image">
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-3.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-2.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
										</a>
										<ul class="product-attributes">
											<li class="product-attributes__item">
												<span>150</span>
												<span>ккал</span>
											</li>
											<li class="product-attributes__item">
												<span>4</span>
												<span>белки</span>
											</li>
											<li class="product-attributes__item">
												<span>0,3</span>
												<span>хе</span>
											</li>
											<li class="product-attributes__item">
												<span>11</span>
												<span>углеводы</span>
											</li>
										</ul>
										<a href="#" class="products__title">
											Мармелад «венское кафе»со вкусом карамели, 180 г
										</a>
										<div class="product__bottom">
											<div class="product-price">
												<span class="product-price__old">400 ₽</span>
												<span class="product-price__new">200 ₽</span>
											</div>
											<div class="product-quantity">
												<a href="#" class="product-quantity__left"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/slider-left.svg" alt="arrow-left"></a>
												<span class="product-quantity__number">2</span>
												<a href="#" class="product-quantity__right"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/slider-right.svg" alt="arrow-right"></a>
											</div>
											<a href="#" class="product__cart-mobile">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/mobile-cart.svg" alt="cart">
											</a>
										</div>
										<a href="#" class="product__buy">В корзину</a>
									</div>
									<!-- product__item -->
									<div class="product__item">
										<div class="stickers">
											<span class="stickers__item stickers__item_new">
												New
											</span>
											<span class="stickers__item stickers__item_calc">1+4=3</span>

											<span class="stickers__item stickers__item_count">
												<b>осталось</b>
												<div class="the-final-countdown">
													<p></p>
												</div>
											</span>
										</div>
										<a href="#" class="product__wishlist">
											<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt="heart">
										</a>
										<a href="#" class="product-images products__image">
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-2.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-3.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
										</a>
										<ul class="product-attributes">
											<li class="product-attributes__item">
												<span>150</span>
												<span>ккал</span>
											</li>
											<li class="product-attributes__item">
												<span>4</span>
												<span>белки</span>
											</li>
											<li class="product-attributes__item">
												<span>0,3</span>
												<span>хе</span>
											</li>
											<li class="product-attributes__item">
												<span>11</span>
												<span>углеводы</span>
											</li>
										</ul>
										<a href="#" class="products__title">
											Мармелад «венское кафе»со вкусом карамели, 180 г
										</a>
										<div class="product__bottom">
											<div class="product-price">
												<span class="product-price__old">400 ₽</span>
												<span class="product-price__new">200 ₽</span>
											</div>
											<div class="product-quantity">
												<a href="#" class="product-quantity__left"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/slider-left.svg" alt="arrow-left"></a>
												<span class="product-quantity__number">2</span>
												<a href="#" class="product-quantity__right"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/slider-right.svg" alt="arrow-right"></a>
											</div>
											<a href="#" class="product__cart-mobile">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/mobile-cart.svg" alt="cart">
											</a>
										</div>
										<a href="#" class="product__buy">В корзину</a>
									</div>
									<!-- product__item -->
									<div class="product__item">
										<div class="stickers">
											<span class="stickers__item stickers__item_new">
												New
											</span>
											<span class="stickers__item stickers__item_calc">1+4=3</span>
										</div>
										<a href="#" class="product__wishlist">
											<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt="heart">
										</a>
										<a href="#" class="product-images products__image">
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-2.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-3.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
										</a>
										<ul class="product-attributes">
											<li class="product-attributes__item">
												<span>150</span>
												<span>ккал</span>
											</li>
											<li class="product-attributes__item">
												<span>4</span>
												<span>белки</span>
											</li>
											<li class="product-attributes__item">
												<span>0,3</span>
												<span>хе</span>
											</li>
											<li class="product-attributes__item">
												<span>11</span>
												<span>углеводы</span>
											</li>
										</ul>
										<a href="#" class="products__title">
											Мармелад «венское кафе»со вкусом карамели, 180 г
										</a>
										<div class="product__bottom">
											<div class="product-price">
												<span class="product-price__old">400 ₽</span>
												<span class="product-price__new">200 ₽</span>
											</div>
											<div class="product-quantity">
												<a href="#" class="product-quantity__left"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/slider-left.svg" alt="arrow-left"></a>
												<span class="product-quantity__number">2</span>
												<a href="#" class="product-quantity__right"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/slider-right.svg" alt="arrow-right"></a>
											</div>
											<a href="#" class="product__cart-mobile">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/mobile-cart.svg" alt="cart">
											</a>
										</div>
										<a href="#" class="product__buy">В корзину</a>
									</div>
									<!-- product__item -->
									<div class="product__item">
										<div class="stickers">
											<span class="stickers__item stickers__item_new">
												New
											</span>
											<span class="stickers__item stickers__item_calc">1+4=3</span>
										</div>
										<a href="#" class="product__wishlist">
											<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt="heart">
										</a>
										<a href="#" class="product-images products__image">
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-2.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-3.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
										</a>
										<ul class="product-attributes">
											<li class="product-attributes__item">
												<span>150</span>
												<span>ккал</span>
											</li>
											<li class="product-attributes__item">
												<span>4</span>
												<span>белки</span>
											</li>
											<li class="product-attributes__item">
												<span>0,3</span>
												<span>хе</span>
											</li>
											<li class="product-attributes__item">
												<span>11</span>
												<span>углеводы</span>
											</li>
										</ul>
										<a href="#" class="products__title">
											Мармелад «венское кафе»со вкусом карамели, 180 г
										</a>
										<div class="product__bottom">
											<div class="product-price">
												<span class="product-price__old">400 ₽</span>
												<span class="product-price__new">200 ₽</span>
											</div>
											<div class="product-quantity">
												<a href="#" class="product-quantity__left"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/slider-left.svg" alt="arrow-left"></a>
												<span class="product-quantity__number">2</span>
												<a href="#" class="product-quantity__right"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/slider-right.svg" alt="arrow-right"></a>
											</div>
											<a href="#" class="product__cart-mobile">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/mobile-cart.svg" alt="cart">
											</a>
										</div>
										<a href="#" class="product__buy">В корзину</a>
									</div>
									<!-- product__item -->
									<div class="product__item">
										<div class="stickers">
											<span class="stickers__item stickers__item_new">
												New
											</span>
											<span class="stickers__item stickers__item_calc">1+4=3</span>

											<span class="stickers__item stickers__item_count">
												<b>осталось</b>
												<div class="the-final-countdown">
													<p></p>
												</div>
											</span>
										</div>
										<a href="#" class="product__wishlist">
											<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt="heart">
										</a>
										<a href="#" class="product-images products__image">
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-2.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-3.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
										</a>
										<ul class="product-attributes">
											<li class="product-attributes__item">
												<span>150</span>
												<span>ккал</span>
											</li>
											<li class="product-attributes__item">
												<span>4</span>
												<span>белки</span>
											</li>
											<li class="product-attributes__item">
												<span>0,3</span>
												<span>хе</span>
											</li>
											<li class="product-attributes__item">
												<span>11</span>
												<span>углеводы</span>
											</li>
										</ul>
										<a href="#" class="products__title">
											Мармелад «венское кафе»со вкусом карамели, 180 г
										</a>
										<div class="product__bottom">
											<div class="product-price">
												<span class="product-price__old">400 ₽</span>
												<span class="product-price__new">200 ₽</span>
											</div>
											<div class="product-quantity">
												<a href="#" class="product-quantity__left"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/slider-left.svg" alt="arrow-left"></a>
												<span class="product-quantity__number">2</span>
												<a href="#" class="product-quantity__right"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/slider-right.svg" alt="arrow-right"></a>
											</div>
											<a href="#" class="product__cart-mobile">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/mobile-cart.svg" alt="cart">
											</a>
										</div>
										<a href="#" class="product__buy">В корзину</a>
									</div>
									<!-- product__item -->
									<div class="product__item">
										<div class="stickers">
											<span class="stickers__item stickers__item_new">
												New
											</span>
											<span class="stickers__item stickers__item_calc">1+4=3</span>
										</div>
										<a href="#" class="product__wishlist">
											<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt="heart">
										</a>
										<a href="#" class="product-images products__image">
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-2.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-3.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
										</a>
										<ul class="product-attributes">
											<li class="product-attributes__item">
												<span>150</span>
												<span>ккал</span>
											</li>
											<li class="product-attributes__item">
												<span>4</span>
												<span>белки</span>
											</li>
											<li class="product-attributes__item">
												<span>0,3</span>
												<span>хе</span>
											</li>
											<li class="product-attributes__item">
												<span>11</span>
												<span>углеводы</span>
											</li>
										</ul>
										<a href="#" class="products__title">
											Мармелад «венское кафе»со вкусом карамели, 180 г
										</a>
										<div class="product__bottom">
											<div class="product-price">
												<span class="product-price__old">400 ₽</span>
												<span class="product-price__new">200 ₽</span>
											</div>
											<div class="product-quantity">
												<a href="#" class="product-quantity__left"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/slider-left.svg" alt="arrow-left"></a>
												<span class="product-quantity__number">2</span>
												<a href="#" class="product-quantity__right"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/slider-right.svg" alt="arrow-right"></a>
											</div>
											<a href="#" class="product__cart-mobile">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/mobile-cart.svg" alt="cart">
											</a>
										</div>
										<a href="#" class="product__buy">В корзину</a>
									</div>
									<!-- product__item -->
									<div class="product__item">
										<div class="stickers">
											<span class="stickers__item stickers__item_new">
												New
											</span>
											<span class="stickers__item stickers__item_calc">1+4=3</span>
										</div>
										<a href="#" class="product__wishlist">
											<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt="heart">
										</a>
										<a href="#" class="product-images products__image">
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-2.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-3.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
										</a>
										<ul class="product-attributes">
											<li class="product-attributes__item">
												<span>150</span>
												<span>ккал</span>
											</li>
											<li class="product-attributes__item">
												<span>4</span>
												<span>белки</span>
											</li>
											<li class="product-attributes__item">
												<span>0,3</span>
												<span>хе</span>
											</li>
											<li class="product-attributes__item">
												<span>11</span>
												<span>углеводы</span>
											</li>
										</ul>
										<a href="#" class="products__title">
											Мармелад «венское кафе»со вкусом карамели, 180 г
										</a>
										<div class="product__bottom">
											<div class="product-price">
												<span class="product-price__old">400 ₽</span>
												<span class="product-price__new">200 ₽</span>
											</div>
											<div class="product-quantity">
												<a href="#" class="product-quantity__left"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/slider-left.svg" alt="arrow-left"></a>
												<span class="product-quantity__number">2</span>
												<a href="#" class="product-quantity__right"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/slider-right.svg" alt="arrow-right"></a>
											</div>
											<a href="#" class="product__cart-mobile">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/mobile-cart.svg" alt="cart">
											</a>
										</div>
										<a href="#" class="product__buy">В корзину</a>
									</div>
									<!-- product__item -->
									<div class="product__item">
										<div class="stickers">
											<span class="stickers__item stickers__item_new">
												New
											</span>
											<span class="stickers__item stickers__item_calc">1+4=3</span>
										</div>
										<a href="#" class="product__wishlist">
											<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt="heart">
										</a>
										<a href="#" class="product-images products__image">
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-2.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-3.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
										</a>
										<ul class="product-attributes">
											<li class="product-attributes__item">
												<span>150</span>
												<span>ккал</span>
											</li>
											<li class="product-attributes__item">
												<span>4</span>
												<span>белки</span>
											</li>
											<li class="product-attributes__item">
												<span>0,3</span>
												<span>хе</span>
											</li>
											<li class="product-attributes__item">
												<span>11</span>
												<span>углеводы</span>
											</li>
										</ul>
										<a href="#" class="products__title">
											Мармелад «венское кафе»со вкусом карамели, 180 г
										</a>
										<div class="product__bottom">
											<div class="product-price">
												<span class="product-price__old">400 ₽</span>
												<span class="product-price__new">200 ₽</span>
											</div>
											<div class="product-quantity">
												<a href="#" class="product-quantity__left"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/slider-left.svg" alt="arrow-left"></a>
												<span class="product-quantity__number">2</span>
												<a href="#" class="product-quantity__right"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/slider-right.svg" alt="arrow-right"></a>
											</div>
											<a href="#" class="product__cart-mobile">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/mobile-cart.svg" alt="cart">
											</a>
										</div>
										<a href="#" class="product__buy">В корзину</a>
									</div>
									<!-- product__item -->
								</div>
								<!-- cab-offers-products_sale -->

								<div class="sect-top">
									<div class="sect-top__wrap">
										<h3 class="sect-top__title">Советуем попробовать</h3>
									</div>
								</div>

								<div class="cab-offers-products" id="CabOffersProductsRecommend">
									<div class="product__item">
										<div class="stickers">
											<span class="stickers__item stickers__item_new">
												New
											</span>
											<span class="stickers__item stickers__item_calc">1+4=3</span>
										</div>
										<a href="#" class="product__wishlist">
											<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt="heart">
										</a>
										<a href="#" class="product-images products__image">
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-2.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-3.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
										</a>
										<ul class="product-attributes">
											<li class="product-attributes__item">
												<span>150</span>
												<span>ккал</span>
											</li>
											<li class="product-attributes__item">
												<span>4</span>
												<span>белки</span>
											</li>
											<li class="product-attributes__item">
												<span>0,3</span>
												<span>хе</span>
											</li>
											<li class="product-attributes__item">
												<span>11</span>
												<span>углеводы</span>
											</li>
										</ul>
										<a href="#" class="products__title">
											Мармелад «венское кафе»со вкусом карамели, 180 г
										</a>
										<div class="product__bottom">
											<div class="product-price">
												<span class="product-price__old">400 ₽</span>
												<span class="product-price__new">200 ₽</span>
											</div>
											<div class="product-quantity">
												<a href="#" class="product-quantity__left"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/slider-left.svg" alt="arrow-left"></a>
												<span class="product-quantity__number">2</span>
												<a href="#" class="product-quantity__right"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/slider-right.svg" alt="arrow-right"></a>
											</div>
											<a href="#" class="product__cart-mobile">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/mobile-cart.svg" alt="cart">
											</a>
										</div>
										<a href="#" class="product__buy">В корзину</a>
									</div>
									<!-- product__item -->
									<div class="product__item">
										<div class="stickers">
											<span class="stickers__item stickers__item_new">
												New
											</span>
											<span class="stickers__item stickers__item_calc">1+4=3</span>
										</div>
										<a href="#" class="product__wishlist">
											<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt="heart">
										</a>
										<a href="#" class="product-images products__image">
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-2.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-3.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
										</a>
										<ul class="product-attributes">
											<li class="product-attributes__item">
												<span>150</span>
												<span>ккал</span>
											</li>
											<li class="product-attributes__item">
												<span>4</span>
												<span>белки</span>
											</li>
											<li class="product-attributes__item">
												<span>0,3</span>
												<span>хе</span>
											</li>
											<li class="product-attributes__item">
												<span>11</span>
												<span>углеводы</span>
											</li>
										</ul>
										<a href="#" class="products__title">
											Мармелад «венское кафе»со вкусом карамели, 180 г
										</a>
										<div class="product__bottom">
											<div class="product-price">
												<span class="product-price__old">400 ₽</span>
												<span class="product-price__new">200 ₽</span>
											</div>
											<div class="product-quantity">
												<a href="#" class="product-quantity__left"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/slider-left.svg" alt="arrow-left"></a>
												<span class="product-quantity__number">2</span>
												<a href="#" class="product-quantity__right"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/slider-right.svg" alt="arrow-right"></a>
											</div>
											<a href="#" class="product__cart-mobile">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/mobile-cart.svg" alt="cart">
											</a>
										</div>
										<a href="#" class="product__buy">В корзину</a>
									</div>
									<!-- product__item -->
									<div class="product__item">
										<div class="stickers">
											<span class="stickers__item stickers__item_new">
												New
											</span>
											<span class="stickers__item stickers__item_calc">1+4=3</span>

											<span class="stickers__item stickers__item_count">
												<b>осталось</b>
												<div class="the-final-countdown">
													<p></p>
												</div>
											</span>
										</div>
										<a href="#" class="product__wishlist">
											<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt="heart">
										</a>
										<a href="#" class="product-images products__image">
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-2.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-3.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
										</a>
										<ul class="product-attributes">
											<li class="product-attributes__item">
												<span>150</span>
												<span>ккал</span>
											</li>
											<li class="product-attributes__item">
												<span>4</span>
												<span>белки</span>
											</li>
											<li class="product-attributes__item">
												<span>0,3</span>
												<span>хе</span>
											</li>
											<li class="product-attributes__item">
												<span>11</span>
												<span>углеводы</span>
											</li>
										</ul>
										<a href="#" class="products__title">
											Мармелад «венское кафе»со вкусом карамели, 180 г
										</a>
										<div class="product__bottom">
											<div class="product-price">
												<span class="product-price__old">400 ₽</span>
												<span class="product-price__new">200 ₽</span>
											</div>
											<div class="product-quantity">
												<a href="#" class="product-quantity__left"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/slider-left.svg" alt="arrow-left"></a>
												<span class="product-quantity__number">2</span>
												<a href="#" class="product-quantity__right"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/slider-right.svg" alt="arrow-right"></a>
											</div>
											<a href="#" class="product__cart-mobile">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/mobile-cart.svg" alt="cart">
											</a>
										</div>
										<a href="#" class="product__buy">В корзину</a>
									</div>
									<!-- product__item -->
									<div class="product__item">
										<div class="stickers">
											<span class="stickers__item stickers__item_new">
												New
											</span>
											<span class="stickers__item stickers__item_calc">1+4=3</span>
										</div>
										<a href="#" class="product__wishlist">
											<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt="heart">
										</a>
										<a href="#" class="product-images products__image">
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-2.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-3.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
										</a>
										<ul class="product-attributes">
											<li class="product-attributes__item">
												<span>150</span>
												<span>ккал</span>
											</li>
											<li class="product-attributes__item">
												<span>4</span>
												<span>белки</span>
											</li>
											<li class="product-attributes__item">
												<span>0,3</span>
												<span>хе</span>
											</li>
											<li class="product-attributes__item">
												<span>11</span>
												<span>углеводы</span>
											</li>
										</ul>
										<a href="#" class="products__title">
											Мармелад «венское кафе»со вкусом карамели, 180 г
										</a>
										<div class="product__bottom">
											<div class="product-price">
												<span class="product-price__old">400 ₽</span>
												<span class="product-price__new">200 ₽</span>
											</div>
											<div class="product-quantity">
												<a href="#" class="product-quantity__left"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/slider-left.svg" alt="arrow-left"></a>
												<span class="product-quantity__number">2</span>
												<a href="#" class="product-quantity__right"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/slider-right.svg" alt="arrow-right"></a>
											</div>
											<a href="#" class="product__cart-mobile">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/mobile-cart.svg" alt="cart">
											</a>
										</div>
										<a href="#" class="product__buy">В корзину</a>
									</div>
									<!-- product__item -->
								</div>
								<!-- cab-offers-products_sale -->

							</div>

						</div>
					</li>
					<!-- li -->

					<li>
						<p class="head_mob">Мои отзывы</p>
						<div class="hidden_mob">
							
							<div class="cab-reviews">
								<div class="cab-review">
									<div class="cab-review_top">
										<div class="cab-review_status cab-review_status_mobile cab-review_status_good">
											Отзыв опубликован
										</div>

										<a href="#!" class="cab-review__img">
											<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-2-rem-bg.png" alt="">
										</a>

										<div class="cab-review_data">
											<div class="cab-review_status cab-review_status_desctop cab-review_status_good">
												Отзыв опубликован
											</div>
											<!-- cab-review_status -->
											<div class="cab-review_rating">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/fill-star.svg" alt="">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/fill-star.svg" alt="">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/fill-star.svg" alt="">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/fill-star.svg" alt="">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/unfill-star.svg" alt="">
											</div>
											<!-- cab-review_rating -->

											<a href="#!" class="cab-review_title">
												Высокобелковые палочки в шоколаде без сахара, 500 г (10x50 г)
											</a>
										</div>
										<!-- cab-review_data -->
									</div>
									<!-- cab-review_top -->

									<div class="cab-review__info">
										<h3>Достоинства</h3>

										<div>
											<p>Лёгкий, USB, дует в 3 мощностях</p>
										</div>
									</div>
									<!-- cab-review__info -->

									<div class="cab-review__info">
										<h3>Недостатки</h3>

										<div>
											<p>Дешёвый пластик, маленькая мощность, высокая цена</p>
										</div>
									</div>
									<!-- cab-review__info -->

									<div class="cab-review__info">
										<h3>Комментарий</h3>

										<div>
											<p>Состоит из компьютерного кулера, так называемых фильтров, которые не могут ничего фильтровать, так как стоять вдоль, а не поперёк, и пластиковой коробки. Если сомневаетесь, то лучше возьмите небольшой настольный вентилятор, по мощнее будет</p>
										</div>
									</div>
									<!-- cab-review__info -->


									<div class="cab-review_bottom">
										<div class="cab-review_left">
											<span class="cab-review__date">10 месяцев назад</span>
										</div>
										<!-- cab-review_left -->

										<div class="cab-review_right">
											<div class="cab-review__views">
												<svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
													<path d="M0.13982 10.5723C0.336359 10.3035 5.01923 3.98926 11.0001 3.98926C16.981 3.98926 21.6641 10.3035 21.8604 10.5721C22.0465 10.8271 22.0465 11.1731 21.8604 11.4282C21.6641 11.697 16.981 18.0112 11.0001 18.0112C5.01923 18.0112 0.336359 11.697 0.13982 11.4284C-0.0465794 11.1733 -0.0465794 10.8271 0.13982 10.5723ZM11.0001 16.5607C15.4057 16.5607 19.2213 12.3698 20.3509 10.9998C19.2228 9.62849 15.4151 5.4398 11.0001 5.4398C6.59477 5.4398 2.77935 9.62995 1.64936 11.0007C2.77742 12.372 6.5851 16.5607 11.0001 16.5607Z" fill="#3C464D"/>
													<path d="M11.0003 6.64844C13.3997 6.64844 15.3519 8.60064 15.3519 11.0001C15.3519 13.3996 13.3997 15.3518 11.0003 15.3518C8.6008 15.3518 6.6486 13.3996 6.6486 11.0001C6.6486 8.60064 8.6008 6.64844 11.0003 6.64844ZM11.0003 13.9012C12.6 13.9012 13.9013 12.5998 13.9013 11.0001C13.9013 9.40042 12.5999 8.09902 11.0003 8.09902C9.40058 8.09902 8.09918 9.40042 8.09918 11.0001C8.09918 12.5998 9.40053 13.9012 11.0003 13.9012Z" fill="#3C464D"/>
												</svg>

												<span>10</span>
											</div>
											<!-- cab-review__views -->

											<div class="cab-review__score">
												<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/score_top.svg" alt=""></a>
												<span>10</span>
											</div>

											<div class="cab-review__score">
												<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/score_bottom.svg" alt=""></a>
												<span>2</span>
											</div>
										</div>
										<!-- cab-review_right -->
									</div>
								</div>
								<!-- cab-review -->

								<div class="cab-review">
									<div class="cab-review_top">
										<div class="cab-review_status cab-review_status_mobile cab-review_status_good">
											Отзыв опубликован
										</div>

										<a href="#!" class="cab-review__img">
											<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-2-rem-bg.png" alt="">
										</a>

										<div class="cab-review_data">
											<div class="cab-review_status cab-review_status_desctop cab-review_status_good">
												Отзыв опубликован
											</div>
											<!-- cab-review_status -->
											<div class="cab-review_rating">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/fill-star.svg" alt="">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/fill-star.svg" alt="">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/fill-star.svg" alt="">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/fill-star.svg" alt="">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/unfill-star.svg" alt="">
											</div>
											<!-- cab-review_rating -->

											<a href="#!" class="cab-review_title">
												Высокобелковые палочки в шоколаде без сахара, 500 г (10x50 г)
											</a>
										</div>
										<!-- cab-review_data -->
									</div>
									<!-- cab-review_top -->

									<div class="cab-review__info">
										<h3>Достоинства</h3>

										<div>
											<p>Лёгкий, USB, дует в 3 мощностях</p>
										</div>
									</div>
									<!-- cab-review__info -->

									<div class="cab-review__info">
										<h3>Недостатки</h3>

										<div>
											<p>Дешёвый пластик, маленькая мощность, высокая цена</p>
										</div>
									</div>
									<!-- cab-review__info -->

									<div class="cab-review__info">
										<h3>Комментарий</h3>

										<div>
											<p>Состоит из компьютерного кулера, так называемых фильтров, которые не могут ничего фильтровать, так как стоять вдоль, а не поперёк, и пластиковой коробки. Если сомневаетесь, то лучше возьмите небольшой настольный вентилятор, по мощнее будет</p>
										</div>
									</div>
									<!-- cab-review__info -->


									<div class="cab-review_bottom">
										<div class="cab-review_left">
											<span class="cab-review__date">10 месяцев назад</span>
										</div>
										<!-- cab-review_left -->

										<div class="cab-review_right">
											<div class="cab-review__views">
												<svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
													<path d="M0.13982 10.5723C0.336359 10.3035 5.01923 3.98926 11.0001 3.98926C16.981 3.98926 21.6641 10.3035 21.8604 10.5721C22.0465 10.8271 22.0465 11.1731 21.8604 11.4282C21.6641 11.697 16.981 18.0112 11.0001 18.0112C5.01923 18.0112 0.336359 11.697 0.13982 11.4284C-0.0465794 11.1733 -0.0465794 10.8271 0.13982 10.5723ZM11.0001 16.5607C15.4057 16.5607 19.2213 12.3698 20.3509 10.9998C19.2228 9.62849 15.4151 5.4398 11.0001 5.4398C6.59477 5.4398 2.77935 9.62995 1.64936 11.0007C2.77742 12.372 6.5851 16.5607 11.0001 16.5607Z" fill="#3C464D"/>
													<path d="M11.0003 6.64844C13.3997 6.64844 15.3519 8.60064 15.3519 11.0001C15.3519 13.3996 13.3997 15.3518 11.0003 15.3518C8.6008 15.3518 6.6486 13.3996 6.6486 11.0001C6.6486 8.60064 8.6008 6.64844 11.0003 6.64844ZM11.0003 13.9012C12.6 13.9012 13.9013 12.5998 13.9013 11.0001C13.9013 9.40042 12.5999 8.09902 11.0003 8.09902C9.40058 8.09902 8.09918 9.40042 8.09918 11.0001C8.09918 12.5998 9.40053 13.9012 11.0003 13.9012Z" fill="#3C464D"/>
												</svg>

												<span>10</span>
											</div>
											<!-- cab-review__views -->

											<div class="cab-review__score">
												<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/score_top.svg" alt=""></a>
												<span>10</span>
											</div>

											<div class="cab-review__score">
												<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/score_bottom.svg" alt=""></a>
												<span>2</span>
											</div>
										</div>
										<!-- cab-review_right -->
									</div>
								</div>
								<!-- cab-review -->

								<div class="cab-review">
									<div class="cab-review_top">
										<div class="cab-review_status cab-review_status_mobile cab-review_status_good">
											Отзыв опубликован
										</div>

										<a href="#!" class="cab-review__img">
											<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-2-rem-bg.png" alt="">
										</a>

										<div class="cab-review_data">
											<div class="cab-review_status cab-review_status_desctop cab-review_status_good">
												Отзыв опубликован
											</div>
											<!-- cab-review_status -->
											<div class="cab-review_rating">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/fill-star.svg" alt="">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/fill-star.svg" alt="">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/fill-star.svg" alt="">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/fill-star.svg" alt="">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/unfill-star.svg" alt="">
											</div>
											<!-- cab-review_rating -->

											<a href="#!" class="cab-review_title">
												Высокобелковые палочки в шоколаде без сахара, 500 г (10x50 г)
											</a>
										</div>
										<!-- cab-review_data -->
									</div>
									<!-- cab-review_top -->

									<div class="cab-review__info">
										<h3>Достоинства</h3>

										<div>
											<p>Лёгкий, USB, дует в 3 мощностях</p>
										</div>
									</div>
									<!-- cab-review__info -->

									<div class="cab-review__info">
										<h3>Недостатки</h3>

										<div>
											<p>Дешёвый пластик, маленькая мощность, высокая цена</p>
										</div>
									</div>
									<!-- cab-review__info -->

									<div class="cab-review__info">
										<h3>Комментарий</h3>

										<div>
											<p>Состоит из компьютерного кулера, так называемых фильтров, которые не могут ничего фильтровать, так как стоять вдоль, а не поперёк, и пластиковой коробки. Если сомневаетесь, то лучше возьмите небольшой настольный вентилятор, по мощнее будет</p>
										</div>
									</div>
									<!-- cab-review__info -->


									<div class="cab-review_bottom">
										<div class="cab-review_left">
											<span class="cab-review__date">10 месяцев назад</span>
										</div>
										<!-- cab-review_left -->

										<div class="cab-review_right">
											<div class="cab-review__views">
												<svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
													<path d="M0.13982 10.5723C0.336359 10.3035 5.01923 3.98926 11.0001 3.98926C16.981 3.98926 21.6641 10.3035 21.8604 10.5721C22.0465 10.8271 22.0465 11.1731 21.8604 11.4282C21.6641 11.697 16.981 18.0112 11.0001 18.0112C5.01923 18.0112 0.336359 11.697 0.13982 11.4284C-0.0465794 11.1733 -0.0465794 10.8271 0.13982 10.5723ZM11.0001 16.5607C15.4057 16.5607 19.2213 12.3698 20.3509 10.9998C19.2228 9.62849 15.4151 5.4398 11.0001 5.4398C6.59477 5.4398 2.77935 9.62995 1.64936 11.0007C2.77742 12.372 6.5851 16.5607 11.0001 16.5607Z" fill="#3C464D"/>
													<path d="M11.0003 6.64844C13.3997 6.64844 15.3519 8.60064 15.3519 11.0001C15.3519 13.3996 13.3997 15.3518 11.0003 15.3518C8.6008 15.3518 6.6486 13.3996 6.6486 11.0001C6.6486 8.60064 8.6008 6.64844 11.0003 6.64844ZM11.0003 13.9012C12.6 13.9012 13.9013 12.5998 13.9013 11.0001C13.9013 9.40042 12.5999 8.09902 11.0003 8.09902C9.40058 8.09902 8.09918 9.40042 8.09918 11.0001C8.09918 12.5998 9.40053 13.9012 11.0003 13.9012Z" fill="#3C464D"/>
												</svg>

												<span>10</span>
											</div>
											<!-- cab-review__views -->

											<div class="cab-review__score">
												<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/score_top.svg" alt=""></a>
												<span>10</span>
											</div>

											<div class="cab-review__score">
												<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/score_bottom.svg" alt=""></a>
												<span>2</span>
											</div>
										</div>
										<!-- cab-review_right -->
									</div>
								</div>
								<!-- cab-review -->

								<div class="cab-review">
									<div class="cab-review_top">
										<div class="cab-review_status cab-review_status_mobile cab-review_status_good">
											Отзыв опубликован
										</div>

										<a href="#!" class="cab-review__img">
											<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-2-rem-bg.png" alt="">
										</a>

										<div class="cab-review_data">
											<div class="cab-review_status cab-review_status_desctop cab-review_status_good">
												Отзыв опубликован
											</div>
											<!-- cab-review_status -->
											<div class="cab-review_rating">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/fill-star.svg" alt="">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/fill-star.svg" alt="">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/fill-star.svg" alt="">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/fill-star.svg" alt="">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/unfill-star.svg" alt="">
											</div>
											<!-- cab-review_rating -->

											<a href="#!" class="cab-review_title">
												Высокобелковые палочки в шоколаде без сахара, 500 г (10x50 г)
											</a>
										</div>
										<!-- cab-review_data -->
									</div>
									<!-- cab-review_top -->

									<div class="cab-review__info">
										<h3>Достоинства</h3>

										<div>
											<p>Лёгкий, USB, дует в 3 мощностях</p>
										</div>
									</div>
									<!-- cab-review__info -->

									<div class="cab-review__info">
										<h3>Недостатки</h3>

										<div>
											<p>Дешёвый пластик, маленькая мощность, высокая цена</p>
										</div>
									</div>
									<!-- cab-review__info -->

									<div class="cab-review__info">
										<h3>Комментарий</h3>

										<div>
											<p>Состоит из компьютерного кулера, так называемых фильтров, которые не могут ничего фильтровать, так как стоять вдоль, а не поперёк, и пластиковой коробки. Если сомневаетесь, то лучше возьмите небольшой настольный вентилятор, по мощнее будет</p>
										</div>
									</div>
									<!-- cab-review__info -->


									<div class="cab-review_bottom">
										<div class="cab-review_left">
											<span class="cab-review__date">10 месяцев назад</span>
										</div>
										<!-- cab-review_left -->

										<div class="cab-review_right">
											<div class="cab-review__views">
												<svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
													<path d="M0.13982 10.5723C0.336359 10.3035 5.01923 3.98926 11.0001 3.98926C16.981 3.98926 21.6641 10.3035 21.8604 10.5721C22.0465 10.8271 22.0465 11.1731 21.8604 11.4282C21.6641 11.697 16.981 18.0112 11.0001 18.0112C5.01923 18.0112 0.336359 11.697 0.13982 11.4284C-0.0465794 11.1733 -0.0465794 10.8271 0.13982 10.5723ZM11.0001 16.5607C15.4057 16.5607 19.2213 12.3698 20.3509 10.9998C19.2228 9.62849 15.4151 5.4398 11.0001 5.4398C6.59477 5.4398 2.77935 9.62995 1.64936 11.0007C2.77742 12.372 6.5851 16.5607 11.0001 16.5607Z" fill="#3C464D"/>
													<path d="M11.0003 6.64844C13.3997 6.64844 15.3519 8.60064 15.3519 11.0001C15.3519 13.3996 13.3997 15.3518 11.0003 15.3518C8.6008 15.3518 6.6486 13.3996 6.6486 11.0001C6.6486 8.60064 8.6008 6.64844 11.0003 6.64844ZM11.0003 13.9012C12.6 13.9012 13.9013 12.5998 13.9013 11.0001C13.9013 9.40042 12.5999 8.09902 11.0003 8.09902C9.40058 8.09902 8.09918 9.40042 8.09918 11.0001C8.09918 12.5998 9.40053 13.9012 11.0003 13.9012Z" fill="#3C464D"/>
												</svg>

												<span>10</span>
											</div>
											<!-- cab-review__views -->

											<div class="cab-review__score">
												<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/score_top.svg" alt=""></a>
												<span>10</span>
											</div>

											<div class="cab-review__score">
												<a href="#!"><img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/score_bottom.svg" alt=""></a>
												<span>2</span>
											</div>
										</div>
										<!-- cab-review_right -->
									</div>
								</div>
								<!-- cab-review -->
							</div>
							<!-- cab-reviews -->

						</div>
						<!-- hidden -->
					</li>
					<!-- li -->

					<li>
						<p class="head_mob">Подписки</p>
						<div class="hidden_mob">
							
							<div class="cab-subscriptions">
								<div class="cab-subscriptions-info">
									<div class="cab-subscriptions_top">
										<h3>Название рассылки</h3>
										<h3>Статус</h3>
									</div>

									<div class="cab-subscriptions__item">
										<h4>Акции и предложения</h4>

										<label class="switch">
											<input type="checkbox" checked="">
											<span class="switch_round round"></span>
										</label>
									</div>
									<!-- cab-subscriptions__item -->

									<div class="cab-subscriptions__item">
										<h4>Полезные статьи, новости и анонсы мероприятий</h4>

										<label class="switch">
											<input type="checkbox">
											<span class="switch_round round"></span>
										</label>
									</div>
									<!-- cab-subscriptions__item -->

									<div class="cab-subscriptions__item">
										<h4>Уведомления о программе лояльности</h4>

										<label class="switch">
											<input type="checkbox" checked="">
											<span class="switch_round round"></span>
										</label>

									</div>
									<!-- cab-subscriptions__item -->
								</div>
								<!-- cab-subscriptions-info -->

								<div class="section-top mt-40">
									<div class="section-top__wrap">
										<h3 class="section-top__title">Подписка на поступление <span>18</span> товаров</h3>
									</div>
								</div>

								<div class="subscriptions__slider__inner products products-buy product-remember">
									<div class="product__item">
										<div class="stickers">
											<span class="stickers__item stickers__item_new">
												New
											</span>
										</div>
										<a href="#" class="product__wishlist">
											<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt="heart">
										</a>
										<a href="#" class="product-images products__image">
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-2.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-3.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
										</a>
										<ul class="product-attributes">
											<li class="product-attributes__item">
												<span>150</span>
												<span>ккал</span>
											</li>
											<li class="product-attributes__item">
												<span>4</span>
												<span>белки</span>
											</li>
											<li class="product-attributes__item">
												<span>0,3</span>
												<span>хе</span>
											</li>
											<li class="product-attributes__item">
												<span>11</span>
												<span>углеводы</span>
											</li>
										</ul>
										<a href="#" class="products__title">
											Мармелад «венское кафе»со вкусом карамели, 180 г
										</a>
										<div class="product__bottom">
											<a href="#!" class="cab-subscriptions_cancel">
												<svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
													<g clip-path="url(#clip0)">
														<path d="M8.00005 0C3.58882 0 0 3.58882 0 8.00005C0 12.4113 3.58882 16.0001 8.00005 16.0001C12.4113 16.0001 16.0001 12.4113 16.0001 8.00005C16.0001 3.58882 12.4112 0 8.00005 0ZM8.00005 14.6286C4.34511 14.6286 1.37145 11.655 1.37145 8.00005C1.37145 4.34511 4.34511 1.37145 8.00005 1.37145C11.655 1.37145 14.6286 4.34511 14.6286 8.00005C14.6286 11.655 11.6549 14.6286 8.00005 14.6286Z" fill="#3C464D"/>
														<path d="M11.0955 4.905C10.8277 4.63714 10.3933 4.63714 10.1259 4.905L8.00003 7.03118L5.87412 4.905C5.60626 4.63714 5.17237 4.63714 4.90451 4.905C4.63665 5.17286 4.63665 5.60675 4.90451 5.87461L7.03042 8.0007L4.90442 10.1269C4.63656 10.3947 4.63656 10.8286 4.90442 11.0965C5.0383 11.2304 5.21379 11.2974 5.38927 11.2974C5.56475 11.2974 5.74042 11.2305 5.87412 11.0965L8.00003 8.9704L10.1259 11.0965C10.2598 11.2304 10.4353 11.2974 10.6108 11.2974C10.7863 11.2974 10.9617 11.2305 11.0956 11.0965C11.3635 10.8286 11.3635 10.3947 11.0956 10.1269L8.96964 8.00079L11.0955 5.8747C11.3634 5.60684 11.3634 5.17286 11.0955 4.905Z" fill="#3C464D"/>
													</g>
													<defs>
														<clipPath id="clip0">
															<rect width="16" height="16" fill="white"/>
														</clipPath>
													</defs>
												</svg>

												<span>Отменить подписку</span>
											</a>
										</div>
										<!-- product__bottom -->
									</div>
									<!-- product__item -->

									<div class="product__item">
										<div class="stickers">
											<span class="stickers__item stickers__item_new">
												New
											</span>
										</div>
										<a href="#" class="product__wishlist">
											<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt="heart">
										</a>
										<a href="#" class="product-images products__image">
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-2.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-3.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
										</a>
										<ul class="product-attributes">
											<li class="product-attributes__item">
												<span>150</span>
												<span>ккал</span>
											</li>
											<li class="product-attributes__item">
												<span>4</span>
												<span>белки</span>
											</li>
											<li class="product-attributes__item">
												<span>0,3</span>
												<span>хе</span>
											</li>
											<li class="product-attributes__item">
												<span>11</span>
												<span>углеводы</span>
											</li>
										</ul>
										<a href="#" class="products__title">
											Мармелад «венское кафе»со вкусом карамели, 180 г
										</a>
										<div class="product__bottom">
											<a href="#!" class="cab-subscriptions_cancel">
												<svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
													<g clip-path="url(#clip0)">
														<path d="M8.00005 0C3.58882 0 0 3.58882 0 8.00005C0 12.4113 3.58882 16.0001 8.00005 16.0001C12.4113 16.0001 16.0001 12.4113 16.0001 8.00005C16.0001 3.58882 12.4112 0 8.00005 0ZM8.00005 14.6286C4.34511 14.6286 1.37145 11.655 1.37145 8.00005C1.37145 4.34511 4.34511 1.37145 8.00005 1.37145C11.655 1.37145 14.6286 4.34511 14.6286 8.00005C14.6286 11.655 11.6549 14.6286 8.00005 14.6286Z" fill="#3C464D"/>
														<path d="M11.0955 4.905C10.8277 4.63714 10.3933 4.63714 10.1259 4.905L8.00003 7.03118L5.87412 4.905C5.60626 4.63714 5.17237 4.63714 4.90451 4.905C4.63665 5.17286 4.63665 5.60675 4.90451 5.87461L7.03042 8.0007L4.90442 10.1269C4.63656 10.3947 4.63656 10.8286 4.90442 11.0965C5.0383 11.2304 5.21379 11.2974 5.38927 11.2974C5.56475 11.2974 5.74042 11.2305 5.87412 11.0965L8.00003 8.9704L10.1259 11.0965C10.2598 11.2304 10.4353 11.2974 10.6108 11.2974C10.7863 11.2974 10.9617 11.2305 11.0956 11.0965C11.3635 10.8286 11.3635 10.3947 11.0956 10.1269L8.96964 8.00079L11.0955 5.8747C11.3634 5.60684 11.3634 5.17286 11.0955 4.905Z" fill="#3C464D"/>
													</g>
													<defs>
														<clipPath id="clip0">
															<rect width="16" height="16" fill="white"/>
														</clipPath>
													</defs>
												</svg>

												<span>Отменить подписку</span>
											</a>
										</div>
										<!-- product__bottom -->
									</div>
									<!-- product__item -->

									<div class="product__item">
										<div class="stickers">
											<span class="stickers__item stickers__item_new">
												New
											</span>
										</div>
										<a href="#" class="product__wishlist">
											<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt="heart">
										</a>
										<a href="#" class="product-images products__image">
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-2.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-3.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
										</a>
										<ul class="product-attributes">
											<li class="product-attributes__item">
												<span>150</span>
												<span>ккал</span>
											</li>
											<li class="product-attributes__item">
												<span>4</span>
												<span>белки</span>
											</li>
											<li class="product-attributes__item">
												<span>0,3</span>
												<span>хе</span>
											</li>
											<li class="product-attributes__item">
												<span>11</span>
												<span>углеводы</span>
											</li>
										</ul>
										<a href="#" class="products__title">
											Мармелад «венское кафе»со вкусом карамели, 180 г
										</a>
										<div class="product__bottom">
											<a href="#!" class="cab-subscriptions_cancel">
												<svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
													<g clip-path="url(#clip0)">
														<path d="M8.00005 0C3.58882 0 0 3.58882 0 8.00005C0 12.4113 3.58882 16.0001 8.00005 16.0001C12.4113 16.0001 16.0001 12.4113 16.0001 8.00005C16.0001 3.58882 12.4112 0 8.00005 0ZM8.00005 14.6286C4.34511 14.6286 1.37145 11.655 1.37145 8.00005C1.37145 4.34511 4.34511 1.37145 8.00005 1.37145C11.655 1.37145 14.6286 4.34511 14.6286 8.00005C14.6286 11.655 11.6549 14.6286 8.00005 14.6286Z" fill="#3C464D"/>
														<path d="M11.0955 4.905C10.8277 4.63714 10.3933 4.63714 10.1259 4.905L8.00003 7.03118L5.87412 4.905C5.60626 4.63714 5.17237 4.63714 4.90451 4.905C4.63665 5.17286 4.63665 5.60675 4.90451 5.87461L7.03042 8.0007L4.90442 10.1269C4.63656 10.3947 4.63656 10.8286 4.90442 11.0965C5.0383 11.2304 5.21379 11.2974 5.38927 11.2974C5.56475 11.2974 5.74042 11.2305 5.87412 11.0965L8.00003 8.9704L10.1259 11.0965C10.2598 11.2304 10.4353 11.2974 10.6108 11.2974C10.7863 11.2974 10.9617 11.2305 11.0956 11.0965C11.3635 10.8286 11.3635 10.3947 11.0956 10.1269L8.96964 8.00079L11.0955 5.8747C11.3634 5.60684 11.3634 5.17286 11.0955 4.905Z" fill="#3C464D"/>
													</g>
													<defs>
														<clipPath id="clip0">
															<rect width="16" height="16" fill="white"/>
														</clipPath>
													</defs>
												</svg>

												<span>Отменить подписку</span>
											</a>
										</div>
										<!-- product__bottom -->
									</div>
									<!-- product__item -->

									<div class="product__item">
										<div class="stickers">
											<span class="stickers__item stickers__item_new">
												New
											</span>
										</div>
										<a href="#" class="product__wishlist">
											<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt="heart">
										</a>
										<a href="#" class="product-images products__image">
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-2.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-3.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
										</a>
										<ul class="product-attributes">
											<li class="product-attributes__item">
												<span>150</span>
												<span>ккал</span>
											</li>
											<li class="product-attributes__item">
												<span>4</span>
												<span>белки</span>
											</li>
											<li class="product-attributes__item">
												<span>0,3</span>
												<span>хе</span>
											</li>
											<li class="product-attributes__item">
												<span>11</span>
												<span>углеводы</span>
											</li>
										</ul>
										<a href="#" class="products__title">
											Мармелад «венское кафе»со вкусом карамели, 180 г
										</a>
										<div class="product__bottom">
											<a href="#!" class="cab-subscriptions_cancel">
												<svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
													<g clip-path="url(#clip0)">
														<path d="M8.00005 0C3.58882 0 0 3.58882 0 8.00005C0 12.4113 3.58882 16.0001 8.00005 16.0001C12.4113 16.0001 16.0001 12.4113 16.0001 8.00005C16.0001 3.58882 12.4112 0 8.00005 0ZM8.00005 14.6286C4.34511 14.6286 1.37145 11.655 1.37145 8.00005C1.37145 4.34511 4.34511 1.37145 8.00005 1.37145C11.655 1.37145 14.6286 4.34511 14.6286 8.00005C14.6286 11.655 11.6549 14.6286 8.00005 14.6286Z" fill="#3C464D"/>
														<path d="M11.0955 4.905C10.8277 4.63714 10.3933 4.63714 10.1259 4.905L8.00003 7.03118L5.87412 4.905C5.60626 4.63714 5.17237 4.63714 4.90451 4.905C4.63665 5.17286 4.63665 5.60675 4.90451 5.87461L7.03042 8.0007L4.90442 10.1269C4.63656 10.3947 4.63656 10.8286 4.90442 11.0965C5.0383 11.2304 5.21379 11.2974 5.38927 11.2974C5.56475 11.2974 5.74042 11.2305 5.87412 11.0965L8.00003 8.9704L10.1259 11.0965C10.2598 11.2304 10.4353 11.2974 10.6108 11.2974C10.7863 11.2974 10.9617 11.2305 11.0956 11.0965C11.3635 10.8286 11.3635 10.3947 11.0956 10.1269L8.96964 8.00079L11.0955 5.8747C11.3634 5.60684 11.3634 5.17286 11.0955 4.905Z" fill="#3C464D"/>
													</g>
													<defs>
														<clipPath id="clip0">
															<rect width="16" height="16" fill="white"/>
														</clipPath>
													</defs>
												</svg>

												<span>Отменить подписку</span>
											</a>
										</div>
										<!-- product__bottom -->
									</div>
									<!-- product__item -->

									<div class="product__item">
										<div class="stickers">
											<span class="stickers__item stickers__item_new">
												New
											</span>
										</div>
										<a href="#" class="product__wishlist">
											<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt="heart">
										</a>
										<a href="#" class="product-images products__image">
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-2.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-3.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
										</a>
										<ul class="product-attributes">
											<li class="product-attributes__item">
												<span>150</span>
												<span>ккал</span>
											</li>
											<li class="product-attributes__item">
												<span>4</span>
												<span>белки</span>
											</li>
											<li class="product-attributes__item">
												<span>0,3</span>
												<span>хе</span>
											</li>
											<li class="product-attributes__item">
												<span>11</span>
												<span>углеводы</span>
											</li>
										</ul>
										<a href="#" class="products__title">
											Мармелад «венское кафе»со вкусом карамели, 180 г
										</a>
										<div class="product__bottom">
											<a href="#!" class="cab-subscriptions_cancel">
												<svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
													<g clip-path="url(#clip0)">
														<path d="M8.00005 0C3.58882 0 0 3.58882 0 8.00005C0 12.4113 3.58882 16.0001 8.00005 16.0001C12.4113 16.0001 16.0001 12.4113 16.0001 8.00005C16.0001 3.58882 12.4112 0 8.00005 0ZM8.00005 14.6286C4.34511 14.6286 1.37145 11.655 1.37145 8.00005C1.37145 4.34511 4.34511 1.37145 8.00005 1.37145C11.655 1.37145 14.6286 4.34511 14.6286 8.00005C14.6286 11.655 11.6549 14.6286 8.00005 14.6286Z" fill="#3C464D"/>
														<path d="M11.0955 4.905C10.8277 4.63714 10.3933 4.63714 10.1259 4.905L8.00003 7.03118L5.87412 4.905C5.60626 4.63714 5.17237 4.63714 4.90451 4.905C4.63665 5.17286 4.63665 5.60675 4.90451 5.87461L7.03042 8.0007L4.90442 10.1269C4.63656 10.3947 4.63656 10.8286 4.90442 11.0965C5.0383 11.2304 5.21379 11.2974 5.38927 11.2974C5.56475 11.2974 5.74042 11.2305 5.87412 11.0965L8.00003 8.9704L10.1259 11.0965C10.2598 11.2304 10.4353 11.2974 10.6108 11.2974C10.7863 11.2974 10.9617 11.2305 11.0956 11.0965C11.3635 10.8286 11.3635 10.3947 11.0956 10.1269L8.96964 8.00079L11.0955 5.8747C11.3634 5.60684 11.3634 5.17286 11.0955 4.905Z" fill="#3C464D"/>
													</g>
													<defs>
														<clipPath id="clip0">
															<rect width="16" height="16" fill="white"/>
														</clipPath>
													</defs>
												</svg>

												<span>Отменить подписку</span>
											</a>
										</div>
										<!-- product__bottom -->
									</div>
									<!-- product__item -->

									<div class="product__item">
										<div class="stickers">
											<span class="stickers__item stickers__item_new">
												New
											</span>
										</div>
										<a href="#" class="product__wishlist">
											<img src="<?=SITE_TEMPLATE_PATH;?>/img/icons/heart.svg" alt="heart">
										</a>
										<a href="#" class="product-images products__image">
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-2.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
											<div class="product-images__item">
												<img src="<?=SITE_TEMPLATE_PATH;?>/img/tovar-3.jpg" alt="Мармелад «венское кафе»со вкусом карамели, 180 г">
											</div>
										</a>
										<ul class="product-attributes">
											<li class="product-attributes__item">
												<span>150</span>
												<span>ккал</span>
											</li>
											<li class="product-attributes__item">
												<span>4</span>
												<span>белки</span>
											</li>
											<li class="product-attributes__item">
												<span>0,3</span>
												<span>хе</span>
											</li>
											<li class="product-attributes__item">
												<span>11</span>
												<span>углеводы</span>
											</li>
										</ul>
										<a href="#" class="products__title">
											Мармелад «венское кафе»со вкусом карамели, 180 г
										</a>
										<div class="product__bottom">
											<a href="#!" class="cab-subscriptions_cancel">
												<svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
													<g clip-path="url(#clip0)">
														<path d="M8.00005 0C3.58882 0 0 3.58882 0 8.00005C0 12.4113 3.58882 16.0001 8.00005 16.0001C12.4113 16.0001 16.0001 12.4113 16.0001 8.00005C16.0001 3.58882 12.4112 0 8.00005 0ZM8.00005 14.6286C4.34511 14.6286 1.37145 11.655 1.37145 8.00005C1.37145 4.34511 4.34511 1.37145 8.00005 1.37145C11.655 1.37145 14.6286 4.34511 14.6286 8.00005C14.6286 11.655 11.6549 14.6286 8.00005 14.6286Z" fill="#3C464D"/>
														<path d="M11.0955 4.905C10.8277 4.63714 10.3933 4.63714 10.1259 4.905L8.00003 7.03118L5.87412 4.905C5.60626 4.63714 5.17237 4.63714 4.90451 4.905C4.63665 5.17286 4.63665 5.60675 4.90451 5.87461L7.03042 8.0007L4.90442 10.1269C4.63656 10.3947 4.63656 10.8286 4.90442 11.0965C5.0383 11.2304 5.21379 11.2974 5.38927 11.2974C5.56475 11.2974 5.74042 11.2305 5.87412 11.0965L8.00003 8.9704L10.1259 11.0965C10.2598 11.2304 10.4353 11.2974 10.6108 11.2974C10.7863 11.2974 10.9617 11.2305 11.0956 11.0965C11.3635 10.8286 11.3635 10.3947 11.0956 10.1269L8.96964 8.00079L11.0955 5.8747C11.3634 5.60684 11.3634 5.17286 11.0955 4.905Z" fill="#3C464D"/>
													</g>
													<defs>
														<clipPath id="clip0">
															<rect width="16" height="16" fill="white"/>
														</clipPath>
													</defs>
												</svg>

												<span>Отменить подписку</span>
											</a>
										</div>
										<!-- product__bottom -->
									</div>
									<!-- product__item -->

								</div>
								<!-- subscriptions__slider__inner -->
							</div>
							<!-- cab-subscriptions -->

						</div>
						<!-- hidden -->
					</li>
					<!-- li -->

				</ul>
			</div>
			<!-- Mobile accordeon -->

		</div>
		<!-- cabinet_mobile -->

	</section>
	
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>